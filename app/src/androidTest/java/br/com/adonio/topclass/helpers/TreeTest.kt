package br.com.adonio.topclass.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TreeTest {

    @Test
    fun basic() {

        val root = Leaf<Int>("root", null)
        root.add("c/wamp/wand/", 2, "dois")
        root.add("c/wamp/wand", 4, "quatro")
        root.add("c/wamp/wand", 5, "cinco")
        root.add("c/wamp/teaser", 6, "seis")
        root.add("c/wamp/butt_hole", 7, "sete")

        val disk = root.leaf("c")
        val wamp = disk?.leaf("wamp")
        val wand = wamp?.leaf("wand")

        assertEquals(wand?.children()?.count(), 3)

        val lines = root.toLines().map { it.level to it.leaf.name }
        assertEquals(lines[0].first, 0)
    }

}