package br.com.adonio.topclass.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ArrTest {

    @Test
    fun forceCount() {
        val d = listOf("dino", "outra")
        val r = Arr(d).forceCount(listOf("monstrao", "cabreiro", "ateu"), false)
        assertEquals(r[0], "dino")
        assertEquals(r[1], "outra")
        assertEquals(r[2], "ateu")
    }

    @Test
    fun forceCountShift() {
        val d = listOf("dino", "outra")
        val r = Arr(d).forceCount(listOf("monstrao", "cabreiro", "ateu"), true)
        assertEquals(r[0], "monstrao")
        assertEquals(r[1], "dino")
        assertEquals(r[2], "outra")
    }

}