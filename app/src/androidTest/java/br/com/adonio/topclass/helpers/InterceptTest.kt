package br.com.adonio.topclass.helpers

import android.util.Log
import org.json.JSONObject
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.threeten.bp.LocalDateTime

@RunWith(JUnit4::class)
class InterceptTest : InterceptListener {

    private var triggerCount = 0

    @Test
    fun test1() {
        Intercept.register(this)
        Intercept.register(this)

        triggerCount = 0
        val one = Intercept.retrieveFirst<Int>("um") ?: 0
        val two = Intercept.retrieveFirst<Int>("dois") ?: 0
        assertEquals(one, 1)

        assertEquals(triggerCount, 2)

        assertEquals(two, 2)

        // wrong type
        val someString = Intercept.retrieveFirst<String>("um") ?: "samba"
        assertEquals(someString, "samba")

        Intercept.unregister(this)
        val nop = Intercept.retrieveFirst<Int>("dois") ?: 0
        assertEquals(nop, 0)
    }

    override fun intercept(key: String): Any? {
        triggerCount++
        when(key) {
            "um" -> return 1
            "dois" -> return 2
        }
        return null
    }

}