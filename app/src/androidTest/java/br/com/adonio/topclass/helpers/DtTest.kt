package br.com.adonio.topclass.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DtTest {

    @Test
    fun fromNow() {
        val now = Dt.fromPattern("2020-02-02 12:30")

        var dt = Dt.fromPattern("2020-02-02 12:31")
        assertEquals("em um minuto", Dt.fromNow(dt, now))

        dt = Dt.fromPattern("2020-02-02 12:29")
        assertEquals("um minuto atrás", Dt.fromNow(dt, now))

        dt = Dt.fromPattern("2020-02-01 12:29")
        assertEquals("ontem, às 12:29", Dt.fromNow(dt, now))

        dt = Dt.fromPattern("2020-02-03 15:15")
        assertEquals("amanhã, às 15:15", Dt.fromNow(dt, now))

        dt = Dt.fromPattern("2020-02-02 13:21")
        assertEquals("em menos de uma hora", Dt.fromNow(dt, now))
    }

}