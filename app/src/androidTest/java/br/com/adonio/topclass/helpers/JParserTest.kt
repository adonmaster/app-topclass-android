package br.com.adonio.topclass.helpers

import com.google.gson.JsonParser
import org.json.JSONObject
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class JParserTest {

    data class Dumm(val id: Int, val desc: String, val updatedAt: String?, val children: List<Dumm>)

    @Test
    fun next() {
        val json = """
             {
                    "id": 1,
                    "desc": "Língua Portuguesa/Fundamental 1",
                    "created_at": "2019-11-21 12:27:16",
                    "updated_at": null,
                    "pro_skill_auth": [
                      {
                        "id": 33,
                        "desc": "dollar",
                        "updated_at": "DATA_FORMAT"
                      }
                    ]
                  }
        """.trimIndent()
        val dum = JParser(JSONObject(json)) {
            store("id")
            store("desc")
            store("updated_at")
            arr("pro_skill_auth") {
                store("id")
                store("desc")
                store("updated_at")
            }
        }.parse {
            Dumm(getNext(), getNext(), getNexto(), arr("pro_skill_auth") {
                Dumm(getNext(), getNext(), getNexto(), listOf())
            })
        }

        assertNotNull(dum)
        assertEquals(dum.id, 1)
        assertEquals(dum.desc, "Língua Portuguesa/Fundamental 1")
        assertNull(dum.updatedAt)

        assertEquals(dum.children.first().id, 33)
        assertEquals(dum.children.first().desc, "dollar")
        assertEquals(dum.children.first().updatedAt, "DATA_FORMAT")

    }

}