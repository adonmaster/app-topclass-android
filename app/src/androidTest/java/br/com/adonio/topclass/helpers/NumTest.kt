package br.com.adonio.topclass.helpers

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class NumTest {

    @Test
    fun fromBrCurrency() {
        var vv = Num.fromBrCurrency("R$ 2.000,06", 0.0)
        assertTrue(vv == 2000.06)

        vv = Num.fromBrCurrency("3.000,40", 0.0)
        assertTrue(vv == 3000.4)

        vv = Num.fromBrCurrency("R$ 5000,01", 0.0)
        assertTrue(vv == 5000.01)

        vv = Num.fromBrCurrency("275,20", 0.0)
        assertTrue(vv == 275.2)

        vv = Num.fromBrCurrency("adon", 66.6)
        assertTrue(vv == 66.6)
    }

}