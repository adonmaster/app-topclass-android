package br.com.adonio.topclass.helpers

import org.json.JSONObject
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class JSimpleTest {

    @Test
    fun get() {
        val json = JSONObject()
        json.put("geremias", "cachorro")

        assertEquals("cachorro", JSimple(json).get("geremias"))

        val rrr: Int? = JSimple(json).get("geremias")
        assertNull(rrr)

        assertNull(JSimple(json).get("carlos"))

        assertNull(JSimple(null).get("geremias"))
    }

}