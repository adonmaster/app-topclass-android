package br.com.adonio.topclass.frags.pro

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.adonio.topclass.frags.StateViewModel
import br.com.adonio.topclass.structs.ProposalBody
import br.com.adonio.topclass.structs.SimpleAddress
import org.threeten.bp.LocalDateTime

class FragProProposalNewVM: ViewModel() {

    var proId: Int = 0
    val items = MutableLiveData<List<ProposalBody.Item>>().also { it.value = listOf() }
    val address = MutableLiveData<SimpleAddress>()

    var priceForHour: Double = 0.0
    var totalValue: Double = 0.0
    var cardToken: String? = null
}