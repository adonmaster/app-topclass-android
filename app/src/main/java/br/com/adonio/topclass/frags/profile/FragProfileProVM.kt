package br.com.adonio.topclass.frags.profile

import androidx.lifecycle.MutableLiveData
import br.com.adonio.topclass.frags.StateViewModel
import java.io.File

class FragProfileProVM: StateViewModel() {

    val pdf = MutableLiveData<File>()

}