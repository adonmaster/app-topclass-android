package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.structs.SimpleCard

class SimpleCardPresenter(private val model: SimpleCard) {

    val no by lazy {
        model.no
            .split(" ")
            .mapIndexed { i, s -> if (i==3) s else "xxxx" }
            .joinToString(" ")
    }

    val secret by lazy {
        "xxx"
    }

}