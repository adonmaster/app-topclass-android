package br.com.adonio.topclass.helpers

import android.annotation.SuppressLint
import java.lang.RuntimeException

class Arr<T>(private val list: List<T>?) {

    @SuppressLint("DefaultLocale")
    fun categorize(extractor: (T)->String): List<Pair<String,List<T>>> {
        val list = list ?: return listOf()
        val r = mutableListOf<Pair<String,ArrayList<T>>>()

        for (item in list) {
            val category = extractor(item)
            val index = r.indexOfFirst { it.first.trim().toLowerCase() == category.trim().toLowerCase() }
            if (index >= 0) {
                r[index].second.add(item)
            } else {
                r.add(category to arrayListOf(item))
            }
        }

        return r
    }

    fun forceCount(defaults: List<T>, shiftRight: Boolean): List<T> {
        val r = mutableListOf<T>()
        val capacity = defaults.count()
        //
        if (shiftRight) {
            val offset = capacity - (list?.count() ?: capacity)
            for (i in capacity-1 downTo 0) {
                val v = list?.getOrNull(i - offset) ?: defaults[i]
                r.add(0, v!!)
            }
        }
        //
        else {
            for (i in 0 until capacity) {
                val v = list?.getOrNull(i) ?: defaults[i]
                r.add(v!!)
            }
        }
        return r
    }

    fun contains(cb: (T)->Boolean): Boolean {
        list?.forEach {
            if (cb(it)) return true
        }
        return false
    }

}