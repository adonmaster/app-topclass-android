package br.com.adonio.topclass.frags.profile

import androidx.lifecycle.MutableLiveData
import br.com.adonio.topclass.frags.StateViewModel
import br.com.adonio.topclass.helpers.Leaf
import br.com.adonio.topclass.rmodels.SkillsRModel

class FragProfileProResolvedVM: StateViewModel() {

    val root = MutableLiveData<Leaf<SkillsRModel>>().also { it.value = Leaf.root() }

}