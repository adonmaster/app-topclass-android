package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.ContractRModelPresenter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class ContractRModel(
    val id: Int,
    val body: JSONObject,
    val price: Double,
    val due_at: LocalDateTime,
    val was_executed: Boolean,
    val clientId: Int, val clientName: String, val clientAvatar: String?,
    val proId: Int, val proName: String, val proAvatar: String?,
    val createdAt: LocalDateTime
) {

    val p by lazy { ContractRModelPresenter(this) }

    companion object {

        fun parse(json: JSONObject) = JParser(json) {
            arr("payload") {
                store("id")
                store("body")
                store("price")
                store("due_at")
                store("was_executed")
                storeSeek("client_id", "client_user.id")
                storeSeek("client_name", "client_user.name")
                storeSeek("client_avatar", "client_user.avatar_thumb")
                storeSeek("pro_id", "pro_user.id")
                storeSeek("pro_name", "pro_user.name")
                storeSeek("pro_avatar", "pro_user.avatar_thumb")
                store("created_at")
            }
        }.parse {
            arr("payload") {
                ContractRModel(
                    getNext(), getNext(), getNext(), Dt.fromJson(getNexto())!!,
                    getNext(), getNext(),
                    getNext(), getNexto(), getNext(), getNext(), getNexto(),
                    Dt.fromJson(getNext())!!
                )
            }
        }

    }

}
/**
{
    "message": "Operação executada com êxito.",
    "payload": [
    {
        "id": 1,
        "pro_user_id": 1,
        "client_user_id": 1,
        "body": {
        "items": [
        {
            "duration": 60,
            "materia": "Língua Portuguesa",
            "dt": "2020-03-15 06:15:00"
        }
        ],
        "address_line": "Avenida V-5 Quadra 199, Lote 23 Cidade Vera Cruz II - Cidade Vera Cruz, Aparecida de Goiânia - GO, 74936-600, Brasil",
        "address_lat": -16.7639998,
        "address_lng": -49.2899184,
        "version": 2
    },
        "price": "50.00",
        "due_at": "2020-03-15 08:15:00",
        "was_executed": false,
        "created_at": "2020-03-06 22:16:51",
        "updated_at": "2020-03-06 22:16:51",
        "client_user": {
        "id": 1,
        "name": "Adonho",
        "email": "adonio.silva@gmail.com",
        "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
        "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
        "email_verified_at": "2020-01-30 16:36:58",
        "last_active": "2020-01-30 16:36:58",
        "is_pro": true,
        "created_at": "2019-11-04 20:33:34",
        "updated_at": "2020-01-30 16:37:00"
    },
        "pro_user": {
        "id": 1,
        "name": "Adonho",
        "email": "adonio.silva@gmail.com",
        "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
        "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
        "email_verified_at": "2020-01-30 16:36:58",
        "last_active": "2020-01-30 16:36:58",
        "is_pro": true,
        "created_at": "2019-11-04 20:33:34",
        "updated_at": "2020-01-30 16:37:00"
    }
    }
    ]
}
*/