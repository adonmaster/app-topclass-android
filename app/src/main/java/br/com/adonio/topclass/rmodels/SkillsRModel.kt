package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.SkillsPresenter
import org.json.JSONObject

data class SkillsRModel(
    val id: Int,
    val desc: String,
    var checked: Boolean,
    var checkedComp: String?
) {

    companion object {

        fun parse(json: JSONObject) = JParser(json) {
            obj("payload") {
                arr("skills") {
                    store("id")
                    store("desc")
                    storeSeek("checked", "pro_skill_auth.0.option")
                    storeSeek("checked_comp", "pro_skill_auth.0.comp")
                }
            }
        }.parse {
            obj("payload") {
                arr("skills") {
                    SkillsRModel(
                        get("id"),
                        get("desc"),
                        geto("checked") ?: false,
                        geto("checked_comp")
                    )
                }
            }
        }

        @Suppress("unused")
        val sample = """
            {
                "id": 2,
                "desc": "Língua Portuguesa/Fundamental 1",
                "created_at": "2019-11-21 13:22:27",
                "updated_at": "2019-11-21 13:22:27",
                "pro_skill_auth": [
                  {
                    "id": 1,
                    "user_id": 1,
                    "skill_id": 2,
                    "comp": "dinossauro"
                  }
                ]
              },
        """.trimIndent()

    }

    val p by lazy { SkillsPresenter(this) }

}