package br.com.adonio.topclass.frags.about

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.LoginActivityVM
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.helpers.Http
import br.com.adonio.topclass.helpers.HttpFormData
import br.com.adonio.topclass.helpers.T
import kotlinx.android.synthetic.main.frag_login_email.*

class FragLoginEmail: Fragment() {

    // state
    private lateinit var vm: LoginActivityVM

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_login_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(LoginActivityVM::class.java)

        vm.loading.observe(this, Observer {
            ui_progress.isVisible = it > 0
            ui_btn_submit.setEnabledEx(it == 0)
        })

        ui_ed_email.setText(vm.email)
        ui_ed_name.setText(vm.name)

        ui_btn_submit.setOnClickListener { trySubmit() }
    }

    private fun trySubmit() {
        vm.email = ui_ed_email.text.toString()
        vm.name = ui_ed_name.text.toString()

        if (vm.email.isBlank()) {
            T.error(activity, "Preencha o email!")
            return
        }

        if (vm.name.isBlank()) {
            T.error(activity, "Preencha o nome!")
            return
        }

        submit()
    }

    private fun submit()
    {
        vm.loadingInc(1)
        val params = HttpFormData()
            .put("name", vm.name)
            .put("email", vm.email)

        Http.post("register", params)
            .then {
                findNavController().navigate(R.id.sg_to_login_code)
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

}