package br.com.adonio.topclass.frags.pro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.FragProVM
import br.com.adonio.topclass.frags.adapters.FragProShowSkillsAdapter
import kotlinx.android.synthetic.main.frag_pro.*
import kotlinx.android.synthetic.main.frag_pro_show_skills.*

class FragProShowSkills: Fragment() {

    private lateinit var vmParent: FragProVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro_show_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vmParent = ViewModelProviders.of(activity!!).get(FragProVM::class.java)
        val m = vmParent.selected.value ?: return
        //-- halt

        ui_rv_skills.adapter = FragProShowSkillsAdapter(m)
        ui_rv_skills.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv_skills.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
    }

}