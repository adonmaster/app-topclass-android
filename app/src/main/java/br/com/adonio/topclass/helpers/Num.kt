package br.com.adonio.topclass.helpers

object Num {

    fun fromBrCurrency(comp: String?, def: Double): Double {
        if (comp==null) return def
        return comp.replace("R$", "")
            .replace(".", "")
            .replace(",", ".")
            .trim()
            .toDoubleOrNull()
            ?: def
    }

}