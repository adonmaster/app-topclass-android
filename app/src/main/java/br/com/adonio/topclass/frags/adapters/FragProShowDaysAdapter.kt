package br.com.adonio.topclass.frags.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.rmodels.ProQueryRModel
import kotlinx.android.synthetic.main.rv_frag_pro_show_days.view.*
import kotlinx.android.synthetic.main.rv_frag_pro_show_days_header.view.*

class FragProShowDaysAdapter(model: ProQueryRModel) : RecyclerView.Adapter<FragProShowDaysAdapter.VH>()
{
    private val mData by lazy {
        model.p.skillsRoot
            .children()
            .firstOrNull { it.name.startsWith("Hor", true) }
            ?.toLines()
            ?.drop(1)
            ?: listOf()
    }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position].level
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val idHeader = R.layout.rv_frag_pro_show_days_header
        val idData = R.layout.rv_frag_pro_show_days
        val id = when(viewType) {
            1 -> idHeader
            else -> idData
        }
        val v = Act.inflate(id, parent)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        when(m.level) {
            1 -> holder.uiHeaderTitle?.text = m.leaf.name
            2 -> holder.uiDesc?.text = m.leaf.name
        }
    }


    // view holder
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiHeaderTitle: TextView? = itemView.ui_header_title
        val uiDesc: TextView? = itemView.ui_desc
    }

}