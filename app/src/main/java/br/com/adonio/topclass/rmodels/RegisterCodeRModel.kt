package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.JParser
import org.json.JSONObject

data class RegisterCodeRModel(
    val id: Long, val api_token: String, val avatar: String?, val avatarThumb: String?,
    val is_pro: Boolean
) {

    /**
    {
        "message": "Operação executada com êxito.",
        "payload": {
            "id": 1,
            "api_token": "EDltaHWydzn7Vqxbgfdwws3iu4KZv7eHpjVyrlB00lqpZ0xF1h5REwUKVOJK"
        }
    }
     */

    companion object {
        fun parse(response: JSONObject): RegisterCodeRModel = JParser(response) {
            obj("payload") {
                store("id")
                store("api_token")
                store("avatar")
                store("avatar_thumb")
                store("is_pro")
            }
        }.parse {
            obj("payload") {
                RegisterCodeRModel(
                    get("id"),
                    get("api_token"),
                    geto("avatar"),
                    geto("avatar_thumb"),
                    get("is_pro")
                )
            }
        }

    }

}