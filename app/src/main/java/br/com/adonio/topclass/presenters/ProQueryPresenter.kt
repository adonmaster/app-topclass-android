@file:Suppress("SpellCheckingInspection")

package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.helpers.Arr
import br.com.adonio.topclass.helpers.Leaf
import br.com.adonio.topclass.helpers.Num
import br.com.adonio.topclass.rmodels.ProQueryRModel
import br.com.adonio.topclass.structs.Cs

class ProQueryPresenter(model: ProQueryRModel) : BasePresenter<ProQueryRModel>(model) {

    val skillsRoot by lazy {
        val root = Leaf.root<ProQueryRModel.Skill>()
        model.skills
            .forEach { m ->
                val split = Arr(m.desc.split("/"))
                    .forceCount(listOf("Habilidades", "-", "-"), true)
                val path = listOf(split[0], split[1])
                val name = split[2]
                root.add(path, m, name)
            }
        root
    }

    fun skillsDescriptionSimple(): String {
        val skillDescs = model.skills.mapNotNull { m ->
            val split = m.desc.split("/")
            val n = split.count()
            val isSkill = (n==2 || (n==3 && split.first().startsWith("Habilid", true)))
            if ( ! isSkill) return@mapNotNull null

            val splitFixed = if (n==3) split.drop(1) else split
            return@mapNotNull splitFixed.first() to splitFixed.last()
        }

        if (skillDescs.count()==0) return "[sem habilidades cadastradas]"

        return skillDescs
            .distinctBy { it.first }
            .joinToString(", ") { it.first }
    }

    fun skillPriceDesc(): String? {
        return skillsRoot.children()
            .find { it.name == Cs.SKILL_CATEGORY_HORA_AULA }
            ?.findFirstContent()
            ?.pivotComp
    }

    fun skillPriceValue(): Double {
        return Num.fromBrCurrency(skillPriceDesc(), 0.0)
    }

    val skillsDays by lazy<List<Pair<String, List<String>>>> {
        val r = mutableListOf<Pair<String, List<String>>>()
        model.skills
            .mapNotNull { m ->
                val split = Arr(m.desc.split("/"))
                    .forceCount(listOf("Habilidades", "-", "-"), true)

                if (split.first() == Cs.SKILL_CATEGORY_HORARIOS) {
                    split[1] to split[2]
                } else {
                    null
                }

            }.forEach { pair ->
                val ii = r.indexOfFirst { l -> l.first == pair.first }
                val ll = (r.getOrNull(ii)?.second ?: listOf()).toMutableList()
                ll.add(pair.second)
                if (ii >= 0) {
                    r[ii] = pair.first to ll
                } else {
                    r.add(pair.first to ll)
                }
            }
        r
    }

}