package br.com.adonio.topclass.frags.profile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setVisible
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.repo.Repo
import br.com.adonio.topclass.rmodels.UserRModel
import kotlinx.android.synthetic.main.frag_profile_detail.*

class FragProfileDetail: Fragment() {

    // state
    private lateinit var vm: FragProfileDetailVM

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_profile_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragProfileDetailVM::class.java)

        val user = App.user ?: return

        vm.loading.observe(this, Observer {
            ui_progress.setVisible(it > 0)
        })

        ui_avatar.setOnClickListener {
            ImgProviderCrop.pick(context!!, this)
        }
        Img.load(user.avatar_thumb, ui_avatar)

        ui_ed_name.setText(user.name)

        ui_email.text = user.email
    }

    private fun save()
    {
        Act.hideKeyboard(ui_ed_name)

        val params = HttpMulti()
            .add("name", ui_ed_name.text.toString())
        vm.avatarFile?.let { f ->
            params.add("avatar", f)
        }

        vm.loadingInc(1)
        Http.upload("profile", params)
            .then {
                val rmodel = UserRModel.parse(it)
                afterSubmit(rmodel)
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun afterSubmit(rmodel: UserRModel) {
        Repo.user.update(rmodel)

        // load to instance
        App.shared.invalidateUid()
        App.shared.loadActiveUser()

        T.success(activity, "Dados salvos com sucesso!")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_profile_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_action_save -> save()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        ImgProviderCrop.onActivityResult(requestCode, resultCode, data)?.let { f ->
            Img.load(f, ui_avatar)
            vm.avatarFile = f
        }
    }

}