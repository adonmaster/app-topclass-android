package br.com.adonio.topclass.structs

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JSimple
import org.json.JSONObject
import org.threeten.bp.LocalDateTime


data class ProposalBody(
    val items: List<Item>,
    val address: SimpleAddress
) {

    data class Item(val dt: LocalDateTime, val materia: String, val duration: Int) {
        fun toMap(): HashMap<String, Any> {
            return hashMapOf(
                "dt" to Dt.formatToJson(dt)!!,
                "materia" to materia,
                "duration" to duration
            )
        }
    }

    companion object {
        fun toJson(items: List<Item>, address: SimpleAddress): JSONObject {
            return JSONObject(hashMapOf(
                "version" to 2,
                "items" to items.map { it.toMap() },
                "address_line" to address.address,
                "address_lat" to address.lat,
                "address_lng" to address.lng,
                "address_comp" to address.comp
            ))
        }
        fun parse(s: String?): ProposalBody? {
            if (s==null) return null
            try {
                return parse(JSONObject(s))
            } catch(e: Exception) {}
            return null
        }
        fun parse(json: JSONObject?): ProposalBody? {
            if (json==null) return null
            return parseV1(json) ?: parseV2(json)
        }

        private fun parseV2(json: JSONObject): ProposalBody? {
            if (json.getInt("version") != 2) return null

            val ritems = arrayListOf<Item>()
            val jitems = json.getJSONArray("items")
            for (cont in 0 until jitems.length()) {
                val jitem = jitems.getJSONObject(cont)
                val dt = Dt.fromJson(jitem.getString("dt"))!!
                val duration = jitem.getInt("duration")
                val materia = jitem.getString("materia")

                ritems.add(Item(dt, materia, duration))
            }

            val addressLine = json.getString("address_line")
            val addressLat = json.getDouble("address_lat")
            val addressLng = json.getDouble("address_lng")
            val addressComp: String? = JSimple(json).get("address_comp")
            val raddress = SimpleAddress(addressLine, addressLat, addressLng, addressComp)

            return ProposalBody(ritems, raddress)
        }

        private fun parseV1(json: JSONObject): ProposalBody? {
            if (json.getInt("version") != 1) return null

            val rdates = arrayListOf<LocalDateTime>()
            val jdates = json.getJSONArray("dates")
            for (cont in 0 until jdates.length()) {
                rdates.add(Dt.fromJson(jdates.getString(cont))!!)
            }

            val addressLine = json.getString("address_line")
            val addressLat = json.getDouble("address_lat")
            val addressLng = json.getDouble("address_lng")
            val raddress = SimpleAddress(addressLine, addressLat, addressLng, null)

            return ProposalBody(rdates.map { Item(it, "[Desconhecido]", 60) }, raddress)
        }
    }

}