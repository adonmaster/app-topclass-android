package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.helpers.Arr
import br.com.adonio.topclass.rmodels.SkillsRModel

class SkillsPresenter(model: SkillsRModel) : BasePresenter<SkillsRModel>(model) {

    val descArray by lazy {
        val or = model.desc.split("/")
            .filter { it.trim().isNotBlank() }
        Arr(or).forceCount(listOf("Habilidades", "[desconhecido]", "[desconhecido]"), true)
    }

    fun category(): String {
        return descArray.first()
    }

    fun subCategory(): String {
        return descArray[1]
    }

    fun desc(): String {
        return descArray[2]
    }

}