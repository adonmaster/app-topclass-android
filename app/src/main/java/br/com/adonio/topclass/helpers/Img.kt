package br.com.adonio.topclass.helpers

import android.graphics.*
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import br.com.adonio.topclass.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import java.io.File

private abstract class ImgResource {
    abstract fun buildRequest(): RequestCreator
}

private class StrResource(val s: String?): ImgResource() {
    override fun buildRequest(): RequestCreator {
        return Picasso.get().load(s)
    }
}

private class FileResource(val file: File): ImgResource() {
    override fun buildRequest(): RequestCreator {
        return Picasso.get().load(file)
    }
}

object Img {

    private fun load(resource: ImgResource, imgView: ImageView?, @DrawableRes placeholder: Int, onDone: ((ImageView)->Unit)?=null) {
        imgView?.let {
            resource.buildRequest()
                .placeholder(placeholder)
                .error(R.drawable.img_404)
                .into(it, object : Callback {
                    override fun onSuccess() {
                        onDone?.invoke(it)
                    }
                    override fun onError(e: Exception?) { }
                })
        }
    }

    fun load(path: String?, imgView: ImageView?, onDone: ((ImageView) -> Unit)?=null) {
        load(StrResource(path), imgView, R.drawable.img_ph, onDone)
    }

    fun load(file: File?, imgView: ImageView?, onDone: ((ImageView) -> Unit)?=null) {
        val f = guard(file) { return }
        load(FileResource(f), imgView, R.drawable.img_ph, onDone)
    }

    fun thumbFrom(targetBmp: Bitmap, reqHeightInPixels: Int=200, reqWidthInPixels: Int=200): Bitmap {
        val matrix = Matrix()
            .apply {
                setRectToRect(
                    RectF(0f, 0f, targetBmp.width.toFloat(), targetBmp.height.toFloat()),
                    RectF(0f, 0f, reqWidthInPixels.toFloat(), reqHeightInPixels.toFloat()),
                    Matrix.ScaleToFit.CENTER
                )
            }
        return Bitmap.createBitmap(targetBmp, 0, 0, targetBmp.width, targetBmp.height, matrix, true)
    }


    fun bitmapFromView(view: View): Bitmap {
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) {
            bgDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        return bitmap!!
    }

    fun loadAvatar(url: String?, imgView: ImageView?) {
        load(StrResource(url), imgView, R.drawable.img_guy)
    }
}