package br.com.adonio.topclass.frags.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.structs.ProposalBody
import kotlinx.android.synthetic.main.rv_frag_pro_proposal_new_dates.view.*

class FragProProposalNewDatesAdapter: RecyclerView.Adapter<FragProProposalNewDatesAdapter.VH>() {

    private var mData = listOf<ProposalBody.Item>()
    var listener: Listener? = null

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_pro_proposal_new_dates, parent, false)!!
        return VH(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]

        val dt = Dt.format(m.dt, "dd/MM/yyyy HH:mm'h'")
        val materia = m.materia
        val duration = m.duration

        holder.uiDesc.text = "$dt\n$materia - $duration mins"
    }

    fun updateData(data: List<ProposalBody.Item>) {
        mData = data
        notifyDataSetChanged()
    }

    // vh
    @Suppress("HasPlatformType")
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiDesc = itemView.ui_desc
        val uiBtnRight = itemView.ui_btn_right
        init {
            uiBtnRight.setOnClickListener { listener?.onDelete(adapterPosition) }
        }
    }

    // listener
    interface Listener {
        fun onDelete(position: Int)
    }
}