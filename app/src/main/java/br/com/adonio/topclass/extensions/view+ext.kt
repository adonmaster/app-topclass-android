package br.com.adonio.topclass.extensions

import android.animation.Animator
import android.graphics.Point
import android.view.View
import androidx.annotation.ColorRes
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Cl
import br.com.adonio.topclass.helpers.ter
import br.com.adonio.topclass.structs.SimpleAnimatorListener
import com.google.android.material.snackbar.Snackbar

inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.()->Unit) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}

fun View.snack(message: String, length: Int) {
    snack(message, length) {}
}

fun View.snack(message: String, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) { cb?.let { setAction("OK", it) } }
}

fun View.snackOk(message: String, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) { setAction("OK") { cb?.invoke(it) } }
}

fun View.snackError(message: String, cb: ((View)->Unit)?=null) {
    snackAction(message, "OK, então!", R.color.md_red_500, cb)
}

fun View.snackSuccess(message: String, cb: ((View)->Unit)?=null) {
    snackAction(message, "Maravilha!", R.color.md_green_500, cb)
}

fun View.snackAction(message: String, actionMessage: String, @ColorRes actionColor: Int, cb: ((View)->Unit)?=null) {
    snack(message, Snackbar.LENGTH_INDEFINITE) {
        setActionTextColor(Cl.res(context, actionColor))
        setAction(actionMessage) { cb?.invoke(it) }
    }
}

fun View.setVisible(visible: Boolean=false) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.setVisibleAlpha(visible: Boolean) {
    val a = ter(visible, 1.0f, 0.0f)
    animate()
        .alpha(a)
        .setDuration(200)
        .setListener(object: SimpleAnimatorListener() {
            override fun onAnimationEnd(animation: Animator?, isReverse: Boolean) {
                setVisible(visible)
                alpha = 1.0f
            }
        })
        .start()
}

fun View.isVisible() = visibility == View.VISIBLE

fun View.toggleVisibility(): Boolean {
    val isVisible = visibility == View.VISIBLE
    setVisible(!isVisible)
    return !isVisible
}

fun View.getLocationInScreen(): Point {
    val arr = IntArray(2)
    this.getLocationOnScreen(arr)
    return Point(arr[0], arr[1])
}

fun View.setEnabledEx(option: Boolean) {
    isEnabled = option
    alpha = ter(option, 1f, 0.2f)
}