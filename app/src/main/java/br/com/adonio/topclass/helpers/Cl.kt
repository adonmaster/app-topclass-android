@file:Suppress("FunctionName", "unused")

package br.com.adonio.topclass.helpers

import android.content.Context
import android.os.Build
import androidx.annotation.ColorRes
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R

@Suppress("DEPRECATION")
object Cl {

    fun res(context: Context, @ColorRes id: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.resources.getColor(id, context.theme)
        } else {
            context.resources.getColor(id)
        }
    }

    fun res(@ColorRes id: Int): Int {
        return res(App.shared, id)
    }

    fun ter(option: Boolean, @ColorRes yep: Int, @ColorRes nop: Int): Int {
        return res(App.shared, if (option) yep else nop)
    }

    fun quad(@ColorRes def: Int, vararg pairs: Pair<Boolean, Int>): Int {
        for (pair in pairs) {
            if (pair.first) return res(App.shared, pair.second)
        }
        return res(App.shared, def)
    }

    /**
     * facilitator
     */
    fun md_red_50(): Int { return res(R.color.md_red_50) }
    fun md_red_100(): Int { return res(R.color.md_red_100) }
    fun md_red_200(): Int { return res(R.color.md_red_200) }
    fun md_red_300(): Int { return res(R.color.md_red_300) }
    fun md_red_400(): Int { return res(R.color.md_red_400) }
    fun md_red_500(): Int { return res(R.color.md_red_500) }
    fun md_red_600(): Int { return res(R.color.md_red_600) }
    fun md_red_700(): Int { return res(R.color.md_red_700) }
    fun md_red_800(): Int { return res(R.color.md_red_800) }
    fun md_red_900(): Int { return res(R.color.md_red_900) }
    fun md_red_A100(): Int { return res(R.color.md_red_A100) }
    fun md_red_A200(): Int { return res(R.color.md_red_A200) }
    fun md_red_A400(): Int { return res(R.color.md_red_A400) }
    fun md_red_A700(): Int { return res(R.color.md_red_A700) }
    fun md_pink_50(): Int { return res(R.color.md_pink_50) }
    fun md_pink_100(): Int { return res(R.color.md_pink_100) }
    fun md_pink_200(): Int { return res(R.color.md_pink_200) }
    fun md_pink_300(): Int { return res(R.color.md_pink_300) }
    fun md_pink_400(): Int { return res(R.color.md_pink_400) }
    fun md_pink_500(): Int { return res(R.color.md_pink_500) }
    fun md_pink_600(): Int { return res(R.color.md_pink_600) }
    fun md_pink_700(): Int { return res(R.color.md_pink_700) }
    fun md_pink_800(): Int { return res(R.color.md_pink_800) }
    fun md_pink_900(): Int { return res(R.color.md_pink_900) }
    fun md_pink_A100(): Int { return res(R.color.md_pink_A100) }
    fun md_pink_A200(): Int { return res(R.color.md_pink_A200) }
    fun md_pink_A400(): Int { return res(R.color.md_pink_A400) }
    fun md_pink_A700(): Int { return res(R.color.md_pink_A700) }
    fun md_purple_50(): Int { return res(R.color.md_purple_50) }
    fun md_purple_100(): Int { return res(R.color.md_purple_100) }
    fun md_purple_200(): Int { return res(R.color.md_purple_200) }
    fun md_purple_300(): Int { return res(R.color.md_purple_300) }
    fun md_purple_400(): Int { return res(R.color.md_purple_400) }
    fun md_purple_500(): Int { return res(R.color.md_purple_500) }
    fun md_purple_600(): Int { return res(R.color.md_purple_600) }
    fun md_purple_700(): Int { return res(R.color.md_purple_700) }
    fun md_purple_800(): Int { return res(R.color.md_purple_800) }
    fun md_purple_900(): Int { return res(R.color.md_purple_900) }
    fun md_purple_A100(): Int { return res(R.color.md_purple_A100) }
    fun md_purple_A200(): Int { return res(R.color.md_purple_A200) }
    fun md_purple_A400(): Int { return res(R.color.md_purple_A400) }
    fun md_purple_A700(): Int { return res(R.color.md_purple_A700) }
    fun md_deep_purple_50(): Int { return res(R.color.md_deep_purple_50) }
    fun md_deep_purple_100(): Int { return res(R.color.md_deep_purple_100) }
    fun md_deep_purple_200(): Int { return res(R.color.md_deep_purple_200) }
    fun md_deep_purple_300(): Int { return res(R.color.md_deep_purple_300) }
    fun md_deep_purple_400(): Int { return res(R.color.md_deep_purple_400) }
    fun md_deep_purple_500(): Int { return res(R.color.md_deep_purple_500) }
    fun md_deep_purple_600(): Int { return res(R.color.md_deep_purple_600) }
    fun md_deep_purple_700(): Int { return res(R.color.md_deep_purple_700) }
    fun md_deep_purple_800(): Int { return res(R.color.md_deep_purple_800) }
    fun md_deep_purple_900(): Int { return res(R.color.md_deep_purple_900) }
    fun md_deep_purple_A100(): Int { return res(R.color.md_deep_purple_A100) }
    fun md_deep_purple_A200(): Int { return res(R.color.md_deep_purple_A200) }
    fun md_deep_purple_A400(): Int { return res(R.color.md_deep_purple_A400) }
    fun md_deep_purple_A700(): Int { return res(R.color.md_deep_purple_A700) }
    fun md_indigo_50(): Int { return res(R.color.md_indigo_50) }
    fun md_indigo_100(): Int { return res(R.color.md_indigo_100) }
    fun md_indigo_200(): Int { return res(R.color.md_indigo_200) }
    fun md_indigo_300(): Int { return res(R.color.md_indigo_300) }
    fun md_indigo_400(): Int { return res(R.color.md_indigo_400) }
    fun md_indigo_500(): Int { return res(R.color.md_indigo_500) }
    fun md_indigo_600(): Int { return res(R.color.md_indigo_600) }
    fun md_indigo_700(): Int { return res(R.color.md_indigo_700) }
    fun md_indigo_800(): Int { return res(R.color.md_indigo_800) }
    fun md_indigo_900(): Int { return res(R.color.md_indigo_900) }
    fun md_indigo_A100(): Int { return res(R.color.md_indigo_A100) }
    fun md_indigo_A200(): Int { return res(R.color.md_indigo_A200) }
    fun md_indigo_A400(): Int { return res(R.color.md_indigo_A400) }
    fun md_indigo_A700(): Int { return res(R.color.md_indigo_A700) }
    fun md_blue_50(): Int { return res(R.color.md_blue_50) }
    fun md_blue_100(): Int { return res(R.color.md_blue_100) }
    fun md_blue_200(): Int { return res(R.color.md_blue_200) }
    fun md_blue_300(): Int { return res(R.color.md_blue_300) }
    fun md_blue_400(): Int { return res(R.color.md_blue_400) }
    fun md_blue_500(): Int { return res(R.color.md_blue_500) }
    fun md_blue_600(): Int { return res(R.color.md_blue_600) }
    fun md_blue_700(): Int { return res(R.color.md_blue_700) }
    fun md_blue_800(): Int { return res(R.color.md_blue_800) }
    fun md_blue_900(): Int { return res(R.color.md_blue_900) }
    fun md_blue_A100(): Int { return res(R.color.md_blue_A100) }
    fun md_blue_A200(): Int { return res(R.color.md_blue_A200) }
    fun md_blue_A400(): Int { return res(R.color.md_blue_A400) }
    fun md_blue_A700(): Int { return res(R.color.md_blue_A700) }
    fun md_light_blue_50(): Int { return res(R.color.md_light_blue_50) }
    fun md_light_blue_100(): Int { return res(R.color.md_light_blue_100) }
    fun md_light_blue_200(): Int { return res(R.color.md_light_blue_200) }
    fun md_light_blue_300(): Int { return res(R.color.md_light_blue_300) }
    fun md_light_blue_400(): Int { return res(R.color.md_light_blue_400) }
    fun md_light_blue_500(): Int { return res(R.color.md_light_blue_500) }
    fun md_light_blue_600(): Int { return res(R.color.md_light_blue_600) }
    fun md_light_blue_700(): Int { return res(R.color.md_light_blue_700) }
    fun md_light_blue_800(): Int { return res(R.color.md_light_blue_800) }
    fun md_light_blue_900(): Int { return res(R.color.md_light_blue_900) }
    fun md_light_blue_A100(): Int { return res(R.color.md_light_blue_A100) }
    fun md_light_blue_A200(): Int { return res(R.color.md_light_blue_A200) }
    fun md_light_blue_A400(): Int { return res(R.color.md_light_blue_A400) }
    fun md_light_blue_A700(): Int { return res(R.color.md_light_blue_A700) }
    fun md_cyan_50(): Int { return res(R.color.md_cyan_50) }
    fun md_cyan_100(): Int { return res(R.color.md_cyan_100) }
    fun md_cyan_200(): Int { return res(R.color.md_cyan_200) }
    fun md_cyan_300(): Int { return res(R.color.md_cyan_300) }
    fun md_cyan_400(): Int { return res(R.color.md_cyan_400) }
    fun md_cyan_500(): Int { return res(R.color.md_cyan_500) }
    fun md_cyan_600(): Int { return res(R.color.md_cyan_600) }
    fun md_cyan_700(): Int { return res(R.color.md_cyan_700) }
    fun md_cyan_800(): Int { return res(R.color.md_cyan_800) }
    fun md_cyan_900(): Int { return res(R.color.md_cyan_900) }
    fun md_cyan_A100(): Int { return res(R.color.md_cyan_A100) }
    fun md_cyan_A200(): Int { return res(R.color.md_cyan_A200) }
    fun md_cyan_A400(): Int { return res(R.color.md_cyan_A400) }
    fun md_cyan_A700(): Int { return res(R.color.md_cyan_A700) }
    fun md_teal_50(): Int { return res(R.color.md_teal_50) }
    fun md_teal_100(): Int { return res(R.color.md_teal_100) }
    fun md_teal_200(): Int { return res(R.color.md_teal_200) }
    fun md_teal_300(): Int { return res(R.color.md_teal_300) }
    fun md_teal_400(): Int { return res(R.color.md_teal_400) }
    fun md_teal_500(): Int { return res(R.color.md_teal_500) }
    fun md_teal_600(): Int { return res(R.color.md_teal_600) }
    fun md_teal_700(): Int { return res(R.color.md_teal_700) }
    fun md_teal_800(): Int { return res(R.color.md_teal_800) }
    fun md_teal_900(): Int { return res(R.color.md_teal_900) }
    fun md_teal_A100(): Int { return res(R.color.md_teal_A100) }
    fun md_teal_A200(): Int { return res(R.color.md_teal_A200) }
    fun md_teal_A400(): Int { return res(R.color.md_teal_A400) }
    fun md_teal_A700(): Int { return res(R.color.md_teal_A700) }
    fun md_green_50(): Int { return res(R.color.md_green_50) }
    fun md_green_100(): Int { return res(R.color.md_green_100) }
    fun md_green_200(): Int { return res(R.color.md_green_200) }
    fun md_green_300(): Int { return res(R.color.md_green_300) }
    fun md_green_400(): Int { return res(R.color.md_green_400) }
    fun md_green_500(): Int { return res(R.color.md_green_500) }
    fun md_green_600(): Int { return res(R.color.md_green_600) }
    fun md_green_700(): Int { return res(R.color.md_green_700) }
    fun md_green_800(): Int { return res(R.color.md_green_800) }
    fun md_green_900(): Int { return res(R.color.md_green_900) }
    fun md_green_A100(): Int { return res(R.color.md_green_A100) }
    fun md_green_A200(): Int { return res(R.color.md_green_A200) }
    fun md_green_A400(): Int { return res(R.color.md_green_A400) }
    fun md_green_A700(): Int { return res(R.color.md_green_A700) }
    fun md_light_green_50(): Int { return res(R.color.md_light_green_50) }
    fun md_light_green_100(): Int { return res(R.color.md_light_green_100) }
    fun md_light_green_200(): Int { return res(R.color.md_light_green_200) }
    fun md_light_green_300(): Int { return res(R.color.md_light_green_300) }
    fun md_light_green_400(): Int { return res(R.color.md_light_green_400) }
    fun md_light_green_500(): Int { return res(R.color.md_light_green_500) }
    fun md_light_green_600(): Int { return res(R.color.md_light_green_600) }
    fun md_light_green_700(): Int { return res(R.color.md_light_green_700) }
    fun md_light_green_800(): Int { return res(R.color.md_light_green_800) }
    fun md_light_green_900(): Int { return res(R.color.md_light_green_900) }
    fun md_light_green_A100(): Int { return res(R.color.md_light_green_A100) }
    fun md_light_green_A200(): Int { return res(R.color.md_light_green_A200) }
    fun md_light_green_A400(): Int { return res(R.color.md_light_green_A400) }
    fun md_light_green_A700(): Int { return res(R.color.md_light_green_A700) }
    fun md_lime_50(): Int { return res(R.color.md_lime_50) }
    fun md_lime_100(): Int { return res(R.color.md_lime_100) }
    fun md_lime_200(): Int { return res(R.color.md_lime_200) }
    fun md_lime_300(): Int { return res(R.color.md_lime_300) }
    fun md_lime_400(): Int { return res(R.color.md_lime_400) }
    fun md_lime_500(): Int { return res(R.color.md_lime_500) }
    fun md_lime_600(): Int { return res(R.color.md_lime_600) }
    fun md_lime_700(): Int { return res(R.color.md_lime_700) }
    fun md_lime_800(): Int { return res(R.color.md_lime_800) }
    fun md_lime_900(): Int { return res(R.color.md_lime_900) }
    fun md_lime_A100(): Int { return res(R.color.md_lime_A100) }
    fun md_lime_A200(): Int { return res(R.color.md_lime_A200) }
    fun md_lime_A400(): Int { return res(R.color.md_lime_A400) }
    fun md_lime_A700(): Int { return res(R.color.md_lime_A700) }
    fun md_yellow_50(): Int { return res(R.color.md_yellow_50) }
    fun md_yellow_100(): Int { return res(R.color.md_yellow_100) }
    fun md_yellow_200(): Int { return res(R.color.md_yellow_200) }
    fun md_yellow_300(): Int { return res(R.color.md_yellow_300) }
    fun md_yellow_400(): Int { return res(R.color.md_yellow_400) }
    fun md_yellow_500(): Int { return res(R.color.md_yellow_500) }
    fun md_yellow_600(): Int { return res(R.color.md_yellow_600) }
    fun md_yellow_700(): Int { return res(R.color.md_yellow_700) }
    fun md_yellow_800(): Int { return res(R.color.md_yellow_800) }
    fun md_yellow_900(): Int { return res(R.color.md_yellow_900) }
    fun md_yellow_A100(): Int { return res(R.color.md_yellow_A100) }
    fun md_yellow_A200(): Int { return res(R.color.md_yellow_A200) }
    fun md_yellow_A400(): Int { return res(R.color.md_yellow_A400) }
    fun md_yellow_A700(): Int { return res(R.color.md_yellow_A700) }
    fun md_amber_50(): Int { return res(R.color.md_amber_50) }
    fun md_amber_100(): Int { return res(R.color.md_amber_100) }
    fun md_amber_200(): Int { return res(R.color.md_amber_200) }
    fun md_amber_300(): Int { return res(R.color.md_amber_300) }
    fun md_amber_400(): Int { return res(R.color.md_amber_400) }
    fun md_amber_500(): Int { return res(R.color.md_amber_500) }
    fun md_amber_600(): Int { return res(R.color.md_amber_600) }
    fun md_amber_700(): Int { return res(R.color.md_amber_700) }
    fun md_amber_800(): Int { return res(R.color.md_amber_800) }
    fun md_amber_900(): Int { return res(R.color.md_amber_900) }
    fun md_amber_A100(): Int { return res(R.color.md_amber_A100) }
    fun md_amber_A200(): Int { return res(R.color.md_amber_A200) }
    fun md_amber_A400(): Int { return res(R.color.md_amber_A400) }
    fun md_amber_A700(): Int { return res(R.color.md_amber_A700) }
    fun md_orange_50(): Int { return res(R.color.md_orange_50) }
    fun md_orange_100(): Int { return res(R.color.md_orange_100) }
    fun md_orange_200(): Int { return res(R.color.md_orange_200) }
    fun md_orange_300(): Int { return res(R.color.md_orange_300) }
    fun md_orange_400(): Int { return res(R.color.md_orange_400) }
    fun md_orange_500(): Int { return res(R.color.md_orange_500) }
    fun md_orange_600(): Int { return res(R.color.md_orange_600) }
    fun md_orange_700(): Int { return res(R.color.md_orange_700) }
    fun md_orange_800(): Int { return res(R.color.md_orange_800) }
    fun md_orange_900(): Int { return res(R.color.md_orange_900) }
    fun md_orange_A100(): Int { return res(R.color.md_orange_A100) }
    fun md_orange_A200(): Int { return res(R.color.md_orange_A200) }
    fun md_orange_A400(): Int { return res(R.color.md_orange_A400) }
    fun md_orange_A700(): Int { return res(R.color.md_orange_A700) }
    fun md_deep_orange_50(): Int { return res(R.color.md_deep_orange_50) }
    fun md_deep_orange_100(): Int { return res(R.color.md_deep_orange_100) }
    fun md_deep_orange_200(): Int { return res(R.color.md_deep_orange_200) }
    fun md_deep_orange_300(): Int { return res(R.color.md_deep_orange_300) }
    fun md_deep_orange_400(): Int { return res(R.color.md_deep_orange_400) }
    fun md_deep_orange_500(): Int { return res(R.color.md_deep_orange_500) }
    fun md_deep_orange_600(): Int { return res(R.color.md_deep_orange_600) }
    fun md_deep_orange_700(): Int { return res(R.color.md_deep_orange_700) }
    fun md_deep_orange_800(): Int { return res(R.color.md_deep_orange_800) }
    fun md_deep_orange_900(): Int { return res(R.color.md_deep_orange_900) }
    fun md_deep_orange_A100(): Int { return res(R.color.md_deep_orange_A100) }
    fun md_deep_orange_A200(): Int { return res(R.color.md_deep_orange_A200) }
    fun md_deep_orange_A400(): Int { return res(R.color.md_deep_orange_A400) }
    fun md_deep_orange_A700(): Int { return res(R.color.md_deep_orange_A700) }
    fun md_brown_50(): Int { return res(R.color.md_brown_50) }
    fun md_brown_100(): Int { return res(R.color.md_brown_100) }
    fun md_brown_200(): Int { return res(R.color.md_brown_200) }
    fun md_brown_300(): Int { return res(R.color.md_brown_300) }
    fun md_brown_400(): Int { return res(R.color.md_brown_400) }
    fun md_brown_500(): Int { return res(R.color.md_brown_500) }
    fun md_brown_600(): Int { return res(R.color.md_brown_600) }
    fun md_brown_700(): Int { return res(R.color.md_brown_700) }
    fun md_brown_800(): Int { return res(R.color.md_brown_800) }
    fun md_brown_900(): Int { return res(R.color.md_brown_900) }
    fun md_grey_50(): Int { return res(R.color.md_grey_50) }
    fun md_grey_100(): Int { return res(R.color.md_grey_100) }
    fun md_grey_200(): Int { return res(R.color.md_grey_200) }
    fun md_grey_300(): Int { return res(R.color.md_grey_300) }
    fun md_grey_400(): Int { return res(R.color.md_grey_400) }
    fun md_grey_500(): Int { return res(R.color.md_grey_500) }
    fun md_grey_600(): Int { return res(R.color.md_grey_600) }
    fun md_grey_700(): Int { return res(R.color.md_grey_700) }
    fun md_grey_800(): Int { return res(R.color.md_grey_800) }
    fun md_grey_900(): Int { return res(R.color.md_grey_900) }
    fun md_black_1000(): Int { return res(R.color.md_black_1000) }
    fun md_black(): Int { return res(R.color.md_black_1000) }
    fun md_white_1000(): Int { return res(R.color.md_white_1000) }
    fun white(): Int { return res(R.color.white) }
    fun md_blue_grey_50(): Int { return res(R.color.md_blue_grey_50) }
    fun md_blue_grey_100(): Int { return res(R.color.md_blue_grey_100) }
    fun md_blue_grey_200(): Int { return res(R.color.md_blue_grey_200) }
    fun md_blue_grey_300(): Int { return res(R.color.md_blue_grey_300) }
    fun md_blue_grey_400(): Int { return res(R.color.md_blue_grey_400) }
    fun md_blue_grey_500(): Int { return res(R.color.md_blue_grey_500) }
    fun md_blue_grey_600(): Int { return res(R.color.md_blue_grey_600) }
    fun md_blue_grey_700(): Int { return res(R.color.md_blue_grey_700) }
    fun md_blue_grey_800(): Int { return res(R.color.md_blue_grey_800) }
    fun md_blue_grey_900(): Int { return res(R.color.md_blue_grey_900) }
    fun md_white_10(): Int { return res(R.color.md_white_10) }
    fun md_white_20(): Int { return res(R.color.md_white_20) }
    fun md_white_30(): Int { return res(R.color.md_white_30) }
    fun md_white_40(): Int { return res(R.color.md_white_40) }
    fun md_white_50(): Int { return res(R.color.md_white_50) }
    fun md_white_60(): Int { return res(R.color.md_white_60) }
    fun md_white_70(): Int { return res(R.color.md_white_70) }
    fun md_white_80(): Int { return res(R.color.md_white_80) }
    fun md_white_90(): Int { return res(R.color.md_white_90) }
    fun md_black_20(): Int { return res(R.color.md_black_20) }
    fun md_black_30(): Int { return res(R.color.md_black_30) }
    fun md_black_50(): Int { return res(R.color.md_black_50) }
    fun md_black_60(): Int { return res(R.color.md_black_60) }
    fun md_black_70(): Int { return res(R.color.md_black_70) }
    fun md_black_80(): Int { return res(R.color.md_black_80) }
    fun md_black_90(): Int { return res(R.color.md_black_90) }
    fun transparent(): Int { return res(R.color.transparent) }


}