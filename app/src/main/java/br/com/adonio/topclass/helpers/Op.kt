package br.com.adonio.topclass.helpers

class Op<T>(val value: T) {

}

fun <T> op(v: T) = Op(v)