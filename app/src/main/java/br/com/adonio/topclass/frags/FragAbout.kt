package br.com.adonio.topclass.frags

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.MainActivity
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Http
import kotlinx.android.synthetic.main.frag_about.*

class FragAbout: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_about, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_version?.text = "Versão ${Act.versionName()}"

        ui_btn_pros?.setOnClickListener {
            MainActivity.navigatePro(findNavController())
        }

        ui_btn_proposal?.setOnClickListener {
            MainActivity.navigateProposal(findNavController())
        }

        ui_btn_transaction?.setOnClickListener {
            MainActivity.navigateTransaction(findNavController())
        }

        ui_btn_contract?.setOnClickListener {
            MainActivity.navigateContract(findNavController())
        }
    }

}