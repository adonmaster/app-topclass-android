package br.com.adonio.topclass.frags.pro

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.concat
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.repo.Repo
import br.com.adonio.topclass.structs.ProposalBody
import br.com.adonio.topclass.structs.SimpleCard
import kotlinx.android.synthetic.main.__empty.*
import kotlinx.android.synthetic.main.frag_pro_proposal_new_card.*
import org.threeten.bp.LocalDateTime
import kotlin.math.max

class FragProProposalNewCard: Fragment() {

    private lateinit var vm: FragProProposalNewCardVM
    private lateinit var vmParent: FragProProposalNewVM
    private var mData: SimpleCard? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro_proposal_new_card, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragProProposalNewCardVM::class.java)
        vmParent = ViewModelProviders.of(activity!!).get(FragProProposalNewVM::class.java)

        setupObject()

        vm.loading.observe(this, Observer {
            ui_progress.isVisible = it
            ui_btn_edit.setEnabledEx(!it)
            ui_btn_next.setEnabledEx(!it)
        })

        //
        mData = SimpleCard.parse(Repo.pref.get("card@json"))

        //
        ui_empty.isVisible = mData==null
        ui_empty_description.text = "Sem cartão cadastrado.\nFaça cadastro apertando o botão abaixo."

        //
        ui_wrapper_card.isVisible = mData!=null

        //
        mData?.let { d ->
            ui_card_no.text = d.p.no
            ui_card_valid.text = d.validMonth + " / " + d.validYear
            ui_card_secret.text = d.p.secret
            ui_card_name.text = d.name
        }

        ui_btn_edit.text = if (mData==null) "Adicionar cartão" else "Alterar cartão"
        ui_btn_edit.setOnClickListener {
            findNavController().navigate(R.id.sg_to_card_edit)
        }

        ui_btn_next.isVisible = mData != null
        ui_btn_next.setOnClickListener {
            requestCardToken()
        }
    }

    private fun requestCardToken() {
        val d = mData ?: return
        synced("FragProProposalNewCard@requestCardToken") {

            val params = HttpFormData()
                .put("no", d.no)
                .put("valid_month", d.validMonth)
                .put("valid_year", d.validYear)
                .put("secret", d.secret)
                .put("name", d.name)

            vm.loading.value = true
            Http.post("proposals/card-token", params)
                .then {
                    JEx(it) {
                        o("payload") {
                            afterRequestCardToken(gStr("token"))
                        }
                    }
                }
                .catch {
                    T.error(activity!!, it)
                }
                .always {
                    vm.loading.value = false
                    syncRelease()
                }
        }
    }

    private fun afterRequestCardToken(token: String) {
        vmParent.cardToken = token

        Task.main(100) { requestProposal() }
    }

    private fun requestProposal() {
        synced("FraProProposalNewCard@requestProposal") {

            val jsonBody = ProposalBody.toJson(vmParent.items.value!!, vmParent.address.value!!)
            val params = HttpFormData()
                .put("pro_user_id", vmParent.proId)
                .put("body", jsonBody)
                .put("customer_name", mData!!.name)
                .put("card_token", vmParent.cardToken!!)
                .put("price", vmParent.totalValue)
                .put("due_at", generateDueAt(vmParent.items.value!!))

            vm.loading.value = true
            Http.post("proposals", params)
                .then {

                    afterRequestProposal()

                }
                .catch {
                    T.info(activity!!, it)
                }
                .always {
                    vm.loading.value = false
                    syncRelease()
                }
        }
    }

    /**
     * Due at will be the last item date, PLUS 2 hours
     */
    private fun generateDueAt(items: List<ProposalBody.Item>): String {
        val dts = items.map { it.dt }
        val lastDate = dts.fold(dts.first(), { acc, i ->
            if (acc > i) acc else i
        })
        val finalDate = lastDate.plusHours(2)
        return Dt.formatSqlDate(finalDate)!!
    }

    private fun afterRequestProposal() {
        findNavController().navigate(R.id.sg_to_done)
    }

    @SuppressLint("SetTextI18n")
    private fun setupObject() {
        val items = vmParent.items.value!!

        // total value
        val totalMins = items
            .map { it.duration }
            .fold(0, { acc, v -> acc + v })
        val total = vmParent.priceForHour * totalMins / 60
        vmParent.totalValue = total

        //
        ui_object.text = "Total: "
            .concat(Str.plural(items.count(), "", "1x aula: ", "$0x aulas: "))
            .concat("R$ " + Str.number(total))
            .concat(" ($totalMins minutos)")
    }

}