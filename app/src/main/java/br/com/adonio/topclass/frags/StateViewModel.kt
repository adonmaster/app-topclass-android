package br.com.adonio.topclass.frags

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class StateViewModel: ViewModel() {

    val loading = MutableLiveData<Int>().also { it.value = 0 }
    fun loadingInc(factor: Int) { loading.value = (loading.value ?: 0) + factor }

}