package br.com.adonio.topclass.frags.about

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Task
import kotlinx.android.synthetic.main.frag_login_intro.*

class FragLoginIntro: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_login_intro, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_version.text = "Versão ${Act.versionName()}"

        App.shared.clearUser()
    }

    override fun onStart() {
        super.onStart()

        Task.main(3500) {
            findNavController().navigate(R.id.sg_to_login_email)
        }
    }

}