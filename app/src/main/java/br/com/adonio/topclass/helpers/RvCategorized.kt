package br.com.adonio.topclass.helpers

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.dialogs.DialogOk
import java.lang.ref.WeakReference

class RvCategorized<T>(
    _owner: Owner<T>,
    private val identifier: String = "",
    @LayoutRes private val idData: Int,
    @LayoutRes private val idCategory: Int,
    val categorizer: (T)->String
) : RecyclerView.Adapter<RvCategorized<T>.VH>() {

    private val ownerInstance = WeakReference(_owner)
    private val owner
        get() = ownerInstance.get()!!

    private var mData = listOf<Row<T>>()

    //

    override fun getItemViewType(position: Int): Int {
        return if (mData[position].hasValue()) 1 else 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val id = if (viewType == 1) idData else idCategory
        return VH(Act.inflate(id, parent, false)!!)
    }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val r = mData[position]
        if (r.hasValue()) {
            owner.rvBindData(identifier, holder.itemView, r.value!!)
        } else {
            owner.rvBindCategory(identifier, holder.itemView, r.category!!)
        }
    }

    //

    @SuppressLint("DefaultLocale")
    fun updateData(list: List<T>) {
        val fakeHash = arrayListOf<Pair<String, ArrayList<T>>>()

        // kind of hashMap
        for (i in list) {
            val category = categorizer(i)
            val ii = fakeHash.indexOfFirst { it.first.trim().toLowerCase() == category.trim().toLowerCase() }
            if (ii >= 0) {
                fakeHash[ii].second.add(i)
            } else {
                fakeHash.add(category to arrayListOf(i))
            }
        }

        // flatten
        val rr = arrayListOf<Row<T>>()
        for (i in fakeHash) {
            rr.add(Row(i.first, null))
            for (v in i.second) {
                rr.add(Row(null, v))
            }
        }

        //
        mData = rr
        notifyDataSetChanged()
    }

    //
    class Row<V>(val category: String?, val value: V?) {
        fun hasValue() = value != null
    }

    //
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            owner.rvCreateViewBothViews(
                identifier, itemView, { adapterPosition },
                { mData[adapterPosition].category },
                { mData[adapterPosition].value }
            )
        }
    }

    //
    interface Owner<V> {
        fun rvCreateViewBothViews(identifier: String, view: View, position: ()->Int, category: ()->String?, data: ()->V?)
        fun rvBindData(identifier: String, view: View, value: V)
        fun rvBindCategory(identifier: String, view: View, value: String)
    }

}