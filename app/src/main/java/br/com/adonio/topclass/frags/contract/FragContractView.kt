package br.com.adonio.topclass.frags.contract

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.BaseFragMapView
import br.com.adonio.topclass.frags.FragContractsVM
import br.com.adonio.topclass.helpers.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.frag_contract_view.*

class FragContractView: BaseFragMapView() {

    //
    private lateinit var vmParent: FragContractsVM

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return baseOnCreateView(R.layout.frag_contract_view, R.id.ui_map, inflater, container, savedInstanceState)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vmParent = ViewModelProviders.of(activity!!).get(FragContractsVM::class.java)
        val m = vmParent.selected!!

        //
        ui_id.text = "Contrato n.o ${m.id}\nCriado ${Dt.fromNow(m.createdAt)}"

        // pro
        Img.loadAvatar(m.proAvatar, ui_pro_avatar)
        ui_pro_name.text = m.proName

        // client
        Img.loadAvatar(m.clientAvatar, ui_client_avatar)
        ui_client_name.text = m.clientName

        //
        val body = m.p.body!! // <--

        // classes title
        ui_title_aulas.text = Str
            .plural(body.items.count(), "3. Aula (nenhuma)", "3. Aula (1)") { "3. Aulas ($it)" }

        // classes
        ui_classes_desc.text = body.items.mapIndexed { i, item ->
            val dt = Dt.formatBr(item.dt)
            val dtHuman = Dt.fromNow(item.dt)
            val duration = item.duration
            val materia = item.materia
            Span("${i+1}. ").bold().concat("$dt ($dtHuman)")
                .concat("\n$materia")
                .concat("\nDuração: $duration mins").color(context!!, R.color.md_grey_500)
                .build()
        }.joinToString("\n\n")

        // location
        ui_location_desc.text = body.address.address
        Str.whenNotEmpty(body.address.comp) {
            ui_location_desc.append("\n\n${it}")
        }

        // map
        ui_map.getMapAsync { g ->
            val lat = body.address.lat
            val lng = body.address.lng
            g.addMarker(
                MarkerOptions()
                    .position(LatLng(lat, lng))
                    .title(m.clientName)
            )
            g.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15.0f))
        }

        //
        ui_price_desc.text = "R$ " + Str.number(m.price)

        //
        val dueDt = Dt.formatBr(m.due_at)!!
        var dueDesc = ""
        var dueCl = 0
        if (m.was_executed) {
            dueDesc = "O contrato foi executado no dia $dueDt"
            dueCl = Cl.md_green_500()
        } else {
            dueDesc = "Se nenhuma objeção for encontrada, o contrato será executado em $dueDt, e o valor transferido do aluno para o profissional."
            dueCl = Cl.md_orange_500()
        }
        ui_due_desc.text = dueDesc
        ui_due_desc.setTextColor(dueCl)
        ui_due_icon.setTextColor(dueCl)
    }

}