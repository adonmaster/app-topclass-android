package br.com.adonio.topclass.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import br.com.adonio.topclass.models.*

object DB {

    @Database(entities = [
        User::class, ConfigModel::class
    ], version = 5)

    @TypeConverters(DBConverters::class)
    abstract class AppDatabase: RoomDatabase() {
        abstract val user: UserDao
        abstract val config: ConfigModelDao
    }

    lateinit var i: AppDatabase

    fun config(context: Context)
    {
        i = Room.databaseBuilder(context, AppDatabase::class.java, "main.room")
            .fallbackToDestructiveMigration()
            .apply { migrate(this) }
            .allowMainThreadQueries()
            .build()
    }

    private fun migrate(db: RoomDatabase.Builder<AppDatabase>) {
        db.apply {
            addMigrations(migrationMaker(0, 1) {
                it.execSQL("")
            })
        }
    }

    private fun migrationMaker(from: Int, to: Int, cb: (SupportSQLiteDatabase)->Unit): Migration {
        return object: Migration(from, to) {
            override fun migrate(database: SupportSQLiteDatabase) {
                cb(database)
            }
        }
    }

    fun seed() {
    }
}