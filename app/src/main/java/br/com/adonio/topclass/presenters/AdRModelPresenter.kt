@file:Suppress("MemberVisibilityCanBePrivate")

package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.rmodels.AdRModel
import br.com.adonio.topclass.rmodels.ContractRModel
import br.com.adonio.topclass.structs.ProposalBody

class AdRModelPresenter(model: AdRModel) : BasePresenter<AdRModel>(model) {

    val body by lazy { ProposalBody.parse(model.body) }

    fun isUserRelated(userId: Long?): Boolean {
        return isUserClient(userId) || isUserSomePro(userId)
    }

    fun isUserClient(userId: Long?): Boolean {
        return model.client_user_id.toLong() == userId
    }

    fun isUserSomePro(userId: Long?): Boolean {
        return findProFrom(userId) != null
    }

    private fun findProFrom(userId: Long?) = model.pros.find { it.id.toLong() == userId }

    fun findPriceProFrom(userId: Long?, def: Double): Double {
        return findProFrom(userId)?.price ?: def
    }

}