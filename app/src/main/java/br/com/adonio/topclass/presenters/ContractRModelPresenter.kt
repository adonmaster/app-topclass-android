package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.rmodels.ContractRModel
import br.com.adonio.topclass.structs.ProposalBody

class ContractRModelPresenter(model: ContractRModel) : BasePresenter<ContractRModel>(model) {

    val body by lazy { ProposalBody.parse(model.body) }

}