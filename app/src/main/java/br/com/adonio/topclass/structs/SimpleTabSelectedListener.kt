package br.com.adonio.topclass.structs

import com.google.android.material.tabs.TabLayout

abstract class SimpleTabSelectedListener: TabLayout.BaseOnTabSelectedListener<TabLayout.Tab> {
    override fun onTabReselected(p0: TabLayout.Tab?) {
    }
    override fun onTabUnselected(p0: TabLayout.Tab?) {
    }
    override fun onTabSelected(p0: TabLayout.Tab?) {
    }
}