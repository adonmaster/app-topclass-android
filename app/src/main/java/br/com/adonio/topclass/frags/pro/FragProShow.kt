package br.com.adonio.topclass.frags.pro

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.frags.FragBlank
import br.com.adonio.topclass.frags.FragProVM
import br.com.adonio.topclass.helpers.Img
import br.com.adonio.topclass.structs.Cs
import kotlinx.android.synthetic.main.frag_pro_show.*

class FragProShow: Fragment() {

    // state
    private lateinit var vmParent: FragProVM

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro_show, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vmParent = ViewModelProviders.of(activity!!).get(FragProVM::class.java)

        val m = vmParent.selected.value ?: return
        // --

        Img.loadAvatar(m.avatarThumb, ui_avatar)
        ui_name?.text = m.name
        ui_desc1?.text = m.email

        val priceDesc = m.p.skillPriceDesc()
        ui_price?.isVisible = priceDesc != null
        if (priceDesc!=null) ui_price?.text = "$priceDesc / hora"

        // pager
        ui_pager.adapter = object: FragmentStatePagerAdapter(childFragmentManager) {
            override fun getCount(): Int {
                return 2
            }
            override fun getPageTitle(position: Int): CharSequence? {
                return when(position) {
                    0 -> "Habilidades"
                    1 -> "Horários"
                    else -> null
                }
            }
            override fun getItem(position: Int): Fragment {
                return when(position) {
                    0 -> FragProShowSkills()
                    1 -> FragProShowDays()
                    else -> FragBlank()
                }
            }
        }
        ui_tab_layout.setupWithViewPager(ui_pager)

        //
        ui_btn_hire.setOnClickListener {
            hire()
        }
    }

    private fun hire() {
        val user = vmParent.selected.value ?: return

        val priceDesc = user.p.skillPriceDesc()

        if (priceDesc==null) {

            val message = "Não é possível contratar o serviço, pois o profissional ainda não designou preço da hora/aula."
            DialogOk.show(context, message, "Atenção")

        } else {

            val classes = user.p.skillsRoot
                .children()
                .firstOrNull { it.name.startsWith("Habili", true) }
                ?.children()
                ?.map { it.name } ?: listOf()

            val horarios = arrayListOf<String>()
            user.p.skillsRoot
                .children()
                .find { it.name == Cs.SKILL_CATEGORY_HORARIOS }
                ?.children()
                ?.forEach { lday ->
                    lday.children().forEach { lturno ->
                        horarios.add("${lday.name}/${lturno.name}")
                    }
                }

            FragProProposalNew.navigate(
                this,
                R.id.sg_to_pro_proposal_new,
                user.id,
                classes,
                horarios,
                user.p.skillPriceValue(),
                user.name,
                user.email,
                user.avatarThumb
            )

        }
    }


    // ui

    override fun onResume() {
        super.onResume()

        activity?.title = vmParent.selected.value?.name ?: "Professor"
    }

}