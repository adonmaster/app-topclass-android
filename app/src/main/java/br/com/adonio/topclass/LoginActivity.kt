package br.com.adonio.topclass

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders

class LoginActivity : AppCompatActivity() {

    private lateinit var vm: LoginActivityVM

    // static
    companion object {
        fun start(activity: Activity) {
            val intent = Intent(activity, LoginActivity::class.java)
            activity.startActivityForResult(intent, 551)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        vm = ViewModelProviders.of(this).get(LoginActivityVM::class.java)
    }

}
