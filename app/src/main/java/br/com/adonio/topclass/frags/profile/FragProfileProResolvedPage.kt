package br.com.adonio.topclass.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.profile.adapters.FragProfileProResolvedAdapter
import br.com.adonio.topclass.helpers.Leaf
import br.com.adonio.topclass.rmodels.SkillsRModel
import kotlinx.android.synthetic.main.frag_profile_pro_resolved_page.*

class FragProfileProResolvedPage: Fragment() {

    // static
    companion object {
        fun f(category: String): FragProfileProResolvedPage {
            val bundle = Bundle()
            bundle.putString("category", category)
            val f = FragProfileProResolvedPage()
            f.arguments = bundle
            return f
        }
    }

    // state
    private lateinit var vm: FragProfileProResolvedVM
    private lateinit var mAdapter: FragProfileProResolvedAdapter

    // computed
    private val cCategory get() = arguments!!.getString("category")!!

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_profile_pro_resolved_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragProfileProResolvedVM::class.java)

        mAdapter = FragProfileProResolvedAdapter(object :
            FragProfileProResolvedAdapter.Listener {
            override fun onChecked(model: SkillsRModel, position: Int) {
                FragProfileProResolved.shared?.outOnCheckedModel(model)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)

        vm.root.observe(this, Observer {
            updateData(it)
        })
    }

    private fun updateData(root: Leaf<SkillsRModel>?) {
        val list = root?.children()
            ?.first { it.name == cCategory }
            ?: return

        mAdapter.updateData(list.toLines().drop(1))
    }

}