package br.com.adonio.topclass.processors

import android.util.Log
import br.com.adonio.topclass.App
import br.com.adonio.topclass.helpers.Http
import br.com.adonio.topclass.helpers.HttpFormData
import br.com.adonio.topclass.repo.Repo
import com.google.firebase.iid.FirebaseInstanceId

object FcmProcessor {

    private const val KEY_TOKEN_PENDING = "FcmProcessor@tokenPending"
    private const val KEY_TOKEN = "FcmProcessor@token"

    fun setToken(token: String?) {
        if (token == null) return
        Repo.pref.put(KEY_TOKEN_PENDING, token)
        upload()
    }

    private fun storedToken() = Repo.pref.get(KEY_TOKEN_PENDING) ?: Repo.pref.get(KEY_TOKEN)

    private var mUploading = false
    @Synchronized
    fun upload() {
        // sync
        if (mUploading) return

        // pending token has to be in order
        val pendingToken = Repo.pref.get(KEY_TOKEN_PENDING) ?: return

        // registered user?
        if (App.user == null) return

        // send it twice? nop!
        val sentToken = Repo.pref.get(KEY_TOKEN)
        if (sentToken == pendingToken) return

        mUploading = true
        val params = HttpFormData().put("gcm", "fcm").put("token", pendingToken)
        Http.post("gcm", params)
            .then {
                mUploading = false
                Repo.pref.put(KEY_TOKEN, pendingToken)
            }
            .catch {
                Log.i("Adon", it)
            }
            .always {  }
    }

    fun mainActivityStart() {
        if (storedToken()==null) {
            FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener { task ->
                    if (!task.isSuccessful) return@addOnCompleteListener
                    setToken(task.result?.token)
                }
        } else {
            upload()
        }
    }

}