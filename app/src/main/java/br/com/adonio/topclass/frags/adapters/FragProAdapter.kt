package br.com.adonio.topclass.frags.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Img
import br.com.adonio.topclass.helpers.RVContainer
import br.com.adonio.topclass.rmodels.ProQueryRModel
import kotlinx.android.synthetic.main.rv_frag_pro.view.*
import kotlinx.android.synthetic.main.rv_frag_pro_header.view.*

class FragProAdapter(private val listener: Listener)
    : RecyclerView.Adapter<FragProAdapter.VH>() {

    @SuppressLint("DefaultLocale")
    private var mData = RVContainer<ProQueryRModel>(null) {
        it.name.first().toString().toUpperCase()
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position]?.getType() ?: -1
    }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val id = if (mData.isTypeData(viewType)) R.layout.rv_frag_pro else R.layout.rv_frag_pro_header
        val v = Act.inflate(id, parent, false)!!
        return VH(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        mData[position]
            ?.whenCategory {
                holder.uiHeaderTitle?.text = it
            }
            ?.whenData { m ->
                Img.loadAvatar(m.avatarThumb, holder.uiAvatar)
                holder.uiTitle?.text = m.name

                val priceDesc = m.p.skillPriceDesc()
                holder.uiPrice?.isVisible = priceDesc != null
                holder.uiPrice?.text = "$priceDesc\nhora"

                holder.uiDesc1?.text = m.email
                holder.uiDesc2?.text = m.p.skillsDescriptionSimple()
            }
    }

    fun updateData(list: List<ProQueryRModel>) {
        mData.update(list)

        notifyDataSetChanged()
    }

    // view holder
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val uiAvatar: ImageView? = itemView.ui_avatar
        val uiTitle: TextView? = itemView.ui_title
        val uiPrice: TextView? = itemView.ui_price
        val uiDesc1: TextView? = itemView.ui_desc1
        val uiDesc2: TextView? = itemView.ui_desc2
        val uiHeaderTitle: TextView? = itemView.ui_header_title

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                mData[position]?.whenData { model ->
                    listener.onSelect(model, position)
                }
            }
        }
    }

    // interface
    interface Listener {
        fun onSelect(model: ProQueryRModel, position: Int)
    }
}