package br.com.adonio.topclass.structs

import br.com.adonio.topclass.helpers.JEncoder
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.SimpleCardPresenter
import org.json.JSONObject

data class SimpleCard(
    val no: String,
    val secret: String,
    val validMonth: String,
    val validYear: String,
    val name: String
) {

    companion object {
        fun parse(s: String?): SimpleCard? {
            if (s==null) return null

            return JParser(JSONObject(s)) {
                store("no")
                store("secret")
                store("valid_month")
                store("valid_year")
                store("name")
            }.parse {
                SimpleCard(getNext(), getNext(), getNext(), getNext(), getNext())
            }
        }
    }

    val p by lazy { SimpleCardPresenter(this) }

    fun toJson(): JSONObject {
        return JEncoder {
            store("no", no)
            store("secret", secret)
            store("valid_month", validMonth)
            store("valid_year", validYear)
            store("name", name)
        }.get()
    }

}