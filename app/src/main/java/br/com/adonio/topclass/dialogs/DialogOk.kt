package br.com.adonio.topclass.dialogs

import android.content.Context
import androidx.appcompat.app.AlertDialog


object DialogOk {

    fun show(context: Context?, message: String, title: String, cancelButton: String?=null, cb: (()->Unit)?=null) {
        val builder = AlertDialog.Builder(context!!)
        builder
            .setMessage(message)
            .setTitle(title)
            .also {
                if (cancelButton!=null) {
                    it.setNegativeButton(cancelButton) { d, _ -> d.dismiss() }
                }
            }
            .setPositiveButton("OK") { _, _ ->
                cb?.invoke()
            }
        val alert = builder.create()
        alert.show()
    }

}
