package br.com.adonio.topclass.repo

object Repo {

    val user by lazy { UserRepo() }
    val pref by lazy { PrefRepo() }
    val global by lazy { GlobalRepo() }
}