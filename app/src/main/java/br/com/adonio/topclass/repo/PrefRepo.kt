package br.com.adonio.topclass.repo

import android.content.Context
import android.content.SharedPreferences
import br.com.adonio.topclass.App

class PrefRepo: GlobalRepo() {

    override val pref: SharedPreferences
        get() = App.shared.getSharedPreferences("main.${App.user?.remote_id ?: 0}.pref", Context.MODE_PRIVATE)

}