package br.com.adonio.topclass.frags.about

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.LoginActivityVM
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.extensions.setVisible
import br.com.adonio.topclass.helpers.Http
import br.com.adonio.topclass.helpers.Span
import br.com.adonio.topclass.helpers.T
import br.com.adonio.topclass.repo.Repo
import br.com.adonio.topclass.rmodels.RegisterCodeRModel
import br.com.adonio.topclass.structs.SimpleTextChangedListener
import kotlinx.android.synthetic.main.frag_login_code.*
import org.json.JSONObject

class FragLoginCode: Fragment() {

    // state
    private lateinit var vm: LoginActivityVM

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_login_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(LoginActivityVM::class.java)

        //
        ui_lbl_desc.text = Span("Enviamos um email para ")
            .concat(vm.email).bold().color(context!!, R.color.md_blue_500)
            .concat("!\nPor favor, confirme o código enviado:")
            .build()


        // ed name
        ui_ed_code.addTextChangedListener(object: SimpleTextChangedListener() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                ui_btn_submit.setEnabledEx(s?.trim()?.isEmpty() == false)
            }
        })
        ui_ed_code.setText("")

        vm.loading.observe(this, Observer { i ->
            val loading = i > 0
            ui_progress.setVisible(loading)
            ui_btn_cancel.setEnabledEx(!loading)
            ui_btn_submit.setEnabledEx(!loading && ui_ed_code.text.toString().isNotEmpty())
        })

        ui_btn_cancel.setOnClickListener {
            findNavController().popBackStack()
        }

        ui_btn_submit.setOnClickListener {
            submit()
        }
    }

    private fun submit() {
        vm.loadingInc(1)

        val params = Http.buildParams()
            .put("email", vm.email)
            .put("code", ui_ed_code.text.toString())

        Http.post("register/code", params)
            .then {
                afterSubmit(it)
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun afterSubmit(json: JSONObject)
    {
        // save to db
        val rmodel = RegisterCodeRModel.parse(json)
        Repo.user.registerAndActivate(
            rmodel.id, vm.email, rmodel.api_token, vm.name, rmodel.avatar,
            rmodel.avatarThumb, rmodel.is_pro
        )

        // load to instance
        App.shared.invalidateUid()
        App.shared.loadActiveUser()

        activity?.finish()
    }

}