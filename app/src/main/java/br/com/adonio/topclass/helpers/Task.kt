package br.com.adonio.topclass.helpers

import android.os.Handler
import android.os.Looper
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

object Task {

    private val debounces by lazy<MutableList<String>> { mutableListOf() }
    private val indexes by lazy<ArrayList<String>> { arrayListOf() }
    private val scheduleCache by lazy<HashMap<String,ScheduledExecutorService>> { hashMapOf() }

    fun main(delay: Long = 0, closure: ()->Unit): Boolean {
        return Handler(Looper.getMainLooper()).postDelayed(closure, delay)
    }

    fun mainIndexed(delay: Long = 0, closure: ()->Unit): String {
        val uid = "Task.MainIndexed." + Str.random(25)
        indexes.add(uid)

        main(delay) {
            if (indexes.contains(uid)) {
                indexes.remove(uid)
                closure()
            }
        }
        return uid
    }

    fun removeMainIndex(uid: String) {
        indexes.remove(uid)
    }

    fun debounce(key: String, interval: Long, cb: ()->Unit) {
        debounces.add(key)
        main(interval) {
            debounces.remove(key)
            if (debounces.indexOf(key) == -1) cb()
        }
    }

    fun schedule(key: String,
                 initialDelay: Long,
                 period: Long,
                 unit: TimeUnit,
                 mainThread: Boolean,
                 cb: () -> Unit
    )
    {
        scheduleCache[key]?.shutdown()
        val executor = Executors.newSingleThreadScheduledExecutor()
        scheduleCache[key] = executor
        executor.scheduleAtFixedRate({

            if (mainThread) {
                main(0, cb)
            } else {
                cb()
            }

        }, initialDelay, period, unit)

    }

    fun unschedule(key: String) {
        scheduleCache[key]?.shutdown()
        scheduleCache.remove(key)
    }

}