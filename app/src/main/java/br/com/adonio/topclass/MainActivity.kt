package br.com.adonio.topclass

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import br.com.adonio.topclass.extensions.setVisible
import br.com.adonio.topclass.frags.FragProfile
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.processors.FcmProcessor
import br.com.adonio.topclass.repo.Repo
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity(), InterceptListener, EvListener {

    private lateinit var vm: MainActivityVM

    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var mNavController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    companion object {
        const val KEY_INFO = "MainActivity@info"
        fun isActive(): Boolean {
            return Intercept.retrieveFirst<Boolean>("MainActivity@isActive") == true
        }
        fun navigatePro(nav: NavController?) {
            nav?.navigate(R.id.nav_frag_teacher, null, buildNavOptions())
        }
        fun navigateProposal(nav: NavController?) {
            nav?.navigate(R.id.nav_frag_proposal, null, buildNavOptions())
        }
        fun navigateTransaction(nav: NavController?) {
            nav?.navigate(R.id.nav_frag_transaction, null, buildNavOptions())
        }
        fun navigateContract(nav: NavController?) {
            nav?.navigate(R.id.nav_frag_contracts, null, buildNavOptions())
        }
        fun navigateAd(nav: NavController?) {
            nav?.navigate(R.id.nav_frag_ads, null, buildNavOptions())
        }
        private fun buildNavOptions(): NavOptions {
            return NavOptions.Builder()
                .setEnterAnim(R.anim.slide_in_left)
                .setExitAnim(R.anim.slide_out_left)
                .setPopEnterAnim(R.anim.slide_in_right)
                .setPopExitAnim(R.anim.slide_out_right)
                .build()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        vm = ViewModelProviders.of(this).get(MainActivityVM::class.java)

        vm.loading.observe(this, Observer {
            findViewById<ProgressBar>(R.id.ui_main_progress)?.setVisible(it > 0)
        })

        mDrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        mNavController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_frag_teacher, R.id.nav_frag_ads, R.id.nav_frag_about,
                R.id.nav_frag_profile, R.id.nav_frag_proposal, R.id.nav_frag_contracts,
                R.id.nav_frag_transaction, R.id.nav_frag_ads
            ), mDrawerLayout
        )
        setupActionBarWithNavController(mNavController, appBarConfiguration)
        navView.setupWithNavController(mNavController)

        navView.setNavigationItemSelectedListener {
            // backup last position
            when(it.itemId) {
                R.id.nav_frag_teacher -> "pro"
                R.id.nav_frag_proposal -> "proposal"
                R.id.nav_frag_contracts -> "contract"
                R.id.nav_frag_about -> "about"
                R.id.nav_frag_transaction -> "trans"
                R.id.nav_frag_ads -> "ads"
                else -> null
            }?.let { last ->
                Repo.pref.put("MainActivity@navSelected2", last)
            }

            // navigate
            mNavController.navigate(it.itemId)
            mDrawerLayout.closeDrawers()

            true
        }

        // restore last position
        Repo.pref.get("MainActivity@navSelected2")
            ?.let { key ->
                when(key) {
                    "pro" -> R.id.nav_frag_teacher
                    "proposal" -> R.id.nav_frag_proposal
                    "about" -> R.id.nav_frag_about
                    "contract" -> R.id.nav_frag_contracts
                    "trans" -> R.id.nav_frag_transaction
                    "ads" -> R.id.nav_frag_ads
                    else -> null
                }
            }
            ?.let {
                mNavController.navigate(it)
            }

        // --
        mDrawerLayout.addDrawerListener(object: DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(newState: Int) {}
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {}
            override fun onDrawerClosed(drawerView: View) {}
            override fun onDrawerOpened(drawerView: View) {
                Act.hideKeyboard(this@MainActivity)
                updateDesktopDrawer()
            }
        })
    }

    private fun updateDesktopDrawer() {
        val user = App.user ?: return

        ui_drawer_title?.text = user.name
        ui_drawer_desc?.text = user.email
        user.avatar_thumb?.let { a ->
            Img.loadAvatar(a, ui_drawer_avatar)
        }

        ui_icon_pro?.setVisible(user.isPro)

        fun openProfile() {
            Act.navigateWithAnimation(mNavController, R.id.nav_frag_profile, null)
            mDrawerLayout.closeDrawers()
        }
        ui_btn_edit_profile?.setOnClickListener { openProfile() }
        ui_drawer_avatar?.setOnClickListener { openProfile() }
    }

    override fun intercept(key: String): Any? {
        return when(key) {
            "MainActivity@isActive" -> true
            else -> null
        }
    }

    override fun ev(r: EvResult) {
        r.whenValue<Pair<String,String>>(KEY_INFO) {
            T.info(this, "${it.first}\n${it.second}")
        }
    }

    override fun onStart() {
        super.onStart()

        Intercept.register(this)
        Ev.register(this)

        //
        updateDesktopDrawer()

        // call login, if user is nil
        if ( ! App.userIsValid) {
            LoginActivity.start(this)
        }

        // request profile info, only 30% of time
        if (Random.nextInt(0, 9) <= 2) {
            vm.loadingInc(1)
            FragProfile.requestProfile {
                vm.loadingInc(-1)
            }
        }

        // fcm upload token and... stuff
        FcmProcessor.mainActivityStart()
    }

    override fun onStop() {
        super.onStop()

        Intercept.unregister(this)
        Ev.unregister(this)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
