package br.com.adonio.topclass.frags.profile

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.extensions.setVisible
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.repo.Repo
import kotlinx.android.synthetic.main.frag_profile_pro.*

class FragProfilePro: Fragment() {

    // state

    private lateinit var vm: FragProfileProVM

      // methods

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_profile_pro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragProfileProVM::class.java)

        vm.loading.observe(this, Observer {
            ui_progress.setVisible(it > 0)
            ui_submit.setEnabledEx(it == 0)
        })

        vm.pdf.observe(this, Observer {
            val msg = if (it==null) "Localizar arquivo..." else "Arquivo encontrado!"
            val colorId = if (it==null) R.color.md_grey_500 else R.color.md_green_500
            ui_cv.text = Span(msg).color(context!!, colorId).build()
        })
        vm.pdf.value = null

        ui_wrapper_cv.setOnClickListener {
            FilePicker.pick(this, arrayOf("pdf")) {
                vm.pdf.value = it
            }
        }

        ui_submit.setOnClickListener {
            submit()
        }

        updateDesktop()
    }

    @SuppressLint("SetTextI18n")
    private fun updateDesktop() {
        // top msg
        val requisitions = Repo.pref.get("FragProfile@pendingProRequisition", 0)
        if (requisitions > 0) {
            ui_lbl_icon_top.text = "\uF269"
            ui_lbl_icon_top.setTextColor(Cl.res(context!!, R.color.md_blue_500))

            ui_lbl_top.text = "Você já enviou $requisitions requisição(ões) com sucesso. Aguarde confirmação de nossos moderadores."
            ui_lbl_top.setTextColor(Cl.res(context!!, R.color.md_blue_500))
        }
    }

    private fun reset() {
        vm.pdf.value = null
        ui_ed_obs.setText("")
    }

    private fun submit() {
        Act.hideKeyboard(ui_ed_obs)

        val params = HttpMulti().add("obs", ui_ed_obs.text.toString())
        vm.pdf.value?.let { f -> params.add("cv", f) }

        vm.loadingInc(1)
        Http.upload("pro_requisition", params)
            .then {
                afterSubmitted()
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }

    }

    private fun afterSubmitted() {
        Repo.pref.inc("FragProfile@pendingProRequisition")

        reset()
        updateDesktop()
    }

}