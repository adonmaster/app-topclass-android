package br.com.adonio.topclass.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.MapView

abstract class BaseFragMapView: Fragment() {

    private var baseMapView: MapView? = null

    fun baseOnCreateView(@LayoutRes fragId: Int, @IdRes mapViewId: Int, inflater: LayoutInflater,
         container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val v = inflater.inflate(fragId, container, false)
        baseMapView = v.findViewById(mapViewId)
        baseMapView?.onCreate(savedInstanceState)
        return v
    }

    override fun onResume() {
        super.onResume()
        baseMapView?.onResume()
    }

    override fun onStart() {
        super.onStart()
        baseMapView?.onStart()
    }

    override fun onStop() {
        super.onStop()
        baseMapView?.onStop()
    }

    override fun onPause() {
        super.onPause()
        baseMapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        baseMapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        baseMapView?.onLowMemory()
    }

}