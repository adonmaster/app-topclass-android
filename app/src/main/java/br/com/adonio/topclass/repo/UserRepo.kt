package br.com.adonio.topclass.repo

import br.com.adonio.topclass.db.DB
import br.com.adonio.topclass.models.User
import br.com.adonio.topclass.models.UserDao
import br.com.adonio.topclass.rmodels.UserRModel

class UserRepo: BaseRepo<User, UserDao>(DB.i.user) {

    fun findActive(): User? {
        return dao.findActive()
    }

    fun registerAndActivate(remoteId: Long, email: String, token: String, name: String, avatar: String?, avatar_thumb: String?, isPro: Boolean): User
    {
        dao.clearActive()

        val model = dao.findBy(email) ?: User()
        model.remote_id = remoteId
        model.email = email
        model.token = token
        model.name = name
        model.avatar = avatar
        model.avatar_thumb = avatar_thumb
        model.active = true
        model.isPro = isPro

        return save(model, true)
    }

    fun clearActive() {
        dao.clearActive()
    }

    fun update(rmodel: UserRModel): User? {
        val model = dao.findBy(rmodel.email) ?: return null
        model.remote_id = rmodel.id
        model.name = rmodel.name
        model.avatar = rmodel.avatar
        model.avatar_thumb = rmodel.avatar_thumb
        model.isPro = rmodel.isPro

        return save(model, true)
    }

    fun testSetAdminActive(option: Boolean) {
        val model = findActive() ?: return
        model.isPro = option
        save(model)
    }


}