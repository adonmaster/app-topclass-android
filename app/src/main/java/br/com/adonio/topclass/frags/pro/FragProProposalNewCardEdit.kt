package br.com.adonio.topclass.frags.pro

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Promise
import br.com.adonio.topclass.helpers.T
import br.com.adonio.topclass.repo.Repo
import br.com.adonio.topclass.structs.MaskWatcher
import br.com.adonio.topclass.structs.SimpleCard
import kotlinx.android.synthetic.main.frag_pro_proposal_new_card_edit.*

class FragProProposalNewCardEdit: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro_proposal_new_card_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        val data = SimpleCard.parse(Repo.pref.get("card@json"))

        //
        MaskWatcher.bindCard(ui_ed_card_no)
        ui_ed_card_no.setText(data?.no)

        MaskWatcher.bind(ui_ed_card_validade_month, "##")
        ui_ed_card_validade_month.setText(data?.validMonth)

        MaskWatcher.bind(ui_ed_card_validade_year, "####")
        ui_ed_card_validade_year.setText(data?.validYear)

        MaskWatcher.bind(ui_ed_card_secret, "###")
//        ui_ed_card_secret.setText(data?.secret)

        ui_ed_card_name.setText(data?.name)

        ui_card_submit.setOnClickListener {
            submit()
        }
    }

    private fun submit() {
        Act.hideKeyboard(this)

        validate()
            .then { model ->

                Repo.pref.put("card@json", model.toJson().toString())
                findNavController().popBackStack()

            }
            .catch {
                T.error(activity!!, it)
            }
    }

    @SuppressLint("DefaultLocale")
    private fun validate() = Promise<SimpleCard> { resolve, reject ->
        val no = ui_ed_card_no.text.toString()
        val validMonth = ui_ed_card_validade_month.text.toString()
        val validYear = ui_ed_card_validade_year.text.toString()
        val secret = ui_ed_card_secret.text.toString()
        val name = ui_ed_card_name.text.toString().trim()

        when {
            no.count() != 19 -> {
                reject("Número de cartão inválido")
            }
            validMonth.count() != 2 -> {
                reject("Mês de validade inválido")
            }
            validYear.count() != 4 -> {
                reject("Ano de validade inválido")
            }
            secret.count() != 3 -> {
                reject("Segredo inválido")
            }
            name.count() < 4 -> {
                reject("Nome inválido")
            }
            else -> {
                resolve(SimpleCard(no, secret, validMonth, validYear, name.toUpperCase()))
            }
        }
    }
}