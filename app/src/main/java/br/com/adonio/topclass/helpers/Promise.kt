package br.com.adonio.topclass.helpers

class Promise<V>(private val cb: ((V) -> Unit, (String) -> Unit) -> Unit) {

    private var resolver: (V)->Unit = {}
    private var rejector: (String)->Unit = {}
    private var doner: (()->Unit)? = null

    init {
        start()
    }

    private fun start() {
        Task.main {
            try {
                cb(resolver, rejector)
            } catch (e: Exception) {
                rejector(e.localizedMessage)
            }
        }
    }

    fun then(cb: (V)->Unit): Promise<V> {
        resolver = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun catch(cb: (String)->Unit): Promise<V> {
        rejector = {
            cb(it)
            doner?.invoke()
        }
        return this
    }

    fun always(cb: ()->Unit) {
        doner = cb
    }

}

fun <T> promiseIt(obj: T?) = Promise<T> { resolve, reject ->
    try {
        if (obj!=null) {
            resolve(obj)
        } else {
            reject("objeto vazio")
        }
    } catch(e: Exception) {
        reject(e.localizedMessage)
    }
}

// fixture
class Chain(private vararg val promises: ()->Promise<Any>) {

    private var mDone: ()->Unit = {}
    private val resolvers: ArrayList<(Any)->Unit> = arrayListOf()
    private var catcher: (String)->Unit = {}
    private var mIndex = -1

    init {
        Task.main {
            next()
        }
    }

    private fun next() {
        mIndex++
        val promise = guard(promises.getOrNull(mIndex)) {
            mDone()
            return
        }

        promise()
            .then {
                resolvers.getOrNull(mIndex)?.invoke(it)
                next()
            }
            .catch(catcher)
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> then(cb: (T)->Unit): Chain {
        resolvers.add(cb as (Any)->Unit)
        return this
    }

    fun thenSkip(): Chain {
        resolvers.add {}
        return this
    }

    fun catchAny(cb: (String)->Unit): Chain {
        catcher = cb
        return this
    }

    fun allDone(cb: ()->Unit): Chain {
        mDone = cb
        return this
    }

}