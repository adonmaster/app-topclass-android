package br.com.adonio.topclass.frags.pro

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.IdRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.dialogs.DialogProposalDate
import br.com.adonio.topclass.frags.adapters.FragProProposalNewDatesAdapter
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.structs.ProposalBody
import br.com.adonio.topclass.structs.SimpleAddress
import com.smartlib.addresspicker.AddressPickerActivity
import kotlinx.android.synthetic.main.frag_pro_proposal_new.*
import org.threeten.bp.LocalDateTime

class FragProProposalNew: Fragment(), DialogProposalDate.Listener {

    //
    private lateinit var vm: FragProProposalNewVM
    private var mDatesAdapter = FragProProposalNewDatesAdapter()

    //
    private val cProId: Int
        get() = arguments!!.getInt("pro_id")

    private val cClasses
        get() = arguments?.getStringArray("classes")?.toList() ?: listOf()

    private val cShifts
        get() = arguments?.getStringArray("shifts")?.toList() ?: listOf()

    private val cPriceForHour
        get() = arguments!!.getDouble("priceForHour")

    private val cUserName get() = arguments!!.getString("userName")!!
    private val cUserEmail get() = arguments!!.getString("userEmail")!!
    private val cUserAvatar get() = arguments?.getString("userAvatar")


    //
    companion object {
        fun navigate(
            fragment: Fragment, @IdRes id: Int,
            proUserRemoteId: Int,
            proClasses: List<String>,
            proShifts: List<String>,
            priceForHour: Double,
            userName: String,
            userEmail: String,
            userAvatar: String?
        )
        {
            val bundle = Bundle()
            bundle.putInt("pro_id", proUserRemoteId)
            bundle.putStringArray("classes", proClasses.toTypedArray())
            bundle.putStringArray("shifts", proShifts.toTypedArray())
            bundle.putDouble("priceForHour", priceForHour)

            bundle.putString("userName", userName)
            bundle.putString("userEmail", userEmail)
            bundle.putString("userAvatar", userAvatar)

            fragment.findNavController().navigate(id, bundle)
        }
    }

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro_proposal_new, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragProProposalNewVM::class.java)

        vm.proId = cProId
        vm.priceForHour = cPriceForHour

        //
        Img.loadAvatar(cUserAvatar, ui_avatar)
        ui_name?.text = cUserName
        ui_desc1?.text = cUserEmail

        ui_price?.text = "R$ ${Str.number(cPriceForHour)} / hora"

        //
        vm.items.value = listOf()
        vm.items.observe(this, Observer { list ->
            ui_dates_empty.isVisible = list.count() == 0
            mDatesAdapter.updateData(list)

            ui_lbl_dates_info.text = Str.plural(
                list.count(),
                "sem aulas programadas",
                "apenas uma aula programada"
                ) { "$it aulas programadas" }
        })

        vm.address.observe(this, Observer {
            ui_location.text = if (it==null) {
                Span("Aperte abaixo para escolher localização...")
                    .color(context!!, R.color.md_grey_200)
                    .build()
            } else {
                Span(it.address)
                    .color(context!!, R.color.md_grey_500)
                    .build()
            }
        })
        vm.address.value = null

        //
        ui_btn_location.setOnClickListener {
            startActivityForResult(Intent(activity, AddressPickerActivity::class.java), 162)
        }

        ui_btn_date_time.setOnClickListener {
            DialogProposalDate.show(childFragmentManager, cClasses)
        }

        //
        mDatesAdapter.listener = object : FragProProposalNewDatesAdapter.Listener {
            override fun onDelete(position: Int) {
                val mm = vm.items.value!!.toMutableList()
                mm.removeAt(position)
                vm.items.value = mm
            }
        }
        ui_rv_dates.adapter = mDatesAdapter
        ui_rv_dates.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv_dates.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)


        ui_btn_next.setOnClickListener {
            submit()
        }
    }

    private fun submit() {
        if (validate()) {

            vm.address.value!!.comp = ui_address_comp.text.toString()

            findNavController().navigate(R.id.sg_to_card)
        }
    }

    private fun validate(): Boolean {
        when {
            vm.items.value!!.count() == 0 -> {
                T.error(activity, "Inclua nem que seja uma aula!")
            }
            vm.address.value == null -> {
                T.error(activity, "Por favor, escolha o local da aula!")
            }
            else -> {
                return true
            }
        }
        return false
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==162 && resultCode == Activity.RESULT_OK) {
            val address = data?.getParcelableExtra<Address>(AddressPickerActivity.RESULT_ADDRESS)
                ?: return
            val addressLine = address.getAddressLine(0) ?: return
            validateAddress(addressLine)
                .then {

                    vm.address.value = SimpleAddress(
                        addressLine, address.latitude, address.longitude, null
                    )

                }
                .catch {
                    DialogOk.show(context, it, "Erro")
                }
        }
    }

    override fun dialogProposalDates(dt: LocalDateTime, duration: Int, materia: String)
    {
        validateDate(dt)
            .then {

                val mutable = vm.items.value?.toMutableList() ?: mutableListOf()
                mutable.add(ProposalBody.Item(dt, materia, duration))
                vm.items.value = mutable

            }
            .catch {
                DialogOk.show(context, it, "Erro")
            }
    }

    @SuppressLint("DefaultLocale")
    private fun validateAddress(addressLine: String) = Promise<Boolean> { resolve, reject ->

        val line = addressLine.toLowerCase().replace("goiânia", "goiania")

        if (!line.contains("goiania")) {
            reject("Esta versão permite apenas localizações dentro da cidade de Goiânia-GO.\nAguarde atualizações!")
            return@Promise
        }

        resolve(true)
    }

    @SuppressLint("DefaultLocale")
    private fun validateDate(d: LocalDateTime) = Promise<Boolean> { resolve, reject ->

        //
        if (LocalDateTime.now() > d) {
            reject("Esta data já passou. Selecione uma nova.")
            return@Promise
        }

        //
        if (cShifts.count() > 0) {
            val dtDesc = Dt.formatBrWeekDay(d.toLocalDate()) + "/" + Dt.formatShift(d)
            for (s in cShifts) {
                if (s.toLowerCase() == dtDesc.toLowerCase()) {
                    resolve(true)
                    return@Promise
                }
            }

            val msg = "Dia/Horário inválido para este professor. Veja os dias que ele está disponível:"
            reject(msg + "\n\n" + cShifts.joinToString("\n"))
            return@Promise
        }

        resolve(true)
    }

    //

    override fun onStart() {
        super.onStart()

        DialogProposalDate.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogProposalDate.detach()
    }

}