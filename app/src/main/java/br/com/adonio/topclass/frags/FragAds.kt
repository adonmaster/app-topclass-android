package br.com.adonio.topclass.frags

import android.os.Bundle
import android.view.*
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.AdRModel
import br.com.adonio.topclass.structs.SimpleTabSelectedListener
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.frag_ads.*
import kotlinx.android.synthetic.main.rv_frag_ads.view.*

class FragAds: Fragment(), Rv.Owner {


    //
    private lateinit var vm: FragAdsVM
    private var mData = listOf<AdRModel>()
    private lateinit var mAdapter: Rv

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_ads, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vm = ViewModelProviders.of(activity!!).get(FragAdsVM::class.java)

        //
        vm.loading.observe(this, Observer {
            if (it == 0) ui_swipe?.isRefreshing = false
            ui_progress?.isVisible = it > 0
        })

        //
        ui_swipe?.setOnRefreshListener {
            ui_swipe?.isRefreshing = true
            requestData()
        }

        //
        TabIt.setup(ui_tabs, 0, "TODOS", null)
        TabIt.setup(ui_tabs, 1, "MEUS ANÚNCIOS", null)

        //
        ui_tabs.getTabAt(0)?.let {
            it.select()
            it.customView?.alpha = 1f
        }
        ui_tabs.getTabAt(1)?.customView?.alpha = 0.2f

        //
        ui_tabs.addOnTabSelectedListener(object: SimpleTabSelectedListener() {
            override fun onTabUnselected(p0: TabLayout.Tab?) {
                super.onTabUnselected(p0)
                p0?.customView?.alpha = 0.2f
            }
            override fun onTabSelected(p0: TabLayout.Tab?) {
                super.onTabSelected(p0)
                p0?.customView?.alpha = 1f
                updateDesktop()
            }
        })

        mAdapter = Rv(this, R.layout.rv_frag_ads)
        ui_rv.adapter = mAdapter
        Rv.setupForTable(ui_rv, context)

        //
        requestData()
    }

    private fun updateDesktop()
    {
        // filter by tabs
        val listAll = mData
        val listMine = mData.filter { it.p.isUserRelated(App.user?.remote_id) }
        val listCurrent = ter(ui_tabs.selectedTabPosition == 1, listMine, listAll)

        //
        TabIt.assign(ui_tabs, 0, null, listAll.count())
        TabIt.assign(ui_tabs, 1, null, listMine.count())

        //
        ui_info?.text = Str.plural(
            listCurrent.count(), "sem itens", "apenas um item",
            "$0 itens"
        )

        //
        ui_empty?.isVisible = listCurrent.count() == 0

        //
        mAdapter.updateData(listCurrent)
    }

    private fun requestData() {
        vm.loadingInc(1)
        Http.get("ads")
            .then { json ->

                mData = AdRModel.parse(json)
                updateDesktop()

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    override fun rvCreate(
        identifier: String,
        itemView: View,
        r: () -> Rv.Value,
        position: () -> Int
    ) {
        itemView.setOnClickListener {
            vm.selected = r().value<AdRModel>()!!

            findNavController().navigate(R.id.sg_to_ad_detail)
        }
    }

    override fun rvBind(identifier: String, v: View, h: Rv.VH, r: Rv.Value, position: Int) {
        r.whenValue<AdRModel> { model ->

            //
            Img.loadAvatar(model.client_user.avatar_thumb, v.ui_avatar)

            //
            v.ui_title.text = model.client_user.name
            v.ui_desc1.text = model.client_user.email

            val body = model.p.body!!
            val assignTo = Dt.format(body.items.first().dt, "dd/MM/yyyy HH:mm")!!
            val clazz = body.items.first().materia
            val duration = body.items.first().duration
            val desc2 = "$assignTo\n$clazz: $duration mins"
            v.ui_desc2.text = desc2

            // badge
            v.ui_badge.text = Str.plural(
                model.pros.count(), "Nenhum profissional respondeu ainda",
                "1 profissional respondeu",
                "$0 profissionais responderam"
            )
            v.ui_badge.setBackgroundColor(Cl.quad(
                R.color.md_grey_400,
                (model.pros.count() > 0) to R.color.md_blue_500
            ))
        }
    }

    override fun rvId(identifier: String, r: Rv.Value, position: Int): Int? {
        return r.value<AdRModel>()!!.id
    }


    //
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_ads, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_action_add -> {
                findNavController().navigate(R.id.sg_to_ad_new)
            }
        }
        return super.onOptionsItemSelected(item)
    }

}