package br.com.adonio.topclass.helpers

import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit
import kotlin.math.abs

@Suppress("unused", "MemberVisibilityCanBePrivate")
object Dt {

    fun fromPattern(s: String?, pattern: String="yyyy-MM-dd HH:mm"): LocalDateTime? {
        val ss = guard(s) { return null }
        val f = DateTimeFormatter.ofPattern(pattern)
        return LocalDateTime.parse(ss, f)
    }

    fun fromJson(s: String?) = fromPattern(s, "yyyy-MM-dd HH:mm:ss") // 2019-08-26 16:37:15

    fun format(dateTime: LocalDateTime?, pattern: String = "dd/MM/yyyy HH:mm"): String? {
        val d = guard(dateTime) { return null }
        return d.format(DateTimeFormatter.ofPattern(pattern))
    }

    fun format(date: LocalDate?, pattern: String = "dd/MM/yyyy"): String? {
        val d = date ?: return null
        return d.format(DateTimeFormatter.ofPattern(pattern))
    }

    fun formatToJson(dt: LocalDateTime?): String? {
        return format(dt, "yyyy-MM-dd HH:mm:ss")
    }

    fun formatBr(dt: LocalDateTime?): String? {
        return format(dt, "dd/MM/yyyy HH:mm")
    }

    fun formatSqlDate(dt: LocalDateTime?): String? {
        return format(dt, "yyyy-MM-dd HH:mm:ss")
    }

    fun formatBrDate(dt: LocalDateTime?): String? {
        return format(dt, "dd/MM/yyyy")
    }

    fun isToday(dt: LocalDateTime?): Boolean {
        if (dt==null) return false
        return isToday(dt.toLocalDate())
    }

    fun isToday(dt: LocalDate?): Boolean {
        return LocalDate.now() == dt
    }

    fun isSameDay(a: LocalDateTime?, b: LocalDateTime?): Boolean
    {
        if (a==null) return false
        if (b==null) return false

        return a.toLocalDate() == b.toLocalDate()
    }

    fun fromNow(d: LocalDate?, fakeNow: LocalDate?=null): String? {
        if (d==null) return null

        val now = fakeNow ?: LocalDate.now()
        val diffInDays = ChronoUnit.DAYS.between(now, d).toInt()
        val diffInMonthsPos = abs(ChronoUnit.MONTHS.between(now, d).toInt())
        val diffInYearsPos = abs(ChronoUnit.YEARS.between(now, d).toInt())

        return if (abs(diffInDays) <= 2) {
            when(diffInDays) {
                -2 -> "anteontem"
                -1 -> "ontem"
                1 -> "amanhã"
                2 -> "depois de amanhã"
                else -> "hoje"
            }
        }
        else
        {
            val suffix = when {
                diffInYearsPos > 0 -> notZeroPlural(diffInYearsPos, "um ano") { "$it anos" }
                diffInMonthsPos > 0 -> notZeroPlural(diffInMonthsPos, "um mês") { "$it meses" }
                else -> "${abs(diffInDays)} dias"
            }

            if (diffInDays > 0) "em $suffix" else "há $suffix"
        }
    }

    fun fromNow(dt: LocalDateTime?, fakeNow: LocalDateTime?=null): String? {
        if (dt==null) return null

        val now = fakeNow ?: LocalDateTime.now()
        val day = fromNow(dt.toLocalDate(), now.toLocalDate())
        return if (day=="hoje") {

            val diffInMin = abs(ChronoUnit.MINUTES.between(now, dt)).toInt()
            val r = if (diffInMin < 50) {
                Str.plural(diffInMin, "menos de um hora", "um minuto") { "$it minutos" }
            } else {
                Str.plural(diffInMin/60, "menos de uma hora", "uma hora") { h -> "$h horas" }
            }
            ter(dt > now, "em $r", "$r atrás")

        } else {

            "$day, às ${format(dt, "HH:mm")}"

        }
    }

    private fun notZeroPlural(i: Int, oneOrZero: String, more: (Int)->String): String {
        if (i <= 1) return oneOrZero
        return more(i)
    }

    fun formatShift(d: LocalDateTime): String {
        return when (d.hour) {
            in 5..11 -> "Matutino"
            in 12..18 -> "Vespertino"
            else -> "Noturno"
        }
    }

    fun formatBrWeekDay(d: LocalDate): String {
        @Suppress("WHEN_ENUM_CAN_BE_NULL_IN_JAVA")
        return when (d.dayOfWeek) {
            DayOfWeek.SUNDAY -> "Domingo"
            DayOfWeek.MONDAY -> "Segunda"
            DayOfWeek.TUESDAY -> "Terça"
            DayOfWeek.WEDNESDAY -> "Quarta"
            DayOfWeek.THURSDAY -> "Quinta"
            DayOfWeek.FRIDAY -> "Sexta"
            DayOfWeek.SATURDAY -> "Sábado"
        }
    }

}