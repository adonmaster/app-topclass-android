package br.com.adonio.topclass.frags.profile.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Leaf
import br.com.adonio.topclass.rmodels.SkillsRModel
import kotlinx.android.synthetic.main.rv_frag_profile_pro_resolved.view.*
import kotlinx.android.synthetic.main.rv_frag_profile_pro_resolved_header.view.*
import kotlinx.android.synthetic.main.rv_frag_profile_pro_resolved_subheader.view.*


class FragProfileProResolvedAdapter(private val listener: Listener)
    : RecyclerView.Adapter<FragProfileProResolvedAdapter.VH>() {

    private var protectedCheck = false
    private var mData = listOf<Leaf.Line<SkillsRModel>>()

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun getItemViewType(position: Int): Int {
        return mData[position].level + 1
    }

    fun updateData(data: List<Leaf.Line<SkillsRModel>>)
    {
        mData = data

        //
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val idData = R.layout.rv_frag_profile_pro_resolved
        val idSubHeader = R.layout.rv_frag_profile_pro_resolved_subheader
        val idHeader = R.layout.rv_frag_profile_pro_resolved_header

        val id = when(viewType) {
            1 -> idHeader
            2 -> idSubHeader
            else -> idData
        }

        val v = Act.inflate(id, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        when (m.level + 1) {
            1 -> {
                holder.uiHeaderTitle?.text = m.leaf.name
            }
            2 -> {
                holder.uiSubHeaderTitle?.text = m.leaf.name
            }
            3 -> {
                m.leaf.whenContent { model ->
                    holder.uiOption?.text = model.p.desc()

                    protectedCheck = true
                    holder.uiOption?.isChecked = model.checked
                    protectedCheck = false
                }
            }
        }
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiOption: SwitchCompat? = itemView.ui_option
        val uiHeaderTitle: TextView? = itemView.ui_title
        val uiSubHeaderTitle: TextView? = itemView.ui_subheader_title

        init {
            uiOption?.setOnCheckedChangeListener { _, isChecked ->
                if (protectedCheck) return@setOnCheckedChangeListener

                val p = adapterPosition
                mData[p].leaf.whenContent { model ->
                    model.checked = isChecked
                    listener.onChecked(model, p)
                }
            }
        }
    }

    // interface
    interface Listener {
        fun onChecked(model: SkillsRModel, position: Int)
    }
}