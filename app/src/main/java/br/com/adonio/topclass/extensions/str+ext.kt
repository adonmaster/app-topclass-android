package br.com.adonio.topclass.extensions

fun String.concat(s: String?): String {
    if (s==null) return this
    return this + s
}