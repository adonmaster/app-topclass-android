package br.com.adonio.topclass.frags

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.MainActivity
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.adapters.FragProAdapter
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Http
import br.com.adonio.topclass.helpers.T
import br.com.adonio.topclass.helpers.Task
import br.com.adonio.topclass.rmodels.ProQueryRModel
import br.com.adonio.topclass.structs.SimpleTextChangedListener
import kotlinx.android.synthetic.main.frag_pro.*

class FragPro: Fragment() {

    // state
    private lateinit var vm: FragProVM
    private lateinit var mAdapter: FragProAdapter
    private val mShowIntro = MutableLiveData<Boolean>().also { it.value = true }
    private var mBkupData = listOf<ProQueryRModel>()

    // methods
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_pro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragProVM::class.java)

        vm.loading.observe(this, Observer {
            ui_progress?.isVisible = it > 0
            if (it==0) ui_swipe?.isRefreshing = false
        })

        //
        ui_empty.isVisible = false

        //
        ui_swipe.setOnRefreshListener {
            requestData()
        }

        //
        ui_query.addTextChangedListener(object: SimpleTextChangedListener() {
            override fun afterTextChanged(s: Editable?) {
                requestData()

                // show intro
                if (s?.isNotBlank()==true) mShowIntro.value = false
            }
        })
        ui_query.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Act.hideKeyboard(v)
                requestData()
                true
            } else {
                false
            }
        }
        ui_query.setOnClickListener { mShowIntro.value = false }

        mAdapter = FragProAdapter(object : FragProAdapter.Listener {
            override fun onSelect(model: ProQueryRModel, position: Int) {
                vm.selected.value = model
                Task.main(1000) { mShowIntro.value = false }

                findNavController().navigate(R.id.sg_to_pro_show)
            }
        })
        mAdapter.updateData(mBkupData)
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)

        //
        ui_btn_create_ad?.setOnClickListener {
            MainActivity.navigateAd(findNavController())
        }
    }

    private fun requestData() {
        Task.debounce("FragPro@requestData", 950) {
            reallyRequestData()
        }
    }

    private fun reallyRequestData() {
        if (vm.loading.value!! > 0) return

        val params = ui_query?.text.toString()
            .takeIf { it.isNotBlank() }
            ?.let { listOf("q" to it) }

        vm.loadingInc(1)
        Http.get("pro/query", params ?: listOf())
            .then {

                mBkupData = ProQueryRModel.parse(it)
                mAdapter.updateData(mBkupData)

                ui_empty?.isVisible = mBkupData.count() == 0

            }
            .catch {
                T.error(activity!!, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    // ui

    override fun onStart() {
        super.onStart()

        requestData()
    }
}