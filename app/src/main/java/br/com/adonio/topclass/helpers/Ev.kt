@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package br.com.adonio.topclass.helpers

object Ev {

    private val cache = arrayListOf<EvListener>()

    fun register(listener: EvListener) {
        if ( ! cache.contains(listener)) cache.add(listener)
    }

    fun unregister(listener: EvListener) {
        cache.remove(listener)
    }

    fun fire(key: String, payload: Any) {
        val r = EvResult(key, payload)
        cache.forEach { it.ev(r) }
    }

    fun fireMain(key: String, payload: Any, delay: Long=0) {
        Task.main(delay) {
            fire(key, payload)
        }
    }

}

interface EvListener {
    fun ev(r: EvResult)
}

class EvResult(val key: String, val value: Any) {

    inline fun <reified T> whenValue(key: String, cb: (T)->Unit) {
        if (this.key == key && value is T) {
            cb(value)
        }
    }

}