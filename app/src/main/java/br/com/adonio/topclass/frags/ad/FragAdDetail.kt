package br.com.adonio.topclass.frags.ad

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogAdSubscribePrice
import br.com.adonio.topclass.dialogs.DialogList
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.frags.BaseFragMapView
import br.com.adonio.topclass.frags.FragAdsVM
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.AdRModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.frag_ad_detail.*
import kotlinx.android.synthetic.main.rv_frag_ad_detail_pro.view.*

class FragAdDetail: BaseFragMapView(), DialogAdSubscribePrice.Listener, Rv.Owner {

    //
    private lateinit var vmParent: FragAdsVM
    private lateinit var vm: FragAdDetailVM
    private lateinit var mModel: AdRModel

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return baseOnCreateView(R.layout.frag_ad_detail, R.id.ui_map, inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vmParent = ViewModelProviders.of(activity!!).get(FragAdsVM::class.java)
        mModel = vmParent.selected
        vm = ViewModelProviders.of(this).get(FragAdDetailVM::class.java)

        //
        vm.loading.observe(this, Observer {
            ui_progress?.isVisible = it > 0
            ui_btn_subscribe?.setEnabledEx(it == 0)
            ui_btn_unsubscribe?.setEnabledEx(it == 0)
            ui_btn_destroy?.setEnabledEx(it == 0)
        })

        setupDesktop()
    }

    @SuppressLint("SetTextI18n")
    private fun setupDesktop() {
        //
        val userId = App.user?.remote_id

        //
        ui_id?.text = "Anúncio #${mModel.id}\nCriada ${Dt.fromNow(mModel.created_at)}"

        // body
        val body = mModel.p.body!! // <--

        // classes title
        ui_title_aulas?.text = Str
            .plural(body.items.count(), "2. Aula (nenhuma)", "2. Aula (1)") { "2. Aulas ($it)" }

        // classes
        ui_classes_desc?.text = body.items.mapIndexed { i, item ->
            val dt = Dt.formatBr(item.dt)
            val dtHuman = Dt.fromNow(item.dt)
            val duration = item.duration
            val materia = item.materia
            Span("${i+1}. ").bold().concat("$dt ($dtHuman)")
                .concat("\n$materia")
                .concat("\nDuração: $duration mins").color(context!!, R.color.md_grey_500)
                .build()
        }.joinToString("\n\n")

        // location
        ui_location_desc?.text = body.address.address
        Str.whenNotEmpty(body.address.comp) {
            ui_location_desc?.append("\n\n${it}")
        }

        // map
        ui_map?.getMapAsync { g ->
            val lat = body.address.lat
            val lng = body.address.lng
            g.clear()
            g.addMarker(
                MarkerOptions()
                    .position(LatLng(lat, lng))
                    .title(mModel.client_user.name)
            )
            g.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15.0f))
        }

        // pros
        ui_wrapper_pros?.isVisible = mModel.pros.count() > 0
        ui_pro_title?.text = Str.plural(
            mModel.pros.count(), "4. Profissionais",
            "4. Profissional (1)", "4. Profissionais ($0)"
        )
        val adapter = Rv(this, R.layout.rv_frag_ad_detail_pro)
        adapter.updateData(mModel.pros)
        ui_rv_pros.adapter = adapter
        Rv.setupForGrid(ui_rv_pros, context)

        // btn subscribe
        ui_btn_subscribe?.isVisible = App.user?.isPro == true
        ui_btn_subscribe?.text = ter(mModel.p.isUserSomePro(userId), "REDEFINIR PREÇO DO ANÚNCIO", "RESPONDER ANÚNCIO")
        ui_btn_subscribe?.setOnClickListener {
            subscribe()
        }

        ui_btn_unsubscribe?.isVisible = mModel.p.isUserSomePro(userId)
        ui_btn_unsubscribe?.setOnClickListener {
            unsubscribe()
        }

        //
        ui_btn_destroy.isVisible = mModel.p.isUserClient(userId)
        ui_btn_destroy?.setOnClickListener {
            destroy()
        }
    }

    private fun requestData() {
        vm.loadingInc(1)
        Http.get("ad/${mModel.id}")
            .then {
                mModel = AdRModel.parseSingle(it)

                setupDesktop()
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }


    private fun subscribe() {
        DialogAdSubscribePrice.show(childFragmentManager, mModel.p.findPriceProFrom(App.user?.remote_id, 0.0))
    }

    private fun unsubscribe() {
        if (vm.loading.value!! > 0) return

        vm.loadingInc(1)
        Http.post("ads/${mModel.id}/unsubscribe", null)
            .then {

                requestData()

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun reallySubscribe(price: Double)
    {
        if (vm.loading.value!! > 0) return

        vm.loadingInc(1)
        val params = HttpFormData().put("price", price)
        Http.post("ads/${mModel.id}/subscribe", params)
            .then {

                requestData()

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun destroy() {
        DialogOk.show(context, "Tem certeza que deseja remover?", "Atenção", "Cancelar") {
            reallyDestroy()
        }
    }

    private fun reallyDestroy() {
        if (vm.loading.value!! > 0) return

        vm.loadingInc(1)
        Http.delete("ads/${mModel.id}")
            .then {
                findNavController().popBackStack()
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun selectPro(pro: AdRModel.AdPro)
    {
        val userId = App.user?.remote_id?.toInt()

        val options = arrayListOf<Pair<String, String>>()
        options.add("Ver perfil" to "profile")

        // it is pro
        if (pro.id == userId) {
            options.add("Redefinir preço do anúncio" to "subscribe")
            options.add("Remover preço do anúncio" to "unsubscribe")
        }

        // it is THE CLIENT
        if (mModel.client_user_id == userId) {
            options.add("Contratar" to "hire")
        }

        DialogList.show(context, pro.name, options.map { it.first }.toTypedArray()) { i, s ->
            @Suppress("MoveVariableDeclarationIntoWhen")
            val action = options.firstOrNull { it.first == s }?.second
            Task.main {
                selectProAction(pro, action)
            }
        }
    }

    private fun selectProAction(pro: AdRModel.AdPro, action: String?) {
        when(action) {
            "profile" -> {
                DialogOk.show(context, "Atenção", "Recurso ainda não instalado no momento")
            }
            "subscribe" -> {
                subscribe()
            }
            "unsubscribe" -> {
                unsubscribe()
            }
            "hire" -> {
                DialogOk.show(context, "Atenção", "Recurso ainda não instalado no momento")
            }
        }
    }

    //

    override fun rvCreate(
        identifier: String,
        itemView: View,
        r: () -> Rv.Value,
        position: () -> Int
    ) {
        itemView.setOnClickListener {
            r().whenValue<AdRModel.AdPro> { m ->
                selectPro(m)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun rvBind(identifier: String, v: View, h: Rv.VH, r: Rv.Value, position: Int) {
        r.whenValue<AdRModel.AdPro> { m ->
            Img.loadAvatar(m.avatar_thumb, v.ui_avatar)
            v.ui_title.text = m.name
            v.ui_desc1.text = m.email
            v.ui_price.text = "R$ " + Str.number(m.price)
        }
    }

    override fun rvId(identifier: String, r: Rv.Value, position: Int): Int? {
        return r.value<AdRModel.AdPro>()!!.id
    }

    //

    override fun dialogAdSubscribePrice(price: Double) {
        reallySubscribe(price)
    }

    //

    override fun onStart() {
        super.onStart()

        DialogAdSubscribePrice.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogAdSubscribePrice.detach()
    }

}