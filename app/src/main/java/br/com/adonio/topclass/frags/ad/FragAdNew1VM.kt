package br.com.adonio.topclass.frags.ad

import androidx.lifecycle.MutableLiveData
import br.com.adonio.topclass.frags.StateViewModel
import br.com.adonio.topclass.structs.ProposalBody
import br.com.adonio.topclass.structs.SimpleAddress

class FragAdNew1VM: StateViewModel() {

    val items = MutableLiveData<List<ProposalBody.Item>>().also { it.value = listOf() }
    val address = MutableLiveData<SimpleAddress>()

}