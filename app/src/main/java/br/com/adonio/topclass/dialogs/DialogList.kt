package br.com.adonio.topclass.dialogs

import android.app.AlertDialog
import android.content.Context

object DialogList {

    private var globalSelected = -1

    fun show(context: Context?, title: String, list: Array<String>, cb: (Int, String)->Unit) {
        if (context == null) return

        val builder = AlertDialog.Builder(context)
        builder.setTitle(title)

        globalSelected = 0
        builder.setSingleChoiceItems(list, 0) { _, position ->
            globalSelected = position
        }

        // add OK and Cancel buttons
        builder.setPositiveButton("OK") { _, _ ->
            cb(globalSelected, list[globalSelected])
        }
        builder.setNegativeButton("Cancel", null)

        builder.create().show()
    }

}