package br.com.adonio.topclass.frags.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Arr
import br.com.adonio.topclass.rmodels.ProQueryRModel
import kotlinx.android.synthetic.main.rv_frag_pro_days.view.*

class FragProDaysAdapter(private val mModel: ProQueryRModel): RecyclerView.Adapter<FragProDaysAdapter.VH>() {

    private val mData by lazy {
        mModel.p.skillsDays
    }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_pro_days, parent)!!
        return VH(v)
    }

    @SuppressLint("DefaultLocale")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        holder.uiDay.text = m.first.substring(0..2).toUpperCase()

        val turns = m.second
        val alphanizer: (String)->Float = { key ->
            if (Arr(turns).contains { it.startsWith(key, true) })
                1.0f
            else
                0.2f
        }
        holder.uiDayMat.alpha = alphanizer("mat")
        holder.uiDayVes.alpha = alphanizer("ves")
        holder.uiDayNot.alpha = alphanizer("not")
    }


    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiDay = itemView.ui_day
        val uiDayMat = itemView.ui_day_mat
        val uiDayVes = itemView.ui_day_ves
        val uiDayNot = itemView.ui_day_not
    }

}