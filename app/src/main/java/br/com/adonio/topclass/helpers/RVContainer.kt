package br.com.adonio.topclass.helpers

class RVContainer<T>(items: List<T>?, private val mSortCategories:Boolean=true, private val extractor: (T)->String) {

    private lateinit var data: List<Item<T>>
    private lateinit var items: List<T>
    private var filter: ((T)->Boolean)?=null

    init {
        update(items ?: listOf())
    }

    fun update(items: List<T>)
    {
        this.items = items

        populateData(items)
    }

    private fun populateData(items: List<T>) {
        val pre = ArrayList<Pair<String, ArrayList<T>>>()
        for (item in items.filter { this.filter?.invoke(it) != false }) {
            val currentCategory = extractor(item)
            val pair = pre.find { it.first.trim().equals(currentCategory.trim(), true) }
            if (pair != null) {
                pair.second.add(item)
            } else {
                pre.add(Pair(currentCategory, arrayListOf(item)))
            }
        }

        // now put the damn thing together
        val r = ArrayList<Item<T>>()
        val l = if (mSortCategories) pre.sortedBy { it.first } else pre
        for (pair in l) {
            r.add(ItemCategory(pair.first, pair.second))
            for (item in pair.second) {
                r.add(ItemData(item))
            }
        }

        data = r
    }

    fun count() = data.count()
    fun getItems() = items

    operator fun get(position: Int) = data.getOrNull(position)

    fun isTypeData(type: Int) = type == 1

    fun isTypeCategory(type: Int) = type == 2

    fun filter(predicate: ((T)->Boolean)?) {
        this.filter = predicate
        update(items)
    }

    // item

    interface Item<T> {
        fun getType(): Int
        fun isTypeData(): Boolean
        fun isTypeCategory(): Boolean
        fun whenCategory(cb: (String)->Unit): Item<T>
        fun whenCategoryList(cb: (String, List<T>)->Unit): Item<T>
        fun whenData(cb: (T)->Unit): Item<T>
    }

    class ItemData<T>(private val content: T): Item<T> {
        override fun getType() = 1
        override fun isTypeData() = true
        override fun isTypeCategory() = false
        override fun whenCategory(cb: (String) -> Unit) = this
        override fun whenCategoryList(cb: (String, List<T>) -> Unit) = this
        override fun whenData(cb: (T) -> Unit): Item<T> {
            cb(content)
            return this
        }
    }

    class ItemCategory<T>(private val content: String, private val items: List<T>): Item<T> {
        override fun getType() = 2
        override fun isTypeData() = false
        override fun isTypeCategory() = true
        override fun whenData(cb: (T) -> Unit): Item<T> = this
        override fun whenCategory(cb: (String) -> Unit): Item<T> {
            cb(content)
            return this
        }
        override fun whenCategoryList(cb: (String, List<T>) -> Unit): Item<T> {
            cb(content, items)
            return this
        }
    }

}