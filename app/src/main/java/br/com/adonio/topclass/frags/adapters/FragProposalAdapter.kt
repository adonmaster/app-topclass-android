@file:Suppress("unused")

package br.com.adonio.topclass.frags.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.FragProposal
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.ProposalRModel
import kotlinx.android.synthetic.main.rv_frag_proposal.view.*

class FragProposalAdapter(private val listener: Listener): RecyclerView.Adapter<FragProposalAdapter.VH>() {

    private lateinit var mState: FragProposal.State
    private var mData = listOf<ProposalRModel>()

    // --
    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_frag_proposal, parent, false)!!
        return VH(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        val context = holder.itemView.context

        val colorId = when(mState) {
            FragProposal.State.PENDENTE -> R.color.md_orange_50
            FragProposal.State.APROVADO -> R.color.md_green_50
            FragProposal.State.EXPIRADO -> R.color.md_grey_50
        }
        holder.uiCardWrapper.setBackgroundColor(Cl.res(context, colorId))

        //
        val body = m.p.body
        val dtTitle = body?.items?.first()?.dt ?: m.createdAt
        holder.uiTitle.text = "Proposta #${m.id} - ${Dt.format(dtTitle)}"

        // pro
        Img.loadAvatar(m.proAvatar, holder.uiProAvatar)
        holder.uiProName.text = m.proName

        // client
        Img.loadAvatar(m.clientAvatar, holder.uiClientAvatar)
        holder.uiClientName.text = m.clientName

        // location
        val location = m.p.body?.address?.address
        holder.uiWrapperLocation.isVisible = location != null
        holder.uiLocationDesc.text = location

        // price
        holder.uiPriceDesc.text = "R$ " + Str.number(m.price)

        // obs
        var obsLeftIcon = "\uF071"
        val expiration = Dt.format(m.expireAt, "dd 'de' MMMM") + " (${Dt.fromNow(m.expireAt)})"
        val obs = when (m.phase) {
            1 -> "Aguardando professor aceitar...\nProposta expira em $expiration"
            2 -> "Aguardando aluno contratar aula...\nProposta expira em $expiration"
            3 -> {
                obsLeftIcon = "\uF058"
                "Contrato já feito!"
            }
            else -> null
        }
        holder.uiWrapperObs.isVisible = obs != null
        holder.uiObsLeft.text = obsLeftIcon
        holder.uiObsDesc.text = obs
    }

    //
    fun updateData(list: List<ProposalRModel>, state: FragProposal.State) {
        mState = state
        mData = list
        notifyDataSetChanged()
    }

    // view holder
    @Suppress("HasPlatformType")
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uiCardWrapper = itemView.ui_card_wrapper
        val uiTitle = itemView.ui_title
        val uiProName = itemView.ui_pro_name
        val uiProAvatar = itemView.ui_pro_avatar
        val uiClientAvatar = itemView.ui_client_avatar
        val uiClientName = itemView.ui_client_name
        val uiWrapperLocation = itemView.ui_wrapper_location
        val uiLocationLeft = itemView.ui_location_left
        val uiLocationDesc = itemView.ui_location_desc
        val uiWrapperObs = itemView.ui_wrapper_obs
        val uiObsLeft = itemView.ui_obs_left
        val uiObsDesc = itemView.ui_obs_desc
        val uiWrapperPrice = itemView.ui_wrapper_price
        val uiPriceLeft = itemView.ui_price_left
        val uiPriceDesc = itemView.ui_price_desc
        init {
            itemView.setOnClickListener {
                val i = adapterPosition
                mData.getOrNull(i)?.let { m ->
                    listener.onSelect(m, i)
                }
            }
        }
    }

    // interface
    interface Listener {
        fun onSelect(item: ProposalRModel, position: Int)
    }

}