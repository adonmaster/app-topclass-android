package br.com.adonio.topclass.helpers

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.fragment.app.Fragment
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import java.io.File

object ImgProviderCrop {

    fun pick(context: Context, fragment: Fragment) {
        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .start(context, fragment)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): File? {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                result.uri.path?.let { filePath ->
                    return File(filePath)
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Log.i("Adon", result.error.toString())
            }
        }
        return null
    }

}