@file:Suppress("SameParameterValue", "SpellCheckingInspection")

package br.com.adonio.topclass.frags

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.frags.adapters.FragProposalAdapter
import br.com.adonio.topclass.frags.proposal.FragProposalView
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.ProposalRModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.frag_proposal.*

class FragProposal: Fragment() {

    enum class State {
        PENDENTE, APROVADO, EXPIRADO
    }

    // state
    private lateinit var vm: FragProposalVM
    private var mData = listOf<ProposalRModel>()
    private lateinit var mState: State
    private val mLoading = MutableLiveData<Boolean>().also { it.value = false }

    private lateinit var mAdapter: FragProposalAdapter

    // methods

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_proposal, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_empty.isVisible = false

        vm = ViewModelProviders.of(activity!!).get(FragProposalVM::class.java)

        //
        mLoading.value = false
        mLoading.observe(this, Observer {
            ui_progress.isVisible = it
        })

        //
        mState = State.PENDENTE

        // tabs
        TabIt.setup(ui_tabs, 0, "PENDENTES", null)
        TabIt.setup(ui_tabs, 1, "ACEITAS", null)
        TabIt.setup(ui_tabs, 2, "EXPIRADAS", null)

        // select first
        ui_tabs?.getTabAt(0)?.select()
        ui_tabs?.getTabAt(0)?.customView?.alpha = 1f
        ui_tabs?.getTabAt(1)?.customView?.alpha = 0.2f
        ui_tabs?.getTabAt(2)?.customView?.alpha = 0.2f

        ui_tabs.addOnTabSelectedListener(object : TabLayout.BaseOnTabSelectedListener<TabLayout.Tab> {
            override fun onTabReselected(p0: TabLayout.Tab?) {
                requestData()
            }
            override fun onTabUnselected(p0: TabLayout.Tab?) {
                p0?.customView?.alpha = 0.2f
            }
            override fun onTabSelected(tab: TabLayout.Tab?)
            {
                tab?.customView?.alpha = 1f

                mState = when (ui_tabs.selectedTabPosition) {
                    0 -> State.PENDENTE
                    1 -> State.APROVADO
                    2 -> State.EXPIRADO
                    else -> State.EXPIRADO
                }
                updateDesktop()
            }
        })

        //
        ui_info.text = "nada para exibir"

        //
        mAdapter = FragProposalAdapter(object : FragProposalAdapter.Listener {
            override fun onSelect(item: ProposalRModel, position: Int) {
                // expired
                if (ui_tabs?.selectedTabPosition == 2) return

                vm.selected = item
                FragProposalView.navigate(this@FragProposal, R.id.to_proposal_view)
            }
        })
        ui_rv.adapter = mAdapter
        ui_rv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
    }

    @SuppressLint("SetTextI18n")
    private fun updateDesktop()
    {
        val filtered = mData.filter { item ->
            when(mState) {
                State.PENDENTE -> item.p.isPhasePending
                State.APROVADO -> item.p.isPhaseDone
                State.EXPIRADO -> item.p.isPhaseExpired
            }
        }.let {  l ->
            if (mState != State.PENDENTE) l.reversed() else l
        }

        // generator
        val listGeneratorString: ((ProposalRModel)->Boolean)->String? = { b ->
            mData.filter(b).count().takeIf { it > 0 }?.let { "$it" }
        }

        // tabs
        val pendentesBadge = listGeneratorString { it.p.isPhasePending }
        TabIt.assign(ui_tabs, 0, null, pendentesBadge)

        val aprovadoBadge = listGeneratorString { it.p.isPhaseDone }
        TabIt.assign(ui_tabs, 1, null, aprovadoBadge)

        val expiradosBadge = listGeneratorString { it.p.isPhaseExpired }
        TabIt.assign(ui_tabs, 2, null, expiradosBadge)

        //
        val n = filtered.count()
        ui_empty.isVisible = n == 0

        ui_info.text = Str.plural(n, "sem propostas", "apenas uma proposta") {
            "$it propostas"
        }

        mAdapter.updateData(filtered, mState)
    }

    private fun requestData() {
        if (mLoading.value == true) return

        mLoading.value = true
        Http.get("proposals")
            .then {
                mData = ProposalRModel.parse(it)
                updateDesktop()
            }
            .catch {
                T.error(activity, it)
            }
            .always {
                mLoading.value = false
            }
    }

    //

    override fun onStart() {
        super.onStart()

        requestData()
    }
}