package br.com.adonio.topclass.dialogs

import android.graphics.Point
import android.view.Gravity
import android.view.WindowManager
import androidx.fragment.app.DialogFragment

abstract class DialogBase: DialogFragment() {

    override fun onResume() {
        val window = dialog?.window
        val size = Point()
        val display = window!!.windowManager.defaultDisplay
        display.getSize(size)
        window.setLayout((size.x * .95).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
        window.setGravity(Gravity.CENTER)

        super.onResume()
    }

}