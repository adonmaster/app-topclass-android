package br.com.adonio.topclass.helpers

import android.annotation.SuppressLint

@Suppress("UNCHECKED_CAST")
class Leaf<T>(val name: String, private val content: T?) {

    // static
    companion object {
        fun <V> root(): Leaf<V> {
            return Leaf("root", null)
        }
    }

    // state
    private val children: ArrayList<Leaf<T>> = arrayListOf()

    @SuppressLint("DefaultLocale")
    fun leaf(name: String): Leaf<T>? {
        return children.firstOrNull { it.name.trim().toLowerCase() == name.trim().toLowerCase() }
    }

    fun add(path: String, content: T?, contentName: String, pathDelimiter: String="/") {
        add(path.split(pathDelimiter), content, contentName)
    }

    fun add(path: List<String>, content: T?, contentName: String) {
        var curNode = this
        path
            .filter { it.isNotBlank() }
            .forEach { pathName ->
                val nodeFound = curNode.leaf(pathName)
                curNode = if (nodeFound==null) {
                    val newNode = Leaf<T>(pathName, null)
                    curNode.children.add(newNode)
                    newNode
                } else {
                    nodeFound
                }
            }
        curNode.children.add(Leaf(contentName, content))
    }

    fun children(): List<Leaf<T>> {
        return children
    }

    fun content(): T? {
        return this.content
    }

    fun toLines(): List<Line<T>> {
        val r = arrayListOf<Line<T>>()
        toLineRec(r,this, 0)
        return r
    }

    private fun toLineRec(r: ArrayList<Line<T>>, leaf: Leaf<T>, level: Int) {
        r.add(Line(leaf, level))
        leaf.children.forEach { child ->
            toLineRec(r, child, level + 1)
        }
    }

    fun whenContent(cb: (T)->Unit) {
        content?.let(cb)
    }

    fun findFirstContent(): T? {
        if (content != null) return content
        children.forEach {
            val v = it.findFirstContent()
            if (v!=null) return v
        }
        return null
    }

    // misc
    data class Line<I>(val leaf: Leaf<I>, val level: Int)

}