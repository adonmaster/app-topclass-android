package br.com.adonio.topclass.helpers

class LightCache {

    private val cache = HashMap<String, Any?>()

    @Suppress("UNCHECKED_CAST")
    fun <T> get(key: String, cb: ()->T?, forceRefresh: Boolean=false): T? {
        if ( ! forceRefresh && cache.keys.contains(key)) return cache[key] as T?
        return cb().also { cache[key] = it }
    }

    fun <T> getGranted(key: String, cb: ()->T, forceRefresh: Boolean=false): T {
        return get(key, cb, forceRefresh)!!
    }

    fun remove(key: String) {
        cache.remove(key)
    }

    fun clear() {
        cache.clear()
    }

}