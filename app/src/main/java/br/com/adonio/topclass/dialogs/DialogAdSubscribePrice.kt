package br.com.adonio.topclass.dialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.helpers.Str
import kotlinx.android.synthetic.main.dialog_ad_subscribe_price.*

@SuppressLint("SetTextI18n")
class DialogAdSubscribePrice: DialogBase() {

    // static
    companion object {
        private var listener: Listener? = null
        fun show(fm: FragmentManager?, price: Double) {
            val bundle = Bundle()
            bundle.putDouble("price", price)
            val dialog = DialogAdSubscribePrice()
            dialog.arguments = bundle

            dialog.show(fm, "DialogAdSubscribePrice")
        }
        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            listener = null
        }
    }

    //
    private var mPrice = MutableLiveData<Double>()


    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog.setTitle("Aceitar anúncio")
        return inflater.inflate(R.layout.dialog_ad_subscribe_price, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mPrice.observe(this, Observer {
            ui_price.text = "R$ " + Str.number(it)

            ui_btn_submit.setEnabledEx(it > 0)
        })
        mPrice.value = arguments!!.getDouble("price")

        ui_btn_p10.setOnClickListener { incPrice(10) }
        ui_btn_p20.setOnClickListener { incPrice(20) }
        ui_btn_p50.setOnClickListener { incPrice(50) }

        ui_btn_m10.setOnClickListener { incPrice(-10) }
        ui_btn_m20.setOnClickListener { incPrice(-20) }
        ui_btn_m50.setOnClickListener { incPrice(-50) }

        ui_btn_cancel.setOnClickListener { dismissAllowingStateLoss() }

        ui_btn_submit.setOnClickListener {
            dismissAllowingStateLoss()

            listener?.dialogAdSubscribePrice(mPrice.value!!)
        }
    }

    private fun incPrice(v: Int) {
        var r = mPrice.value!! + v
        if (r < 0) r = 0.0
        mPrice.value = r
    }

    //
    interface Listener {
        fun dialogAdSubscribePrice(price: Double)
    }
}