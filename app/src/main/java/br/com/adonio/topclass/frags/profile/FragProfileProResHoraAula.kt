package br.com.adonio.topclass.frags.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Num
import br.com.adonio.topclass.helpers.Str
import br.com.adonio.topclass.structs.Cs
import kotlinx.android.synthetic.main.frag_profile_pro_res_hora_aula.*

class FragProfileProResHoraAula: Fragment() {

    //
    private lateinit var vm: FragProfileProResolvedVM
    private var mValue = MutableLiveData<Double>()

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_profile_pro_res_hora_aula, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragProfileProResolvedVM::class.java)

        vm.root.observe(this, Observer {
            updateDesktop()
        })

        listOf(
            ui_btn_p10 to { incValue(10.0) },
            ui_btn_p20 to { incValue(20.0) },
            ui_btn_p50 to { incValue(50.0) },
            ui_btn_m10 to { incValue(-10.0) },
            ui_btn_m20 to { incValue(-20.0) },
            ui_btn_m50 to { incValue(-50.0) }
        ).forEach { pair -> pair.first.setOnClickListener { pair.second() } }

        mValue.observe(this, Observer {
            val s = "R$ " + Str.number(it)
            ui_value.text = s
        })
    }

    private fun incValue(d: Double) {
        val r = mValue.value!! + d
        mValue.value = if (r >= 0) r else 0.0

        FragProfileProResolved.shared?.outSetHoraAula(ui_value.text.toString())
    }

    private fun updateDesktop() {
        val comp = vm.root.value
            ?.children()
            ?.find { it.name == Cs.SKILL_CATEGORY_HORA_AULA }
            ?.findFirstContent()
            ?.checkedComp ?: "R$ 0"

        mValue.value = Num.fromBrCurrency(comp, 0.0)
    }

}