@file:Suppress("unused", "KDocUnresolvedReference", "MemberVisibilityCanBePrivate")

package br.com.adonio.topclass.structs

import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView

class MaskWatcher(private val mask: String) : TextWatcher {

    private var isRunning = false
    private var isDeleting = false

    override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {
        isDeleting = count > after
    }

    override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        if (isRunning || isDeleting) {
            return
        }
        isRunning = true

        editable.replace(0, editable.length, maskIt(mask, editable.toString(), false))
        /**
        val editableLength = editable.length
        if (editableLength < mask.length) {
        if (mask[editableLength] != '#') {
        editable.append(mask[editableLength])
        } else if (mask[editableLength - 1] != '#') {
        editable.insert(editableLength - 1, mask, editableLength - 1, editableLength)
        }
        }
         */
        isRunning = false
    }

    companion object {

        fun bind(v: TextView?, pattern: String) {
            v?.addTextChangedListener(MaskWatcher(pattern))
        }
        fun bindCpf(v: TextView?) = bind(v, "###.###.###-##")
        fun bindPhone(v: TextView?) = bind(v, "(##) ####-####")
        fun bindCellPhone(v: TextView?) = bind(v, "(##) #####-####")
        fun bindCard(v: TextView?) = bind(v, "#### #### #### ####")

        fun maskIt(mask: String, s: String?, allowTail: Boolean): String? {
            if (s==null) return null
            if (s.isEmpty()) return ""

            val list = mask.toCharArray().map { it.toString() }
            val r = mutableListOf<String>()

            var requestCount = 0
            val request: (Boolean)->String? = { increment ->
                (if (requestCount < s.length) s[requestCount].toString() else null).also {
                    if (increment) requestCount++
                }
            }

            for (cur in list) {
                if (cur == "#") {
                    val requested = request(true) ?: break
                    r.add(requested)
                } else {
                    r.add(cur)
                    val requested = request(false) ?: continue
                    if (requested == cur) request(true)
                }
            }

            while(allowTail) { r.add(request(true) ?: break) }

            return r.joinToString("")
        }
    }
}