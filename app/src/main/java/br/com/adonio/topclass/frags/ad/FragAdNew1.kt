package br.com.adonio.topclass.frags.ad

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.dialogs.DialogProposalDate
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.frags.adapters.FragProProposalNewDatesAdapter
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.structs.ProposalBody
import br.com.adonio.topclass.structs.SimpleAddress
import com.smartlib.addresspicker.AddressPickerActivity
import kotlinx.android.synthetic.main.frag_ad_new_1.*
import org.threeten.bp.LocalDateTime

class FragAdNew1: Fragment(), DialogProposalDate.Listener {

    //
    private lateinit var vm: FragAdNew1VM
    private var mDatesAdapter = FragProProposalNewDatesAdapter()

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_ad_new_1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        vm = ViewModelProviders.of(this).get(FragAdNew1VM::class.java)

        //
        vm.loading.observe(this, Observer {
            ui_progress?.isVisible = it > 0
            ui_btn_submit?.setEnabledEx(it == 0)
        })

        //
        vm.items.value = listOf()
        vm.items.observe(this, Observer { list ->
            ui_dates_empty.isVisible = list.count() == 0
            mDatesAdapter.updateData(list)

            ui_lbl_dates_info.text = Str.plural(
                list.count(),
                "sem aulas programadas",
                "apenas uma aula programada"
            ) { "$it aulas programadas" }
        })

        //
        vm.address.observe(this, Observer {
            ui_location.text = if (it==null) {
                Span("Aperte abaixo para escolher localização...")
                    .color(context!!, R.color.md_grey_500)
                    .build()
            } else {
                Span(it.address)
                    .color(context!!, R.color.md_grey_500)
                    .build()
            }
        })
        vm.address.value = null

        //
        ui_btn_location.setOnClickListener {
            startActivityForResult(Intent(activity, AddressPickerActivity::class.java), 165)
        }

        //
        ui_btn_date_time.setOnClickListener {
            val classes = resources.getStringArray(R.array.classes).toList()
            DialogProposalDate.show(childFragmentManager, classes)
        }

        //
        mDatesAdapter.listener = object : FragProProposalNewDatesAdapter.Listener {
            override fun onDelete(position: Int) {
                val mm = vm.items.value!!.toMutableList()
                mm.removeAt(position)
                vm.items.value = mm
            }
        }
        ui_rv_dates.adapter = mDatesAdapter
        ui_rv_dates.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        ui_rv_dates.layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)

        ui_btn_submit.setOnClickListener {
            submit()
        }
    }

    private fun submit() {
        if (! validate()) return
        if (vm.loading.value!! > 0) return

        //
        vm.address.value!!.comp = ui_address_comp.text.toString()
        val address = vm.address.value!!
        val items = vm.items.value!!
        val body = ProposalBody.toJson(items, address)
        val expireAt = Dt.formatSqlDate(items.minBy { it.dt }!!.dt)!!
        val params = HttpFormData()
            .put("body", body)
            .put("expire_at", expireAt)

        vm.loadingInc(1)
        Http.post("ads", params)
            .then {

                findNavController().popBackStack()

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun validate(): Boolean {
        when {
            vm.items.value!!.count() == 0 -> {
                T.error(activity, "Inclua nem que seja uma aula!")
            }
            vm.address.value == null -> {
                T.error(activity, "Por favor, escolha o local da aula!")
            }
            else -> {
                return true
            }
        }
        return false
    }

    //
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode==165 && resultCode == Activity.RESULT_OK) {
            val address = data?.getParcelableExtra<Address>(AddressPickerActivity.RESULT_ADDRESS)
                ?: return
            val addressLine = address.getAddressLine(0) ?: return
            validateAddress(addressLine)
                .then {

                    vm.address.value = SimpleAddress(
                        addressLine, address.latitude, address.longitude, null
                    )

                }
                .catch {
                    DialogOk.show(context, it, "Erro")
                }
        }
    }

    @SuppressLint("DefaultLocale")
    private fun validateAddress(addressLine: String) = Promise<Boolean> { resolve, reject ->

        val line = addressLine.toLowerCase().replace("goiânia", "goiania")

        if (!line.contains("goiania")) {
            reject("Esta versão permite apenas localizações dentro da cidade de Goiânia-GO.\nAguarde atualizações!")
            return@Promise
        }

        resolve(true)
    }

    override fun dialogProposalDates(dt: LocalDateTime, duration: Int, materia: String)
    {
        validateDate(dt)
            .then {

                val mutable = vm.items.value?.toMutableList() ?: mutableListOf()
                mutable.add(ProposalBody.Item(dt, materia, duration))
                vm.items.value = mutable

            }
            .catch {
                DialogOk.show(context, it, "Erro")
            }
    }

    @SuppressLint("DefaultLocale")
    private fun validateDate(d: LocalDateTime) = Promise<Boolean> { resolve, reject ->

        if (LocalDateTime.now() > d) {
            reject("Esta data já passou. Selecione uma nova.")
            return@Promise
        }

        resolve(true)
    }

    //

    override fun onStart() {
        super.onStart()

        DialogProposalDate.attach(this)
    }

    override fun onStop() {
        super.onStop()

        DialogProposalDate.detach()
    }

}