@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package br.com.adonio.topclass.helpers

import android.widget.TextView
import androidx.core.view.isVisible
import br.com.adonio.topclass.R
import com.google.android.material.tabs.TabLayout

object TabIt {

    private const val tabLayoutId: Int = R.layout.tab_inverted

    fun setup(tabs: TabLayout, index: Int, text: String, badge: String?) {
        tabs.getTabAt(index)?.setCustomView(tabLayoutId)
        assign(tabs, index, op(text), badge)
    }

    fun assign(tabs: TabLayout, index: Int, text: Op<String>?, badge: Int) {
        val b = ter(badge > 0, "$badge", null)
        assign(tabs, index, text, b)
    }

    fun assign(tabs: TabLayout, index: Int, text: Op<String>?, badge: String?) {
        val customView = tabs.getTabAt(index)?.customView
        val uiDesc = customView?.findViewById<TextView>(R.id.ui_tab_desc) ?: return
        val uiBadge = customView.findViewById<TextView>(R.id.ui_tab_badge) ?: return

        text?.let { uiDesc.text = it.value }
        uiBadge.text = badge ?: ""
        uiBadge.isVisible = badge != null
    }

}