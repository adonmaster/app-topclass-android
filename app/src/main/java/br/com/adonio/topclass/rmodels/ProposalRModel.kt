package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.ProposalRModelPresenter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class ProposalRModel(
    val id: Int,
    val clientId: Int, val clientName: String, val clientAvatar: String?,
    val price: Double, val phase: Int,
    val body: String?,
    val proId: Int, val proName: String, val proAvatar: String?,
    val expireAt: LocalDateTime,
    val createdAt: LocalDateTime
) {

    val p by lazy { ProposalRModelPresenter(this) }

    companion object {
        fun parse(json: JSONObject) = JParser(json) {
            arr("payload") {
                store("id")
                store("price")
                store("phase")
                store("body")
                storeSeek("client_id", "client_user.id")
                storeSeek("client_name", "client_user.name")
                storeSeek("client_avatar", "client_user.avatar_thumb")
                storeSeek("pro_id", "pro_user.id")
                storeSeek("pro_name", "pro_user.name")
                storeSeek("pro_avatar", "pro_user.avatar_thumb")
                store("expire_at")
                store("created_at")
            }
        }.parse {
            arr("payload") {
                ProposalRModel(
                    get("id"),
                    get("client_id"),
                    get("client_name"),
                    geto("client_avatar"),
                    get("price"),
                    get("phase"),
                    getStrOrJson("body"),
                    get("pro_id"),
                    get("pro_name"),
                    geto("pro_avatar"),
                    Dt.fromJson(get("expire_at"))!!,
                    Dt.fromJson(get("created_at"))!!
                )
            }
        }

        @Suppress("unused")
        val sample = """
            {
              "message": "Operação executada com êxito.",
              "payload": [
                {
                  "id": 1,
                  "client_user_id": 1,
                  "pro_user_id": 1,
                  "body": {
                    "try to foyo": "positrons dont react like that"
                  },
                  "price": 4555,
                  "phase": 2,
                  "expire_at": "2020-01-27 22:02:09",
                  "created_at": "2020-01-25 22:02:09",
                  "updated_at": "2020-01-25 22:04:12",
                  "client_user": {
                    "id": 1,
                    "name": "Adonho",
                    "email": "adonio.silva@gmail.com",
                    "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
                    "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
                    "email_verified_at": "2020-01-25 23:23:03",
                    "last_active": "2020-01-25 23:23:03",
                    "is_pro": true,
                    "created_at": "2019-11-04 20:33:34",
                    "updated_at": "2020-01-25 23:23:03"
                  },
                  "pro_user": {
                    "id": 1,
                    "name": "Adonho",
                    "email": "adonio.silva@gmail.com",
                    "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
                    "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
                    "email_verified_at": "2020-01-25 23:23:03",
                    "last_active": "2020-01-25 23:23:03",
                    "is_pro": true,
                    "created_at": "2019-11-04 20:33:34",
                    "updated_at": "2020-01-25 23:23:03"
                  }
                },
        """.trimIndent()
    }
}