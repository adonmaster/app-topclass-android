package br.com.adonio.topclass.models

import androidx.room.Dao
import androidx.room.Query

@Dao
abstract class ConfigModelDao: BaseDao<ConfigModel>() {

    @Query("select * from config where id = :id")
    abstract override fun find(id: Long): ConfigModel?

    @Query("select * from config where `key` = :key limit 1")
    abstract fun findKey(key: String): ConfigModel?

}