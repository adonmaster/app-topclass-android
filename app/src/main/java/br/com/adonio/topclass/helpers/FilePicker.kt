package br.com.adonio.topclass.helpers

import android.os.Environment
import androidx.fragment.app.Fragment
import com.developer.filepicker.model.DialogConfigs
import com.developer.filepicker.model.DialogProperties
import com.developer.filepicker.view.FilePickerDialog
import java.io.File

object FilePicker {

    fun pick(frag: Fragment, exts: Array<String>?, listener: (File)->Unit) {
        val properties = DialogProperties()
        properties.selection_mode = DialogConfigs.SINGLE_MODE
        properties.selection_type = DialogConfigs.FILE_SELECT
        properties.root = Environment.getExternalStorageDirectory()
        properties.extensions = exts

        val dialog = FilePickerDialog(frag.context, properties)

        dialog.setTitle("Selecione...")

        dialog.setNegativeBtnName("CANCELAR")
        dialog.setPositiveBtnName("SELECIONAR")

        dialog.setDialogSelectionListener {
            it.firstOrNull()?.let { p ->
                listener(File(p))
            }
        }

        dialog.show()
    }

}