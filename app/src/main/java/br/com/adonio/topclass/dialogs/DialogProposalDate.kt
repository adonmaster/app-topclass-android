@file:Suppress("unused")

package br.com.adonio.topclass.dialogs

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.adapters.DialogProposalDateAdapter
import br.com.adonio.topclass.helpers.Dt
import kotlinx.android.synthetic.main.dialog_proposal_date.*
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime

class DialogProposalDate: DialogBase() {

    private lateinit var mCalendarAdapter: DialogProposalDateAdapter
    private var mSelectedDate = LocalDate.now()
    private var mTime = LocalTime.now()

    //
    private val oClasses
        get() = arguments?.getStringArray("classes")?.toList() ?: listOf()

    //
    companion object {
        private var listener: Listener? = null
        fun show(fm: FragmentManager?, classes: List<String>) {
            val dialog = DialogProposalDate()
            val bundle = Bundle()
            bundle.putStringArray("classes", classes.toTypedArray())
            dialog.arguments = bundle
            //
            dialog.show(fm, "DialogProposalDates")
        }
        fun attach(listener: Listener) {
            this.listener = listener
        }
        fun detach() {
            listener = null
        }
    }

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog.setTitle("Data, hora e duração")
        return inflater.inflate(R.layout.dialog_proposal_date, container, false)
    }

    @SuppressLint("DefaultLocale")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //
        mCalendarAdapter = DialogProposalDateAdapter(object :
            DialogProposalDateAdapter.Listener {
            override fun onSelect(days: List<LocalDate>) {
                mSelectedDate = days.first()
            }
            override fun onSelCount() = 1
            override fun onMonth(day: LocalDate) {
                ui_month_name.text = Dt.format(day, "MMMM yyyy")!!.toUpperCase()
            }
        })
        rv_calendar.adapter = mCalendarAdapter
        rv_calendar.layoutManager = GridLayoutManager(context, 7, RecyclerView.VERTICAL, false)

        //
        ui_btn_month_next.setOnClickListener {
            mCalendarAdapter.changeMonth(1)
        }
        ui_btn_month_prior.setOnClickListener {
            mCalendarAdapter.changeMonth(-1)
        }

        //
        val hourRange = 0..23
        ui_hour.adapter = generateAdapter(hourRange, 1, false) {
            it.toString().padStart(2, '0')
        }
        ui_hour.setSelection(hourRange.toList().indexOf(mTime.hour))

        val minRange = 0..59
        ui_minute.adapter = generateAdapter(minRange, 5, false) {
            it.toString().padStart(2, '0')
        }
        ui_minute.setSelection(minRange.toList().indexOf(mTime.minute) / 5)

        //
        ui_class_duration.adapter = generateAdapter(60..120, 30, false) { "$it mins" }

        //
        ui_classes.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, oClasses)

        //
        ui_btn_submit.setOnClickListener {
            dismissAllowingStateLoss()

            val hour = (ui_hour.selectedItem as String).toInt()
            val minute = (ui_minute.selectedItem as String).toInt()
            val dt = mSelectedDate.atTime(hour, minute)
            val duration = (ui_class_duration.selectedItem as String).replace(" mins", "").toInt()
            val materia = (ui_classes.selectedItem as? String) ?: "[desconhecido]"

            listener?.dialogProposalDates(dt, duration, materia)
        }

        ui_btn_cancel.setOnClickListener {
            dismissAllowingStateLoss()
        }
    }

    private fun generateAdapter(range: IntRange, step: Int=1, reverse: Boolean, converter: ((Int)->String)?=null): SpinnerAdapter? {
        val list = range
            .step(step)
            .toList()
            .let { if (reverse) it.reversed() else it }
            .map { converter?.invoke(it) ?: it.toString() }
        return ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, list)
    }


    // listener
    interface Listener {
        fun dialogProposalDates(dt: LocalDateTime, duration: Int, materia: String)
    }
}