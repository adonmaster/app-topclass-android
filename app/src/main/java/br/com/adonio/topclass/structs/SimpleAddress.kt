package br.com.adonio.topclass.structs

data class SimpleAddress(val address: String, val lat: Double, val lng: Double, var comp: String?)