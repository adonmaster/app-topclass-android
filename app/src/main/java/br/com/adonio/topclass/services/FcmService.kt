package br.com.adonio.topclass.services

import br.com.adonio.topclass.MainActivity
import br.com.adonio.topclass.helpers.Ev
import br.com.adonio.topclass.helpers.Notify
import br.com.adonio.topclass.processors.FcmProcessor
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FcmService: FirebaseMessagingService() {

    override fun onNewToken(token: String?) {
        FcmProcessor.setToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage)
    {
        val msg = Notify.extractMessage(remoteMessage)
        if (msg != null) {
            Ev.fireMain(MainActivity.KEY_INFO, msg)
        }

        if ( ! MainActivity.isActive()) {
            Notify.from(remoteMessage).show(this, MainActivity::class.java)
        }
    }

}