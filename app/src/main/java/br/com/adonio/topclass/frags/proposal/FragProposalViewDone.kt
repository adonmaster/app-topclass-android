package br.com.adonio.topclass.frags.proposal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.MainActivity
import br.com.adonio.topclass.R
import kotlinx.android.synthetic.main.frag_proposal_view_done.*

class FragProposalViewDone: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_proposal_view_done, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ui_btn_ok.setOnClickListener {
            MainActivity.navigateProposal(findNavController())
        }
    }

}