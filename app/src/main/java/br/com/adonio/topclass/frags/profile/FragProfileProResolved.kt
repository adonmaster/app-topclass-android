package br.com.adonio.topclass.frags.profile

import android.os.Bundle
import android.view.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.SkillsRModel
import br.com.adonio.topclass.structs.Cs
import br.com.adonio.topclass.structs.FragmentPagerAdapterPaging
import kotlinx.android.synthetic.main.frag_profile_pro_resolved.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.ref.WeakReference

class FragProfileProResolved: Fragment() {

    // static
    companion object {
        private var instance = WeakReference<FragProfileProResolved>(null)
        val shared get() = instance.get()
    }

    // state
    private lateinit var vm: FragProfileProResolvedVM
    private var mChanged = mutableListOf<SkillsRModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = WeakReference(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.frag_profile_pro_resolved, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragProfileProResolvedVM::class.java)

        vm.loading.observe(this, Observer {
            ui_progress.isVisible = it > 0
        })

        vm.root.observe(this, Observer {
            buildPages(it)
        })

        updateDesktop()
    }


    private fun buildPages(root: Leaf<SkillsRModel>)
    {
        // list
        val pages = arrayListOf<FragmentPagerAdapterPaging.Page>()
        for (skill in root.children()) {
            pages.add(
                FragmentPagerAdapterPaging.Page(skill.name) {
                    if (skill.name == Cs.SKILL_CATEGORY_HORA_AULA)
                        FragProfileProResHoraAula()
                    else
                        FragProfileProResolvedPage.f(skill.name)
                }
            )
        }

        // adapter
        ui_pager.adapter = FragmentPagerAdapterPaging.from(childFragmentManager, pages)
        ui_tab_layout.setupWithViewPager(ui_pager)
    }

    private fun updateDesktop()
    {
        // title
        val changeCount = mChanged.count().takeIf { it > 0 }

        ui_skill_title.text = changeCount?.let { n ->
            "Não se esqueça de salvar suas alterações ($n)"
        }
    }

    fun outOnCheckedModel(model: SkillsRModel) {
        val index = mChanged.indexOfFirst { it.id == model.id }
        if (index == -1) mChanged.add(model)

        updateDesktop()
    }

    fun outSetHoraAula(v: String) {
        val model = vm.root.value!!
            .children()
            .find { it.name == Cs.SKILL_CATEGORY_HORA_AULA }
            ?.findFirstContent()
            ?: return

        model.checkedComp = v
        model.checked = true

        if ( ! mChanged.contains(model)) mChanged.add(model)

        updateDesktop()
    }

    private fun requestData()
    {
        vm.loadingInc(1)
        Http.get("skills")
            .then {

                val list = SkillsRModel.parse(it)

                val root = Leaf.root<SkillsRModel>()
                list.forEach { model ->
                    val directories = listOf(model.p.category(), model.p.subCategory())
                    root.add(directories, model, model.p.desc())
                }

                vm.root.value = root
            }
            .catch {
                T.error(activity!!, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun save() {
        if (mChanged.count() <= 0) return

        val jskills = JSONArray()
        mChanged.forEach { item ->
            val json = JSONObject()
            json.put("id", item.id)
            json.put("option", item.checked)
            json.put("comp", item.checkedComp)
            jskills.put(json)
        }
        val params = HttpFormData().put("skills", jskills)

        vm.loadingInc(1)
        Http.post("skills", params)
            .then {
                mChanged.clear()

                T.success(activity, "Dados salvos com sucesso!")

                updateDesktop()
            }
            .catch {
                T.error(activity!!, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    // ui

    override fun onStart() {
        super.onStart()

        //
        updateDesktop()
        requestData()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_frag_profile_pro_resolved, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_action_save -> save()
        }
        return super.onOptionsItemSelected(item)
    }

}