package br.com.adonio.topclass.helpers

import org.json.JSONArray
import org.json.JSONObject

fun JEncoder(cb: JEncoderDsl<Any>.()->Unit): JEncoderDsl<Any> {
    return JEncoderDsl(null, cb)
}

class JEncoderDsl<T>(private val item: T?=null, cb: JEncoderDsl<T>.()->Unit) {

    private val json = JSONObject()

    val i get() = item!!

    init {
        cb(this)
    }

    fun store(field: String, v: Any?) {
        json.put(field, v ?: JSONObject.NULL)
    }

    fun get() = json

    fun <T> arr(field: String, list: List<T>, cb: JEncoderDsl<T>.()->Unit) {
        val array = JSONArray()
        list.forEachIndexed { cont, item ->
            val instance = JEncoderDsl(item, cb)
            array.put(cont, instance.get())
        }
        json.put(field, array)
    }

    fun <T> obj(field: String, item: T?, cb: JEncoderDsl<T>.()->Unit) {
        val i = guard(item) { return }
        json.put(field, JEncoderDsl(i, cb).get())
    }

    fun obj(field: String, cb: JEncoderDsl<Int>.()->Unit) {
        obj(field, 0, cb)
    }

    fun <T> arrPrimitives(field: String, list: List<T>) {
        val arr = JSONArray()
        list.forEachIndexed { index, item ->
            arr.put(index, item)
        }
        json.put(field, arr)
    }

}
