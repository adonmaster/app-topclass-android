package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.ProQueryPresenter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class ProQueryRModel(
    val id: Int,
    val name: String,
    val email: String,
    val avatar: String?,
    val avatarThumb: String?,
    val lastActive: LocalDateTime,
    val skills: List<Skill>
) {

    data class Skill(
        val id: Int,
        val desc: String,
        val pivotComp: String?,
        val pivotOption: Boolean
    )

    companion object {

        fun parse(json: JSONObject) = JParser(json) {
            arr("payload") {
                store("id")
                store("name")
                store("email")
                store("avatar")
                store("avatar_thumb")
                store("last_active")
                arr("skills") {
                    store("id")
                    store("desc")
                    storeSeek("pivot_comp", "pivot.comp")
                    storeSeek("pivot_option", "pivot.option")
                }
            }
        }.parse {
            arr("payload") {
                ProQueryRModel(
                    get("id"), get("name"), get("email"),
                    geto("avatar"), geto("avatar_thumb"),
                    Dt.fromJson(geto("last_active")) ?: LocalDateTime.now(),
                    arr("skills") {
                        Skill(
                            get("id"), get("desc"), geto("pivot_comp"),
                            getBoolean("pivot_option")
                        )
                    }
                )
            }
        }

        @Suppress("unused")
        val sampleJson get() = JSONObject(sample)

        @Suppress("unused")
        val sample = """
            {
  "message": "Operação executada com êxito.",
  "payload": [
    {
      "id": 1,
      "name": "Adonho",
      "email": "adonio.silva@gmail.com",
      "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
      "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
      "email_verified_at": "2019-11-19 17:34:22",
      "last_active": "2019-11-20 20:04:44",
      "is_pro": true,
      "created_at": "2019-11-04 20:33:34",
      "updated_at": "2019-11-21 20:12:53",
      "skills": [
        {
          "id": 1,
          "desc": "Língua Portuguesa/Fundamental 1",
          "created_at": "2019-11-21 12:27:16",
          "updated_at": "2019-11-21 12:27:16",
          "pivot": {
            "user_id": 1,
            "skill_id": 1,
            "comp": null,
            "option": 1
          }
        },
        {
          "id": 2,
          "desc": "Língua Portuguesa/Fundamental 2",
          "created_at": "2019-11-21 12:27:16",
          "updated_at": "2019-11-21 12:27:16",
          "pivot": {
            "user_id": 1,
            "skill_id": 1,
            "comp": null,
            "option": 1
          }
        },
        {
          "id": 3,
          "desc": "Língua Chinesa/Fundamental 2",
          "created_at": "2019-11-21 12:27:16",
          "updated_at": "2019-11-21 12:27:16",
          "pivot": {
            "user_id": 1,
            "skill_id": 1,
            "comp": null,
            "option": 1
          }
        }
      ]
    },
    {
      "id": 5,
      "name": "A Rodolfo Pinheiro",
      "email": "rodolfinhocp@gmail.com",
      "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574469337_pmEA67DF3LVu.thumb.jpg",
      "avatar": "http://topclass.adonio.com.br/storage/attachments/1574469337_vaPxV5cVaauw.jpg",
      "email_verified_at": "2019-11-22 21:09:17",
      "last_active": "2019-11-22 21:35:37",
      "is_pro": true,
      "created_at": "2019-11-20 20:16:13",
      "updated_at": "2019-11-22 21:35:37",
      "skills": []
    }
  ]
}
        """.trimIndent()
    }

    val p by lazy { ProQueryPresenter(this) }

}