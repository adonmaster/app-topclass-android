package br.com.adonio.topclass.helpers

import android.annotation.SuppressLint
import android.content.Context
import br.com.adonio.topclass.R
import java.text.NumberFormat
import java.util.*
import kotlin.random.Random


object Str {

    private const val mCollectionAlphaNumeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_-"
    private var mRandom = Random(23451)

    fun random(len: Int): String {
        val sb = StringBuilder(len)
        sb.append(Calendar.getInstance().time.time)
        for (i in 0 until len-sb.count()) {
            sb.append(
                mCollectionAlphaNumeric[mRandom.nextInt(
                    mCollectionAlphaNumeric.length)])
        }
        return sb.toString()
    }

    fun coalesce(vararg args: String?): String {
        for (arg in args) {
            if (arg!=null) return arg
        }
        return ""
    }

    fun colorEmpty(context: Context, s: String?, emptyText: String): CharSequence? {
        return if (s?.trim()?.isEmpty() == false) s else Span(
            emptyText
        )
            .color(context, R.color.md_grey_700)
            .build()
    }

    fun upper(s: String?): String? {
        return s?.toUpperCase(Locale.getDefault())
    }

    @SuppressLint("DefaultLocale")
    fun containsInsensitive(needle: String, vararg haystack: String): Boolean {
        for (cur in haystack) {
            if (needle.trim().toLowerCase() == cur.trim().toLowerCase()) {
                return true
            }
        }
        return false
    }

    fun extractYTIdFrom(url: String?): String? {
        if (url==null) return null
        val regex = """(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/user/\S+|/ytscreeningroom\?v=|/sandalsResorts#\w/\w/.*/))([^/&]{10,12})"""
            .toRegex()
        val groups = regex.find(url)?.groupValues
        return groups?.getOrNull(1)
    }

    fun plural(n: Int, zero: String, one: String, more: (Int) -> String): String {
        if (n <= 0) return zero
        if (n == 1) return one
        return more(n)
    }

    fun plural(n: Int, zero: String, one: String, more: String): String {
        return plural(n, zero, one) { more.replace("$0", "$it") }
    }

    fun number(d: Double, digits: Int = 2): String {
        val nf = NumberFormat.getInstance()
        nf.minimumFractionDigits = digits
        nf.maximumFractionDigits = digits
        nf.isGroupingUsed = true
        return nf.format(d)
    }

    inline fun whenNotEmpty(s: String?, cb: (String) -> Unit) {
        if (s?.trim()?.isNotBlank() != true) return
        cb(s)
    }


}