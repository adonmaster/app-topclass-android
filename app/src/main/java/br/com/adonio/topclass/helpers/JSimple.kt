package br.com.adonio.topclass.helpers

import org.json.JSONObject

class JSimple(val json: JSONObject?) {

    inline fun <reified T> get(key: String): T? {
        if (json == null) return null
        if (!json.has(key)) return null
        if (json.isNull(key)) return null
        return json.get(key) as? T
    }

}