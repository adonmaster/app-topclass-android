package br.com.adonio.topclass.frags

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.TransactionRModel
import kotlinx.android.synthetic.main.frag_transaction.*
import kotlinx.android.synthetic.main.rv_frag_transaction.view.*
import kotlinx.android.synthetic.main.rv_frag_transaction_header.view.*

class FragTransaction: Fragment(), RvCategorized.Owner<TransactionRModel.Item> {

    //
    private lateinit var vm: FragTransactionVM
    private lateinit var mAdapter: RvCategorized<TransactionRModel.Item>

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragTransactionVM::class.java)

        //
        vm.loading.observe(this, Observer {
            if (it == 0) ui_swipe?.isRefreshing = false
            ui_progress?.isVisible = it > 0
        })

        //
        ui_swipe.setOnRefreshListener {
            ui_swipe?.isRefreshing = true
            requestData()
        }

        //
        mAdapter = RvCategorized(
            this, "", R.layout.rv_frag_transaction, R.layout.rv_frag_transaction_header
        ) {
            Dt.formatBrDate(it.createdAt)!!
        }
        ui_rv.adapter = mAdapter
        Rv.setupForTable(ui_rv, context)

        //
        requestData()
    }

    private fun requestData() {
        vm.loadingInc(1)
        Http.get("transactions")
            .then {

                val rmodel = TransactionRModel.parse(it)
                val list = rmodel.items
                mAdapter.updateData(list)

                ui_empty.isVisible = list.count() == 0

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }


    //

    override fun rvCreateViewBothViews(
        identifier: String,
        view: View,
        position: () -> Int,
        category: () -> String?,
        data: () -> TransactionRModel.Item?
    ) {}

    @SuppressLint("SetTextI18n")
    override fun rvBindData(identifier: String, view: View, value: TransactionRModel.Item) {
        view.ui_desc1.text = value.desc

        val dtExt = Dt.format(value.createdAt.toLocalDate(), "dd/MM")!!
        val dtFrom = Dt.fromNow(value.createdAt)!!
        view.ui_desc2.text = "$dtFrom ($dtExt)"

        //
        val monetizer: (TextView?, Double, Boolean)->Unit = { v, d, showCurrency ->
            v?.text = ter(showCurrency, "R$ ", "") + Str.number(d)
            v?.setTextColor(if (d < 0) Cl.md_red_500() else Cl.md_green_500())
        }

        //
        monetizer(view.ui_price1, value.amount, false)

        //
        monetizer(view.ui_price2, value.balance, true)

        //
        view.ui_badge.text = value.p.operationTypeDesc
        view.ui_badge.setBackgroundColor(value.p.operationTypeCl)
    }

    override fun rvBindCategory(identifier: String, view: View, value: String) {
        view.ui_header_title.text = value
    }

}