package br.com.adonio.topclass.presenters

import android.widget.TextView
import br.com.adonio.topclass.helpers.Cl
import br.com.adonio.topclass.rmodels.TransactionRModel

class TransactionRModelPresenter(model: TransactionRModel.Item) :
    BasePresenter<TransactionRModel.Item>(model)
{

    val operationTypeDesc by lazy {
        when(model.operationType) {
            "fees" -> "taxas"
            "credit" -> "crédito"
            else -> model.operationType
        }
    }

    val operationTypeCl by lazy {
        when (model.operationType) {
            "fees" -> Cl.md_orange_500()
            "credit" -> Cl.md_green_500()
            else -> Cl.md_blue_500()
        }
    }

}