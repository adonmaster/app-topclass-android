package br.com.adonio.topclass.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.adonio.topclass.R
import br.com.adonio.topclass.helpers.Act
import br.com.adonio.topclass.helpers.Cl
import br.com.adonio.topclass.helpers.Dt
import kotlinx.android.synthetic.main.rv_dialog_proposal_date.view.*
import org.threeten.bp.LocalDate

class DialogProposalDateAdapter(private val listener: Listener):
    RecyclerView.Adapter<DialogProposalDateAdapter.VH>() {

    // class
    data class Item(val text: String, val dt: LocalDate?)

    //
    private var mSelectedDates = arrayListOf<LocalDate>()
    private val mData = arrayListOf<Item>()

    //
    private val cDisplayMonth
        get() = mData.getOrNull(20)

    // adapter

    init {
        selectDate(LocalDate.now())
    }

    private fun selectDate(d: LocalDate) {
        val n = listener.onSelCount()

        // selection
        if (mSelectedDates.contains(d)) {
            mSelectedDates.remove(d)
        } else {
            mSelectedDates.add(d)
            if (mSelectedDates.count() > n) mSelectedDates.removeAt(0)
        }
        if (mSelectedDates.count()==0 && n<=1) mSelectedDates.add(d)

        // build?
        buildDays(d)

        //
        notifyDataSetChanged()

        //
        listener.onSelect(mSelectedDates)
    }

    private fun buildDays(d: LocalDate) {
        if (d.month != cDisplayMonth?.dt?.month) {
            mData.clear()

            // week
            listOf("D", "S", "T", "Q", "Q", "S", "S").forEach { dayOfWeek ->
                mData.add(Item(dayOfWeek, null))
            }

            // days
            var cur = d.withDayOfMonth(1)
            var end = cur.plusMonths(1).minusDays(1)

            // prefix & suffix
            cur = cur.plusDays((cur.dayOfWeek.value * -1).toLong() % 7)
            val endFixedWeekDay = end.dayOfWeek.value % 7
            end = end.plusDays((6 - endFixedWeekDay).toLong())

            // fill
            while (cur <= end) {
                mData.add(Item(Dt.format(cur, "d")!!, cur))
                cur = cur.plusDays(1)
            }

            listener.onMonth(d)
        }
    }

    fun changeMonth(factor: Int) {
        val d = cDisplayMonth!!.dt!!.plusMonths(factor.toLong())
        buildDays(d)

        notifyDataSetChanged()
    }

    //

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val v = Act.inflate(R.layout.rv_dialog_proposal_date, parent, false)!!
        return VH(v)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val m = mData[position]
        val context = holder.itemView.context
        holder.lbl.text = m.text

        //
        val isHeader = m.dt==null
        holder.itemView.isClickable = !isHeader
        holder.itemView.isFocusable = !isHeader
        holder.lbl.alpha = if (isHeader) 0.15f else 1.0f
        holder.lbl.setTextColor(Cl.res(context, R.color.md_grey_500))
        holder.lbl.setBackgroundColor(Cl.res(context, R.color.transparent))

        //
        val dt = m.dt ?: return
        val displayMonth = cDisplayMonth?.dt ?: return

        //
        val isSameDisplayMonth = dt.month == displayMonth.month
        holder.lbl.setTextColor(Cl.ter(isSameDisplayMonth, R.color.md_grey_700, R.color.md_grey_400))

        //
        if (Dt.isToday(dt)) {
            holder.lbl.setTextColor(Cl.res(context, R.color.md_pink_500))
        }

        //

        //
        val isSelected = mSelectedDates.contains(dt)
        if (isSelected) {
            holder.lbl.setBackgroundColor(Cl.res(context, R.color.md_blue_500))
            holder.lbl.setTextColor(Cl.res(context, R.color.white))
        } else if (dt < LocalDate.now()) {
            holder.lbl.setBackgroundColor(Cl.res(context, R.color.md_grey_100))
        }

        //
        holder.itemView.setOnClickListener { selectDate(dt) }
    }

    // vh
    inner class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val lbl = itemView.ui_lbl_date_label
    }

    // listener
    interface Listener {
        fun onSelect(days: List<LocalDate>)
        fun onMonth(day: LocalDate)
        fun onSelCount(): Int
    }
}