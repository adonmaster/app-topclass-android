package br.com.adonio.topclass

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginActivityVM: ViewModel() {

    var email = ""
    var name = ""

    val loading = MutableLiveData<Int>().also { it.value = 0 }
    fun loadingInc(diff: Int) {
        loading.value = (loading.value ?: 0) + diff
    }

}