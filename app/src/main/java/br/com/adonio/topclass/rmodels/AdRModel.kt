package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.helpers.JParserDsl
import br.com.adonio.topclass.helpers.JParserResult
import br.com.adonio.topclass.presenters.AdRModelPresenter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class AdRModel(
    val id: Int,
    val client_user_id: Int,
    val body: JSONObject,
    val expire_at: LocalDateTime,
    val created_at: LocalDateTime,
    val client_user: Usr,
    val pros: List<AdPro>
)
{
    val p by lazy { AdRModelPresenter(this) }


    //
    data class Usr(
        val id: Int,
        val name: String,
        val email: String,
        val avatar_thumb: String?,
        val avatar: String?
    )

    data class AdPro(
        val price: Double,
        val id: Int,
        val name: String,
        val email: String,
        val avatar: String?,
        val avatar_thumb: String?
    )

    //
    companion object {
        private fun storeSingle(): JParserDsl.()->Unit = {
            store("id")
            store("client_user_id")
            store("body")
            store("expire_at")
            store("created_at")
            obj("client_user") {
                store("id")
                store("name")
                store("email")
                store("avatar_thumb")
                store("avatar")
            }
            arr("pros") {
                store("price")
                storeSeek("pro_user_id", "pro_user.id")
                storeSeek("pro_user_name", "pro_user.name")
                storeSeek("pro_user_email", "pro_user.email")
                storeSeek("pro_user_avatar", "pro_user.avatar")
                storeSeek("pro_user_avatar_thumb", "pro_user.avatar_thumb")
            }
        }

        private fun parseSingle(): JParserResult.() -> AdRModel = {
            AdRModel(
                getNext(), getNext(), getNext(), Dt.fromJson(getNext())!!, Dt.fromJson(getNext())!!,
                obj("client_user") {
                    Usr(getNext(), getNext(), getNext(), getNexto(), getNexto())
                },
                arr("pros") {
                    AdPro(getNext(), getNext(), getNext(), getNext(), getNexto(), getNexto())
                }
            )
        }

        fun parseSingle(json: JSONObject?): AdRModel = JParser(json) {
            obj("payload", storeSingle())
        }.parse {
            obj("payload", parseSingle())
        }

        fun parse(json: JSONObject?): List<AdRModel> = JParser(json) {
            arr("payload", storeSingle())
        }.parse {
            arr("payload", parseSingle())
        }

        val sample = """
            {
      "id": 3,
      "client_user_id": 1,
      "body": {
        "items": [
          {
            "duration": 60,
            "materia": "Língua Portuguesa",
            "dt": "2020-04-22 07:05:00"
          }
        ],
        "address_comp": null,
        "address_line": "Avenida V-5 Quadra 199, Lote 23 Cidade Vera Cruz II - Cidade Vera Cruz, Aparecida de Goiânia - GO, 74936-600, Brasil",
        "address_lat": -16.7639998,
        "address_lng": -49.2899184,
        "version": 2
      },
      "expire_at": "2020-04-22 07:05:00",
      "created_at": "2020-04-10 02:06:24",
      "updated_at": "2020-04-10 02:06:24",
      "client_user": {
        "id": 1,
        "name": "Adonho",
        "email": "adonio.silva@gmail.com",
        "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
        "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
        "email_verified_at": "2020-01-30 16:36:58",
        "last_active": "2020-01-30 16:36:58",
        "is_pro": true,
        "created_at": "2019-11-04 20:33:34",
        "updated_at": "2020-01-30 16:37:00"
      },
      "pros": [
        {
          "id": 1,
          "ad_id": 3,
          "pro_user_id": 1,
          "price": 60,
          "created_at": "2020-04-10 02:29:21",
          "updated_at": "2020-04-10 02:29:21",
          "pro_user": {
            "id": 1,
            "name": "Adonho",
            "email": "adonio.silva@gmail.com",
            "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574196353_LxNG0hojm9K2.thumb.jpg",
            "avatar": "http://topclass.adonio.com.br/storage/attachments/1574196353_Eko9xuibclBN.jpg",
            "email_verified_at": "2020-01-30 16:36:58",
            "last_active": "2020-01-30 16:36:58",
            "is_pro": true,
            "created_at": "2019-11-04 20:33:34",
            "updated_at": "2020-01-30 16:37:00"
          }
        }
      ]
    }
        """.trimIndent()
    }

}