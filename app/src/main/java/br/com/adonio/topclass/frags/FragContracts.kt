package br.com.adonio.topclass.frags

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.frags.contract.FragContractView
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.ContractRModel
import kotlinx.android.synthetic.main.frag_contracts.*
import kotlinx.android.synthetic.main.rv_frag_contracts.view.*
import org.threeten.bp.LocalDateTime

class FragContracts: Fragment(), Rv.Owner {


    //
    private lateinit var vm: FragContractsVM
    private lateinit var mAdapter: Rv

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_contracts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(activity!!).get(FragContractsVM::class.java)

        vm.loading.observe(this, Observer {
            if (it == 0) ui_swipe?.isRefreshing = false
            ui_progress?.isVisible = it > 0
        })

        ui_swipe?.setOnRefreshListener {
            ui_swipe?.isRefreshing = true
            requestData()
        }

        mAdapter = Rv(this, R.layout.rv_frag_contracts)
        ui_rv.adapter = mAdapter
        Rv.setupForTable(ui_rv, context)

        requestData()
    }

    private fun requestData()
    {
        vm.loadingInc(1)
        Http.get("contracts")
            .then {

                val list = ContractRModel.parse(it)

                ui_empty?.isVisible = list.count() == 0

                mAdapter.updateData(list)

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }


    //

    override fun rvCreate(
        identifier: String,
        itemView: View,
        r: () -> Rv.Value,
        position: () -> Int
    ) {
        itemView.setOnClickListener {

            synced("rvCreate@itemSelected") {
                vm.loadingInc(1)
                vm.selected = r().getValue()
                Task.main {
                    findNavController().navigate(R.id.sg_to_contract_view)

                    vm.loadingInc(-1)
                    syncRelease()
                }
            }

        }
    }

    @SuppressLint("SetTextI18n", "DefaultLocale")
    override fun rvBind(identifier: String, v: View, h: Rv.VH, r: Rv.Value, position: Int) {
        r.whenValue<ContractRModel> { model ->

            val displayPro = model.clientId == App.user?.remote_id?.toInt()
            val userName = if (displayPro) model.proName else model.clientName
            val userAvatar = if (displayPro) model.proAvatar else model.clientAvatar

            //
            v.ui_title.text = userName

            //
            Img.load(userAvatar, v.ui_avatar)

            //
            v.ui_desc1.text = "R$ ${Str.number(model.price)}, contrato n.o ${model.id}"

            //
            val body = model.p.body!!
            val lastDate = body.items.last().dt
            val dt = body.items
                .map { it.dt }
                .fold(lastDate, { acc, i ->
                    if (i > LocalDateTime.now() && i < acc) i else acc
                })
            v.ui_desc2.text = "${Dt.fromNow(dt)}, ${Dt.formatBrDate(dt)}".capitalize()

            if (model.was_executed) {
                v.ui_wrapper_desc3.setBackgroundColor(Cl.md_green_500())

                v.ui_desc3_icon.text = "\uF058"
                v.ui_desc3.text = "Contrato executado"
            } else {
                v.ui_wrapper_desc3.setBackgroundColor(Cl.md_indigo_500())

                v.ui_desc3_icon.text = "\uF253"
                v.ui_desc3.text = "Contrato ainda vigente"
            }
        }
    }

    override fun rvId(identifier: String, r: Rv.Value, position: Int): Int? {
        return r.value<ContractRModel>()?.id
    }

}