package br.com.adonio.topclass.presenters

import br.com.adonio.topclass.rmodels.ProposalRModel
import br.com.adonio.topclass.structs.ProposalBody
import org.threeten.bp.LocalDateTime

class ProposalRModelPresenter(model: ProposalRModel) : BasePresenter<ProposalRModel>(model) {

    val body by lazy { ProposalBody.parse(model.body) }

    val isPhasePending by lazy { model.phase < 3 && model.expireAt >= LocalDateTime.now() }
    val isPhaseDone by lazy { model.phase == 3 }
    val isPhaseExpired by lazy { model.phase < 3 && model.expireAt < LocalDateTime.now() }

}