package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.JParser
import org.json.JSONObject

data class UserRModel(
    val id: Long,
    val name: String,
    val email: String,
    val avatar_thumb: String?,
    val avatar: String?,
    val isPro: Boolean
) {

    companion object {

        fun parse(json: JSONObject) = JParser(json) {
            obj("payload") {
                obj("user") {
                    store("id")
                    store("name")
                    store("email")
                    store("avatar_thumb")
                    store("avatar")
                    store("is_pro")
                }
            }
        }.parse {
            obj("payload") {
                obj("user") {
                    UserRModel(
                        get("id"),
                        get("name"),
                        get("email"),
                        geto("avatar"),
                        geto("avatar_thumb"),
                        get("is_pro")
                    )
                }
            }
        }

        val sample = """
            {
  "message": "Operação executada com êxito.",
  "payload": {
    "user": {
      "id": 2,
      "name": "Adon",
      "email": "adonio@outlook.com",
      "avatar_thumb": "http://topclass.adonio.com.br/storage/attachments/1574194348_arPmIlAuUj14.thumb.jpg",
      "avatar": "http://topclass.adonio.com.br/storage/attachments/1574194348_eXobcy1gG7jd.jpg",
      "email_verified_at": "2019-11-11 22:52:35",
      "last_active": "2019-11-19 17:12:28",
      "is_pro": false,
      "created_at": "2019-11-05 03:41:22",
      "updated_at": "2019-11-19 17:12:28"
    }
  }
}
        """.trimIndent()

    }

}