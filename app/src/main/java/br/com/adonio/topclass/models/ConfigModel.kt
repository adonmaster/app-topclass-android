package br.com.adonio.topclass.models

import androidx.room.Entity

@Entity(tableName = "config")
class ConfigModel: BaseModel() {

    var key: String = ""
    var value: String? = null

}