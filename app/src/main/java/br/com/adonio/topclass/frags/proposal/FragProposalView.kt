package br.com.adonio.topclass.frags.proposal

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.dialogs.DialogOk
import br.com.adonio.topclass.extensions.setEnabledEx
import br.com.adonio.topclass.frags.BaseFragMapView
import br.com.adonio.topclass.frags.FragProposalVM
import br.com.adonio.topclass.helpers.*
import br.com.adonio.topclass.rmodels.ProposalRModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.frag_proposal_view.*

@SuppressLint("SetTextI18n")
class FragProposalView: BaseFragMapView()  {

    // static
    companion object {
        fun navigate(f: Fragment, @IdRes id: Int) {
            f.findNavController().navigate(id)
        }
    }

    // state
    private lateinit var vm: FragProposalViewVM
    private lateinit var mData: ProposalRModel

    //
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return baseOnCreateView(R.layout.frag_proposal_view, R.id.ui_map, inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragProposalViewVM::class.java)

        val vmParent = ViewModelProviders.of(activity!!).get(FragProposalVM::class.java)
        mData = vmParent.selected

        //
        ui_id.text = "Proposta #${mData.id}\nCriada ${Dt.fromNow(mData.createdAt)}"

        // pro
        Img.loadAvatar(mData.proAvatar, ui_pro_avatar)
        ui_pro_name.text = mData.proName

        // client
        Img.loadAvatar(mData.clientAvatar, ui_client_avatar)
        ui_client_name.text = mData.clientName

        // body
        val body = mData.p.body!! // <--

        // classes title
        ui_title_aulas.text = Str
            .plural(body.items.count(), "3. Aula (nenhuma)", "3. Aula (1)") { "3. Aulas ($it)" }

        // classes
        ui_classes_desc.text = body.items.mapIndexed { i, item ->
            val dt = Dt.formatBr(item.dt)
            val dtHuman = Dt.fromNow(item.dt)
            val duration = item.duration
            val materia = item.materia
            Span("${i+1}. ").bold().concat("$dt ($dtHuman)")
                .concat("\n$materia")
                .concat("\nDuração: $duration mins").color(context!!, R.color.md_grey_500)
                .build()
        }.joinToString("\n\n")

        // location
        ui_location_desc.text = body.address.address
        Str.whenNotEmpty(body.address.comp) {
            ui_location_desc.append("\n\n${it}")
        }

        // map
        ui_map.getMapAsync { g ->
            val lat = body.address.lat
            val lng = body.address.lng
            g.addMarker(
                MarkerOptions()
                    .position(LatLng(lat, lng))
                    .title(mData.clientName)
            )
            g.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 15.0f))
        }

        // price
        ui_price_desc.text = "R$ " + Str.number(mData.price)

        // obs
        var obsLeftIcon = "\uF071"
        val expiration = Dt.format(mData.expireAt, "dd 'de' MMMM") + " (${Dt.fromNow(mData.expireAt)})"
        val obs = when (mData.phase) {
            1 -> "Aguardando professor aceitar...\nProposta expira $expiration"
            2 -> "Aguardando aluno contratar aula...\nProposta expira $expiration"
            3 -> {
                obsLeftIcon = "\uF058"
                "Contrato já feito!"
            }
            else -> null
        }
        ui_wrapper_obs.isVisible = obs != null
        ui_obs_left.text = obsLeftIcon
        ui_obs_desc.text = obs

        //
        vm.loading.observe(this, Observer {
            ui_progress.isVisible = it > 0
            ui_btn_submit.setEnabledEx(it == 0)
            ui_btn_destroy.setEnabledEx(it == 0)
        })

        //
        val appUserRemoteId = App.user!!.remote_id.toInt()
        ui_wrapper_action.isVisible = false
        when (mData.phase) {
            1 -> {
                ui_wrapper_action.isVisible = appUserRemoteId == mData.proId
                ui_btn_submit.text = "ACEITAR AULA"
                ui_btn_submit.setOnClickListener { submitProposal() }
            }
        }

        ui_btn_destroy.setOnClickListener {
            tryDestroy()
        }
    }

    private fun tryDestroy() {
        DialogOk.show(context, "Tem certea que deseja remover este anúncio?", "Atenção", "Cancelar") {
            destroy()
        }
    }

    private fun destroy() {
        vm.loadingInc(1)
        Http.delete("proposal/${mData.id}")
            .then {

                afterDestroyed()

            }
            .catch {
                T.error(activity, it)
            }
            .always {
                vm.loadingInc(-1)
            }
    }

    private fun afterDestroyed() {
        findNavController().popBackStack()
    }

    private fun submitProposal() {
        synced("FragProposalView@submitProposal") {

            val params = HttpFormData().put("id", mData.id)

            vm.loadingInc(1)
            Http.post("proposals/accept", params)
                .then {
                    afterSubmitProposal()
                }
                .catch {
                    T.error(activity, it)
                }
                .always {
                    syncRelease()
                    vm.loadingInc(-1)
                }

        }
    }

    private fun afterSubmitProposal() {
        findNavController().navigate(R.id.sg_to_done)
    }

}