package br.com.adonio.topclass.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.adonio.topclass.App
import br.com.adonio.topclass.R
import br.com.adonio.topclass.extensions.setVisible
import br.com.adonio.topclass.frags.profile.FragProfileDetail
import br.com.adonio.topclass.frags.profile.FragProfilePro
import br.com.adonio.topclass.frags.profile.FragProfileProResolved
import br.com.adonio.topclass.helpers.Http
import br.com.adonio.topclass.repo.Repo
import br.com.adonio.topclass.rmodels.UserRModel
import kotlinx.android.synthetic.main.frag_profile.*

class FragProfile: Fragment() {

    companion object {
        fun requestProfile(whenDone: ()->Unit) {
            val requisitions: Int = Repo.pref.get("FragProfile@pendingProRequisition", 0)
            if (requisitions > 0) {
                Http.get("profile")
                    .then {
                        val rmodel = UserRModel.parse(it)
                        Repo.user.update(rmodel)

                        // load to instance
                        App.shared.invalidateUid()
                        App.shared.loadActiveUser()
                    }
                    .catch {}
                    .always(whenDone)
            } else {
                whenDone()
            }
        }
    }

    private lateinit var vm: FragProfileVM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProviders.of(this).get(FragProfileVM::class.java)

        vm.loading.observe(this, Observer {
            ui_profile_progress.setVisible(it > 0)
        })

        setupPager()
    }

    private fun setupPager() {
        val adapter = object: FragmentPagerAdapter(childFragmentManager) {
            override fun getPageTitle(position: Int): CharSequence? {
                return when(position) {
                    0 -> "PERFIL"
                    1 -> "PROFESSOR"
                    else -> null
                }
            }
            override fun getItem(position: Int): Fragment {
                return when(position) {
                    0 -> FragProfileDetail()
                    1 -> if (App.user?.isPro == true) FragProfileProResolved() else FragProfilePro()
                    else -> Fragment()
                }
            }
            override fun getCount(): Int {
                return 2
            }
        }
        ui_pager.adapter = adapter
        ui_tab_layout.setupWithViewPager(ui_pager)
    }

}