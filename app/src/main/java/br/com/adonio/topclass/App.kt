package br.com.adonio.topclass

import android.app.Application
import br.com.adonio.topclass.db.DB
import br.com.adonio.topclass.helpers.Str
import br.com.adonio.topclass.models.User
import br.com.adonio.topclass.repo.Repo
import com.androidnetworking.AndroidNetworking
import com.jakewharton.threetenabp.AndroidThreeTen
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class App: Application() {

    companion object {
        lateinit var shared: App

        val user
            get() = shared.user

        val userIsValid
            get() = shared.user != null
    }

    private var user: User? = null

    fun clearUser() {
        user = null
        Repo.user.clearActive()
    }

    private var uid = Str.random(32)
    fun getUid() = uid
    fun invalidateUid() {
        uid = Str.random(32)
    }

    private fun test() {
    }

    override fun onCreate() {
        super.onCreate()

        shared = this

        test()

        configDateTime()
        configHttp()
        configDb(false)
        loadActiveUser()
    }

    private fun configDateTime() {
        AndroidThreeTen.init(this)
    }

    private fun configHttp() {
        val config = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val authRequest = chain.request()!!.newBuilder()
                    .addHeader("Accept", "application/json")
                    .also { r ->
                        user?.token?.let { tk ->
                            r.addHeader("Authorization","Bearer $tk")
                        }
                    }
                    .build()
                chain.proceed(authRequest)
            }
            .addInterceptor { chain ->
                val r = chain.proceed(chain.request())
                if (r.code() == 401) { clearUser() }
                r
            }
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        AndroidNetworking.initialize(applicationContext, config)
        AndroidNetworking.enableLogging()
    }

    private fun configDb(deleteDatabase: Boolean)
    {
        deleteDatabase && deleteDatabase("main.room")

        DB.config(this)
        DB.seed()
    }

    fun loadActiveUser() {
        user = Repo.user.findActive()
    }

}