@file:Suppress("unused")

package br.com.adonio.topclass.helpers

import android.app.Activity
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import br.com.adonio.topclass.R
import com.google.firebase.messaging.RemoteMessage

class Notify {

    companion object {

        fun from(remoteMessage: RemoteMessage): Notify {
            return Notify().assign(remoteMessage)
        }

        fun extractFromPayload(remoteMessage: RemoteMessage, key: String): String? {
            var r: String? = null
            JEx(remoteMessage.data["payload"]) {
                r = str(key)
            }
            return r
        }

        fun extractMessage(remoteMessage: RemoteMessage): Pair<String,String>? {
            listOfNotNull(
                    remoteMessage.data["title"],
                    remoteMessage.data["body"]
                )
                .takeIf { it.count() == 2 }
                ?.let { return it[0] to it[1] }

            return null
        }

    }

    private var title: String? = null
    private var body: String? = null
    private var badge = 1
    private var color = R.color.white

    private fun assign(rm: RemoteMessage): Notify
    {
        this.title = rm.data["title"]
        this.body = rm.data["body"]
        this.badge = rm.data["badge"]?.toInt() ?: 1

        return this
    }

    fun <T: Activity> show(context: Context, activityClass: Class<T>)
    {
        // halt
        if (title==null || body==null) return

        // go on
        val intent = Intent(context, activityClass).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        val pending = PendingIntent
            .getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        val notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notifyBuilder = NotificationCompat.Builder(context, "what")
            .setContentTitle(title)
            .setContentText(body)
            .apply {
                if (badge == 1) {
                    setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                } else {
                    setBadgeIconType(NotificationCompat.BADGE_ICON_LARGE)
                }
            }
            .setColor(Cl.res(context, color))
            .setSmallIcon(R.mipmap.ic_launcher)
            .setAutoCancel(true)
            .setSound(notificationSound)
            .setContentIntent(pending)

        val service = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        service.notify(0, notifyBuilder.build())
    }


}