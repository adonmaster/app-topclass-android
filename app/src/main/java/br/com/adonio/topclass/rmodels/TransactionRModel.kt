package br.com.adonio.topclass.rmodels

import br.com.adonio.topclass.helpers.Dt
import br.com.adonio.topclass.helpers.JParser
import br.com.adonio.topclass.presenters.TransactionRModelPresenter
import org.json.JSONObject
import org.threeten.bp.LocalDateTime

data class TransactionRModel(
    val page: Int,
    val prevPageUrl: String?,
    val nextPageUrl: String?,
    val items: List<Item>
) {

    data class Item(
        val id: Int,
        val operationType: String,
        val amount: Double,
        val balance: Double,
        val desc: String,
        val createdAt: LocalDateTime
    ) {
        val p by lazy { TransactionRModelPresenter(this) }
    }

    companion object {
        fun parse(json: JSONObject) = JParser(json) {
            obj("payload") {
                store("current_page")
                store("prev_page_url")
                store("next_page_url")
                arr("data") {
                    store("id")
                    store("operation_type")
                    store("amount")
                    store("balance")
                    store("desc")
                    store("created_at")
                }
            }
        }.parse {
            obj("payload") {
                TransactionRModel(getNext(), getNexto(), getNexto(), arr("data") {
                    Item(
                        getNext(), getNext(), getNext(), getNext(), getNext(),
                        Dt.fromJson(getNext())!!
                    )
                })
            }
        }
    }

}
/**
{
    "message": "Operação executada com êxito.",
    "payload": {
    "current_page": 1,
    "data": [
    {
        "id": 1,
        "user_id": 1,
        "operation_type": "credit",
        "sourceable_type": "App\\Contract",
        "sourceable_id": 1,
        "amount": "50.00",
        "balance": "50.00",
        "desc": "Pagamento relativo ao contrato #1",
        "created_at": "2020-03-06 22:16:51",
        "updated_at": "2020-03-06 22:16:51"
    },
    {
        "id": 2,
        "user_id": 1,
        "operation_type": "fees",
        "sourceable_type": null,
        "sourceable_id": null,
        "amount": "-2.40",
        "balance": "47.60",
        "desc": "Taxa de transação via cartão de crédito1",
        "created_at": "2020-03-06 22:16:51",
        "updated_at": "2020-03-06 22:16:51"
    }
    ],
    "first_page_url": "http://topclass.adonio.com.br/api/transactions?page=1",
    "from": 1,
    "next_page_url": null,
    "path": "http://topclass.adonio.com.br/api/transactions",
    "per_page": 15,
    "prev_page_url": null,
    "to": 2
}
}
*/