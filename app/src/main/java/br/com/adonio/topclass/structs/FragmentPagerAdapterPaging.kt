package br.com.adonio.topclass.structs

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class FragmentPagerAdapterPaging {

    data class Page(val title: String, val creator: ()-> Fragment)

    companion object {
        fun from(fm: FragmentManager, pages: List<Page>): FragmentPagerAdapter {
            return object: FragmentPagerAdapter(fm) {
                override fun getCount(): Int {
                    return pages.count()
                }
                override fun getPageTitle(position: Int): CharSequence? {
                    return pages[position].title
                }
                override fun getItem(position: Int): Fragment {
                    return pages[position].creator()
                }
            }
        }
    }

}